﻿using Microsoft.EntityFrameworkCore;
using UserApi.Model;

namespace UserApi.Context
{
    public class UserDbContext:DbContext
    {
        public UserDbContext(DbContextOptions<UserDbContext> Context):base(Context)
        {

        }

        //create a table 
        public DbSet<User> UserList { get; set; }
    }
}
