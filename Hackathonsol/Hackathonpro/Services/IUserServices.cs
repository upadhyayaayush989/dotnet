﻿using Hackathonpro.Models;

namespace Hackathonpro.Services
{
    public interface IUserServices
    {
        public bool AddUser(string Name,User user);
        public bool VerifyUser(string userName, string password);
        List<Food> GetProducts();
        void AddValues(int id);
        List<AddToCart> GetCart();
        void DeleteItemFromCart(int id);
        int GenerateBill();
        void RemoveItems();
    }
}
