﻿using Day5.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day5.Repository
{
    internal interface IUserRepository
    {
        bool RegisterUser(User user);
    }
}
