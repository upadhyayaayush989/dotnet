﻿
using Hackathonpro.Models;
using Microsoft.EntityFrameworkCore;

namespace Database_MVC.Context
{
    public class FoodDbContext : DbContext
    {
        public FoodDbContext(DbContextOptions<FoodDbContext> Context) : base(Context)
        {

        }

        public DbSet<User> users { get; set; }
        public DbSet<Food> foods { get; set; }
        public DbSet<AddToCart> addToCarts { get; set; }
        public DbSet<LoginDet> loginDet { get; set; }
    }
}