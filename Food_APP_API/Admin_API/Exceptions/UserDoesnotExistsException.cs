﻿namespace Admin_API.Exceptions
{
    public class UserDoesnotExistsException:ApplicationException
    {
        public UserDoesnotExistsException()
        {

        }
        public UserDoesnotExistsException(string msg) : base(msg)
        {

        }
    }
}
