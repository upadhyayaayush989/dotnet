﻿using Hackathonpro.Models;

namespace Hackathonpro.Repository
{
    public interface IUserRepo
    {
        
        bool GetUserByName(string Name);
        void AddUser(User user);
        bool GetUserByUserName(string username, string password);
        bool GetUserByUserPassword(string password);
        List<Food> GetAllProducts();
        void AddValuesToCart(int id);
        List<AddToCart> GetCart();
        void DeleteElement(int id);
        int GenerateBill();
        void RemoveElements();
    }
}
