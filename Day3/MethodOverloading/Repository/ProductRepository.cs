﻿using MethodOverloading.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MethodOverloading.Repository
{
    internal class ProductRepository
    {
        Product[] products;
        public ProductRepository()
        {
            products = new Product[]
            {
                new Product("oppo","mobile",15000,4.2f),
                new Product("moto","mobile",17000,4.3f),
                new Product("Samsung","TV",25000,4.5f),
                new Product("Lenovo","laptop",45000,4.2f)
            };
        }
        public Product[] GetAllProducts()
        {
            return products;
        }
        public void UpdateProduct(int index,string name1,string categ,int price1,int rating)
        {
            products[index].Name = name1;
            products[index].Category = categ;
            products[index].Price = price1;
            products[index].Rating = rating;

        }
        public void DeleteProduct(int indexD)
        {
            int i = 0;
            foreach(Product p in products)
            {
                i++;
            }
            Product[] fproduct = new Product[i-1];
            int a = 0; 
            foreach(Product item1 in products)
            {
                if(item1!= products[indexD])
                {
                    fproduct[a] = item1;
                    a++;
                }
            }
            products = fproduct;
        }
        
            
        
        //public void GetProductByCategory(string category)
        //{

        //}
    }
}
