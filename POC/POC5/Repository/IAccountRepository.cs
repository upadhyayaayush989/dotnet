﻿using Microsoft.AspNetCore.Identity;
using POC5.Model;

namespace POC5.Repository
{
    public interface IAccountRepository
    {
        Task<IdentityResult> RegisterUser(RegisterUser registerUser);
        Task<string> LoginUser(LoginUser loginUser);

        Task<string> GetRefreshToken(string userName);
        public  Task<ApplicationUser> GetUserByName(string id);

        public Task<List<ApplicationUser>> GetAllUsers();
    }
}
