﻿using System;
using System.Collections.Generic;

namespace RegistrationForm.Models
{
    public partial class CtcinThousandTbl
    {
        public CtcinThousandTbl()
        {
            ProfessionalDetailsTbls = new HashSet<ProfessionalDetailsTbl>();
        }

        public int CtcinThousandPerAnumId { get; set; }
        public int? CtcinThousandPerAnum { get; set; }

        public virtual ICollection<ProfessionalDetailsTbl> ProfessionalDetailsTbls { get; set; }
    }
}
