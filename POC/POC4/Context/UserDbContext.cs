﻿using Microsoft.EntityFrameworkCore;
using POC4.Model;

namespace POC4.Context
{
    public class UserDbContext:DbContext
    {
        public UserDbContext(DbContextOptions<UserDbContext> options) : base(options)
        {

        }
        public DbSet<User> Users { get; set; }
    }
}
