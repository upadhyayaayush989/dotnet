using Food_API.Context;
using Food_API.Controllers;
using Food_API.Model;
using Food_API.Repository;
using Food_API.Service;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace Food_API_Tests

{
    public class UnitTest1:IClassFixture<FoodDbFixture>
    {
        //private readonly IFoodService _foodService;
        //private readonly FoodController _foodController;
        //private readonly IFoodRepository _foodRepository;
        //private readonly FoodDbContext _foodDbContext;
        readonly IFoodService _foodService;
        readonly IFoodRepository _foodRepository;
        readonly FoodDbContext _foodDbContext;
        public UnitTest1(FoodDbFixture foodDbFixture)
        {
            _foodRepository = new FoodRepository(foodDbFixture._foodDbContext);
            _foodService = new FoodService(_foodRepository);
            _foodDbContext = foodDbFixture._foodDbContext;
        }

       

        [Fact]
        public void AddFood_ShouldAddFoodIfFoodNotPresentAndReturnTrue()
        {
            //Arrange
            var expected = true;
            FoodItems food = new FoodItems() { FoodId = 4, FoodName = "Rice4", FoodType = "Veg", Price = 300, ImagePath = "string", FoodDiscription = "gesgs" };
            //Act
            var actual = _foodService.AddFood(food);
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Edit_ShouldEditFoodIfFoodIsPresent()
        {
            //Arrange
            var expected = true;
            var id = 2;
            FoodItems food = new FoodItems() { FoodName = "Rice4Edit", FoodType = "Veg", Price = 300, ImagePath = "string", FoodDiscription = "after edit " };
            //Act
            var actual = _foodService.EditFood(id, food);
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public async Task GetAllFood_ShouldReturnAllFoods()
        {
            //Arrange
            var expected = _foodDbContext.FoodItems.Count();
            //Act
            var actual = await _foodService.GetAllFood();
            //Assert
            Assert.Equal(expected, actual.Count());
        }



        [Fact]
        public void Delete_ShouldDeleteFoodIfPresentAndReturnTrue()
        {
            //Arrange
            var expected = true;
            var id = 4;
            //Act
            var actual = _foodService.DeleteFood(id);
            //Assert
            Assert.Equal(expected, actual);
        }

    }
}