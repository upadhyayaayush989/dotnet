﻿using Microsoft.AspNetCore.Mvc;
using SessionData.Models;
using System.Diagnostics;

namespace SessionData.Controllers
{
    public class HomeController : Controller
    {
        

        public IActionResult Index()
        {
            HttpContext.Session.SetString("Name", "user1");
            HttpContext.Session.SetInt32("Age", 22);
            return View();
        }

        public ActionResult Get()
        {
            User user = new User()
            {
                Name = HttpContext.Session.GetString("Name"),
                Age = HttpContext.Session.GetInt32("Age").Value
            };
            return View(user);
        }
        public ActionResult GetQueryString(string name,int age)
        {
            User user = new User()
            {
                Name = name, 
                Age = age 
            };
            return View(user);
        }



        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}