﻿using System;
using System.Collections.Generic;

namespace RegistrationForm.Models
{
    public partial class CtcinLakhsTbl
    {
        public CtcinLakhsTbl()
        {
            ProfessionalDetailsTbls = new HashSet<ProfessionalDetailsTbl>();
        }

        public int CtcinLakhPerAnumId { get; set; }
        public int? CtcinLakhPerAnum { get; set; }

        public virtual ICollection<ProfessionalDetailsTbl> ProfessionalDetailsTbls { get; set; }
    }
}
