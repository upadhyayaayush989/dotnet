﻿namespace Hackathonpro.Models
{
    public class LoginDet
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
