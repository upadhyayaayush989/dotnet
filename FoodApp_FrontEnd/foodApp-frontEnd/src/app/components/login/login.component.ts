import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user';
import { LoginRegisterService } from 'src/app/services/user/login-register.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})




export class LoginComponent implements OnInit {
  
  loginUser:User;
  
  constructor(private router:Router,private userService:LoginRegisterService) {
    this.loginUser=new User();
   }

  ngOnInit(): void {
    
  }

  LoginUser() {
    this.userService.LoginUser(this.loginUser).subscribe({next: res=>{
      if(res){
        let userName = this.loginUser.userName;
        let userEmail = this.loginUser.email;
        let jsonObject = JSON.stringify(res);
        let jsonToken = JSON.parse(jsonObject);
        localStorage.setItem("userToken", jsonToken["Token"]);
        localStorage.setItem("username",userName! );
        localStorage.setItem("useremail",jsonToken["Email"]);
        console.log("Login Success");
        Swal.fire(
          'User Login',
          'Login Success',
          'success'
        )
        
        this.router.navigate(["dashboard"]);
      }
      else{
        console.log("Login Failed");
        Swal.fire(
          'Oops',
          'Login Failed',
          'error'
        )
      }
    },
    error:(e)=>{
      console.error(e);
      Swal.fire({
        icon: 'error',
        title: 'Error!!',
        text: `${this.loginUser.userName} is not Valid`  
      });
      
      this.router.navigate(['login']);
    }
  });
  }
  SignUp(){
    this.router.navigate(["signup"]);
  }
  Admin(){
    this.router.navigate(['adminlogin']);
  }
}
