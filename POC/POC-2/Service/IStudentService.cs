﻿using POC_2.Models;
using StudentPersistantLayer.Model;

namespace POC_2.Service
{
    public interface IStudentService
    {

        List<StudentViewModel> GetAllStudents();

        bool StudentAlreadyExists(Student student);
        bool AddStudent(Student student);
        
        void DeleteStudent(int studentId);
        Student GetStudentById(int studentId);
        bool EditStudent(Student updatedStudent);
    }
}
