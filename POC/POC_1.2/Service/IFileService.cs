﻿using POC_1._2.Models;

namespace POC_1._2.Service
{
    public interface IFileService
    {
        public FileUploadViewModel CreateNewInstanceFileViewModel();
        public void GetDataFromExcelFile(FileUploadViewModel model);

        
    }
}
