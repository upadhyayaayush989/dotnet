﻿using Microsoft.AspNetCore.Mvc;
using MvcCore.Models;

namespace MvcCore.Repository
{
    public class UserRepository : IUserRepository
    {
        List<User> users;
        public UserRepository()
        {
            users = new List<User>();
        }
        public List<User> GetAllUsers()
        {
            return users;
        }

        //string guid = Guid.NewGuid().ToString();//Inbuilt structure
        //public override string ToString()
        //{
        //    return guid;
        //}
        /// <summary>
        /// This Mehtod Adds user to the list when the form's submit button
        /// is clicked
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public bool AddUsers(User user)
        {
            users.Add(user);
            return true;
        }

        public bool DeleteUser(int id)
        {
            User user = GetUserById(id);
            return users.Remove(user); 
        }

        private User GetUserById(int id)
        {
            return users.Find(u=>u.Id==id);
        }
    }
}
