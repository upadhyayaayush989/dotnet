﻿using ProductApi.Model;

namespace ProductApi.Repository
{
    public class ProductRepository:IProductRepository
    {
        static List<Product> products;
        public ProductRepository()
        {
            products = new List<Product>()
            {
                new Product(){ID=1,Name="TV",Category="Electronics"},
                new Product(){ID=2,Name="Laptop",Category="Electronics"},
                new Product(){ID=3,Name="Mobile",Category="Mobile"}
            };
        }

        public bool AddProduct(Product product)
        {
            products.Add(product);
            return true;
        }

        public bool EditProduct(int id,Product productUpdate)
        {
            if (products.Any(p => p.ID == id))
            {
                products.Find(p => p.ID == id).Name = productUpdate.Name;
                products.Find(p => p.ID == id).Category = productUpdate.Category;
                return true;
            }
            
           //product.ID = productId;
           // product.Name = productName;
           // product.Category = productCategory;
           // return product;
           return false;
        }

        public List<Product> GetAllProducts()
        {
            return products;
        }

        public object GetProductById(int id)
        {
           var foundProduct= products.Find(p => p.ID == id);
            return foundProduct;
        }
        public object DeleteProductById(int id)
        {
            var deleteProduct = products.Find(p => p.ID == id);
            products.Remove(deleteProduct);
            return deleteProduct;
        }
    }
}
