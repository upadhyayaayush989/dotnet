﻿using Microsoft.EntityFrameworkCore;
using MvcCore.Models;

namespace MvcCore.Context
{
    public class UserDbContext:DbContext
    {
        public UserDbContext(DbContextOptions<UserDbContext>Context):base(Context)
        {
            //Database.EnsureCreated();--disadvatage model changes aren't updated automatically


        }

        //Table creation
        public DbSet<User> users { get; set; }
    }
}
