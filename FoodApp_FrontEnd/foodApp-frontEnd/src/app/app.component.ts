import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  homepage?:boolean;
  title = 'foodApp-frontEnd';

  constructor(private router: Router) {
    console.log(this.router.url)
    if(this.router.url == "/")
    {
      this.homepage= true;
    }
    else{
      this.homepage= false;
    }
  }

}



