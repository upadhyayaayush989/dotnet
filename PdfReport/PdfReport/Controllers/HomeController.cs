﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PdfReport.Model;
using Wkhtmltopdf.NetCore;

namespace PdfReport.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        readonly IGeneratePdf _generatePdf;
        public HomeController(IGeneratePdf generatePdf)
        {
            _generatePdf = generatePdf;
        }

        [HttpGet]
        [Route("GetEmployeeInfo")]
        public async Task<IActionResult> GetEmployeeInfo()
        {
            var empObject = new Empoyee
            {
                EmpId = 1,
                EmpName = "Aayush",
                Department = "It",
                Designation = "Software Engineer"
            };
            return await _generatePdf.GetPdf("Views/Empoyee/EmployeeInfo.cshtml",empObject);
        }

    }
}
