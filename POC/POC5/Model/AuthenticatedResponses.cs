﻿namespace POC5.Model
{
    public class AuthenticatedResponses
    {
        public string? Token { get; set; }
        public string RefreshToken { get; set; }
    }
}
