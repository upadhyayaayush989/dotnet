﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;

namespace FoodApp_Mvc.Models.ViewModel
{
    public class SignUpUserViewModel
    {
        public int UserId { get; set; }

        [Required(ErrorMessage = "Please enter Username")]
        [Remote(action: "UserNameExists", controller: "Account")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Please enter Email")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please enter Mobile Number")]
        [Display(Name = "Mobile Number")]
        public long Mobile { get; set; }
        [Required(ErrorMessage = "Please enter Password")]
        public string Password { get; set; }
        [Required(ErrorMessage = "Please Confirm Password ")]
        [Display(Name = "Confirm Password")]
        public string ConfirmPassword { get; set; }
        [Display(Name = "Active")]
        public bool IsActive { get; set; }
    }
}
