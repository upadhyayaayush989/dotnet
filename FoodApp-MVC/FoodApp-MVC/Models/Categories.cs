﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FoodApp_MVC.Models
{
    public class Categories
    {
        [Key]
        public string CategoryName { get; set; }

    }
}
