﻿$(document).ready(function () {
    $("#CountryId").change(function () {
        var countryId = $(this).val();
        //debugger
        $.ajax({
            type: "post",
            url: "/Home/GetAllStates?CountryId=" + countryId,
            contentType: "html",
            success: function (response) {
                //debugger
                $("#StateId").empty();
                $("#StateId").append(response);
            }
        })
    })


    $("#StateId").change(function () {
        var stateId = $(this).val();
        //debugger
        $.ajax({
            type: "post",
            url: "/Home/GetAllCities?StateId=" + stateId,
            contentType: "html",
            success: function (response) {
                //debugger
                $("#CityId").empty();
                $("#CityId").append(response);
            }
        })
    })

    $("#CityId").change(function () {
        var cityId = $(this).val();
        //debugger
        $.ajax({
            type: "post",
            url: "/Home/GetAllLocalities?CityId=" + cityId,
            contentType: "html",
            success: function (response) {
                //debugger
                $("#LocalityId").empty();
                $("#LocalityId").append(response);
            }
        })
    })


    $("#HighestQualificationId").change(function () {
        var qualificationId = $(this).val();
        //debugger
        $.ajax({
            type: "post",
            url: "/Home/GetAllSpecializations?HighestQualificationId=" + qualificationId,
            contentType: "html",
            success: function (response) {
                //debugger
                $("#SpecializationId").empty();
                $("#SpecializationId").append(response);
            }
        })
    })

    $("#SpecializationId").change(function () {
        var specializationId = $(this).val();
        //debugger
        $.ajax({
            type: "post",
            url: "/Home/GetAllInstituteList?SpecializationId=" + specializationId,
            contentType: "html",
            success: function (response) {
                //debugger
                $("#InstituteId").empty();
                $("#InstituteId").append(response);
            }
        })
    })

    $("#CurrentEmployerId").change(function () {
        var currentEmployerId = $(this).val();
        //debugger
        $.ajax({
            type: "post",
            url: "/Home/GetPreviousEmployer?CurrentEmployerId=" + currentEmployerId,
            contentType: "html",
            success: function (response) {
                //debugger
                $("#PreviousEmployerId").empty();
                $("#PreviousEmployerId").append(response);
            }
        })
    })

  

})

$(document).ready(function () {
    $("#InstituteId").change(function () {
        var instituteId = $(this).val();

        if (instituteId == "0") {
            $("#OtherInstitute").prop('disabled', false);
        }
        else {
            $("#OtherInstitute").prop('disabled', true);
        }
    })
})

$(document).ready(function () {
    if ($("#InstituteId").val != "0") {
        $("#OtherInstitute").prop('disabled', true);
    }
    else if ($("#InstituteId").prop('disabled')) {
        $("#OtherInstitute").prop('disabled', false);
    }
})


var thisFile = document.getElementById('ResumePath');
$(thisFile).on('change', function () {
    //5242880=5MB
    if (this.files[0].size > 5242880 && this.files[0].type != "application/vnd.openxmlformats-officedocument.wordprocessingml.document" && this.files[0].type != "application/pdf") {
        $.notify("Please Upload the Resume in Pdf or docx formate with less than 5Mb file size", "error")
    }
    else if (this.files[0].size > 5242880) {
        $.notify("Please upload the Resume file which has less than 5MB size", "error");

    }
    else if (this.files[0].type != "application/vnd.openxmlformats-officedocument.wordprocessingml.document" && this.files[0].type != "application/pdf") {
        $.notify("Please Upload the Resume in pdf or docx format", "error")
    }
    else {
        $.notify("File uploaded Successfully", "success");
    }

});



















//$(document).ready(function () {
//    $(function () {
//        var todayDate = new Date();
//        var month = todayDate.getMonth() + 1;
//        var day = todayDate.getDate();
//        var year = todayDate.getFullYear();
//        if (month < 0) {
//            month = '0' + month.toString();
//        }
//        if (day < 10) {
//            day = '0' + day.toString();
//            var maxDate = day + '-' + month + '-' + year;
//            $('.txtDate').attr('max', maxDate);
//        }
//    })

//})


//$(function () {
//    var dtToday = new Date();

//    var month = dtToday.getMonth() + 1;// jan=0; feb=1 .......
//    var day = dtToday.getDate();
//    var year = dtToday.getFullYear() - 18;
//    if (month < 10)
//        month = '0' + month.toString();
//    if (day < 10)
//        day = '0' + day.toString();
//    var minDate = year + '-' + month + '-' + day;
//    var maxDate = year + '-' + month + '-' + day;
//    $('#DateofBirth').attr('max', maxDate);
//});

//$(function () {
//    var dtToday = new Date();

//    var month = dtToday.getMonth() + 1;// jan=0; feb=1 .......
//    var day = dtToday.getDate();
//    var year = dtToday.getFullYear() - 60;
//    if (month < 10)
//        month = '0' + month.toString();
//    if (day < 10)
//        day = '0' + day.toString();
//    var minDate = year + '-' + month + '-' + day;
//    var maxDate = year + '-' + month + '-' + day;
//    $('#DateofBirth').attr('min', minDate);
//});