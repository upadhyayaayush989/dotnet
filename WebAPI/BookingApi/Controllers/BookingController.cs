﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RabbitMQ.Client;
using System.Text;

namespace BookingApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookingController : ControllerBase
    {
        [Route("BookOrder")]
        [HttpPost]
        public ActionResult BookOrder()
        {
            //public void SendDataToExchange(string bookingMessage)
            //{
            //    string bookingMessage = "Order Placed Successfully!!";
            //    SendDataToExchange(bookingMessage);
            //    Byte[] bookingMessageInBytes = Encoding.UTF8.GetBytes(bookingMessage);
            //    ConnectionFactory connectionFactory = new ConnectionFactory();
            //    connectionFactory.Uri = new Uri("ampq://aayush:aayush@localhost:5672");
            //    var connection = connectionFactory.CreateConnection();
            //    var channel = connection.CreateModel();
            //    channel.ExchangeDeclare("Aayush-BookingExchange", ExchangeType.Fanout, true, false);
            //    channel.BasicPublish("Aayush-BookingExchange", "", null, bookingMessageInBytes);
            //    channel.Close();  
            //    connection.Close();
            //}
            

            return Ok("Success");
        }
        
        [Route("CancelOrder")]
        [HttpPost]
        public ActionResult CancelOrder()
        {
            return Ok("Success!!!");
        }

    }
}
