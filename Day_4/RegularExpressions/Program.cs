﻿using System.Text.RegularExpressions;

namespace RegularExpressions;


class Program
{
    public static void Main()
    {
         string mystr = "Today is thursday\n4th day of august 2022. day of the week is 4, session #4. Let us learn regular Expressions";

        Console.WriteLine(mystr);
        Match mymatch = Regex.Match(mystr, "day");
        Console.WriteLine(mymatch);

        Console.WriteLine("*******************");
        MatchCollection myMatches = Regex.Matches(mystr,"day",RegexOptions.IgnoreCase);
        Console.WriteLine($"The total Count: {myMatches.Count}");
        Console.WriteLine("*******************");
        MatchCollection mymatches1 = Regex.Matches(mystr, @"\d", RegexOptions.IgnoreCase);
        foreach(var item in mymatches1)
        {
            Console.WriteLine(item);
        }
        Console.WriteLine("******d{2,4}*******");
        mymatches1 = mymatches1 = Regex.Matches(mystr, @"\d{2,4}", RegexOptions.IgnoreCase);
        foreach (var item in mymatches1)
        {
            Console.WriteLine(item);
        }


    }
}















