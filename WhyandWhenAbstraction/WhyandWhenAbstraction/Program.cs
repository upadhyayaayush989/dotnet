﻿using WhyandWhenAbstraction;

FullTimeEmployee fte = new FullTimeEmployee()
{
    ID = 100,
    FirstName = "Aayush",
    LastName = "Upadhyay",
    AnnualSalary = 360000
};

Console.WriteLine(fte.GetFullName());
Console.WriteLine(fte.GetMonthlySalary());
Console.WriteLine("-----------------");


ContractEmployee cte = new ContractEmployee()
{
    ID = 200,
    FirstName = "Manoj",
    LastName = "Upadhyay",
    HourlyPay = 200,
    TotalHoursWorked = 40
};

Console.WriteLine(cte.GetFullName());
Console.WriteLine(cte.GetMonthlySalary());


