using User_API.Model;
using User_API.Repository;
using User_API.Service;

namespace UserAPI_tests
{
    public class UserApiTest:IClassFixture<UserDbFixture>
    {
        readonly IUserService _userService;
        readonly IUserRepository _userRepository;
        public UserApiTest(UserDbFixture userDbFixture)
        {
            _userRepository = new UserRepository(userDbFixture._userDbContext);
            _userService = new UserService(_userRepository);
        }
        [Fact]
        public void RegisterUser_ShouldRegisterUserIfNotAlreadyPresentAndReturnTrue()
        {
            //Arrange
            var expected = true;
            User user = new User() { Id = 2, UserName = "newUser", Password = "newUser", Email = "newUser@gmail.com", Location = "chennai", IsBlocked = false };
            //Act
            var actual = _userService.RegisterUser(user);
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LoginUser_ShouldReturnTheUserObjectIfSuccessfulLogin()
        {
            //Arrange
            User user = new User() { Id = 1, UserName = "aayush", Password = "aayush", Email = "aayushupadhyay989@gmail.com", Location = "Mumbai", IsBlocked = false };
            var expected = user;
            LoginUser loginUser = new LoginUser() { UserName = "aayush", Password = "aayush" };
            //Act
            var actual = _userService.LogIn(loginUser);
            //Assert
            Assert.Equal(expected.UserName, actual.UserName);
            Assert.Equal(expected.Password, actual.Password);
        }

    }
}