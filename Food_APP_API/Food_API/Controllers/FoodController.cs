﻿using Food_API.Model;
using Food_API.Repository;
using Food_API.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Food_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FoodController : ControllerBase
    {
        readonly IFoodService _foodService;
        readonly IFoodRepository _foodRepository;
        public FoodController(IFoodService foodService,IFoodRepository foodRepository)
        {
            _foodService=foodService;
            _foodRepository=foodRepository;
        }

        [Route("api/GetAllFood")]
        [HttpGet]
        public async Task<ActionResult> GetAllFood()
        {
            List<FoodItems> foodItems = await _foodService.GetAllFood();
            return Ok(foodItems);
        }

        [Route("api/AddFood")]
        [HttpPost]
        public ActionResult AddFood(FoodItems food)
        { 
            bool addFoodStatus = _foodService.AddFood(food);
            return Ok(addFoodStatus);
        }

        [Route("DeleteFood")]
        [HttpDelete]
        public ActionResult DeleteFood(int id)
        {
            bool foodDeleteStatus =  _foodService.DeleteFood(id);
            return Ok(foodDeleteStatus);
        }

        [Route("EditFood/{id:int}")]
        [HttpPut]
        public ActionResult EditFood(int id, FoodItems food)
        {
            bool userEditStatus = _foodService.EditFood(id, food);
            return Ok(userEditStatus);
        }

        [Route("GetFoodById/{id:int}")]
        [HttpGet]
        public ActionResult GetFoodById(int id)
        {
            var getFood = _foodRepository.GetFoodById(id)
;
            if (getFood == null)
            {
                return BadRequest($"Food with Id {id} not found");
            }
            else
                return Ok(getFood);
        }

    }
}
