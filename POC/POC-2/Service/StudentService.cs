﻿
using AutoMapper;
using Microsoft.CodeAnalysis;
using Microsoft.EntityFrameworkCore;
using POC_2.Models;
using StudentPersistantLayer;
using StudentPersistantLayer.Model;

namespace POC_2.Service
{
    public class StudentService : IStudentService
    {
        private readonly StudentDbContext _studentDbContext;
        private readonly IMapper _mapper;
        public StudentService(StudentDbContext productDbContext, IMapper mapper)
        {
            _studentDbContext = productDbContext;
            _mapper = mapper;
        }

        public bool AddStudent(Student student)
        {
            bool studentExists = StudentAlreadyExists(student);
            if (studentExists)
            {
                _studentDbContext.StudentDetails.Add(student);
                return _studentDbContext.SaveChanges() == 1 ? true : false;
            }
            else
                return false;
        }

        public void DeleteStudent(int studentId)
        {
            Student student = GetStudentById(studentId);
            _studentDbContext.StudentDetails.Remove(student);
            _studentDbContext.SaveChanges();
        }

        public bool EditStudent(Student updatedStudent)
        {
            Student student = _studentDbContext.StudentDetails.FirstOrDefault(u => u.StudentName == updatedStudent.StudentName);
            student.StudentName = updatedStudent.StudentName;
            student.StudentLocation = updatedStudent.StudentLocation;
            student.Password = updatedStudent.Password;
            _studentDbContext.StudentDetails.Update(student);
            _studentDbContext.SaveChanges();
            return true;
        }

        public List<StudentViewModel> GetAllStudents()
        {
            //List<ProductViewModel> productViewModel = _productDbContext.productTbl.ToList();

            List<Student> students = _studentDbContext.StudentDetails.ToList();
            var studentView = _mapper.Map<List<StudentViewModel>>(students);
            return studentView;
            
        }

        public Student GetStudentById(int studentId)
        {
            Student student = _studentDbContext.StudentDetails.Where(u => u.StudentId == studentId).FirstOrDefault();
            return student;
        }

        public bool StudentAlreadyExists(Student student)
        {
            Student studentExist = _studentDbContext.StudentDetails.Where(u => u.StudentName == student.StudentName).FirstOrDefault();
            if (studentExist != null)
                return false;
            else return true;
        }
    }
}
