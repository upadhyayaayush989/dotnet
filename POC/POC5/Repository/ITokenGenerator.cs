﻿using System.Security.Claims;

namespace POC5.Repository
{
    public interface ITokenGenerator
    {
        string GenerateToken(IEnumerable<Claim> claims);
        string GenerateRefreshToken();
        ClaimsPrincipal GetPricipalFromExpiredToken(string token);
    }
}
