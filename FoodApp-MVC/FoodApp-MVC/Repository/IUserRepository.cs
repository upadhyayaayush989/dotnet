﻿using FoodApp_MVC.Models;

namespace FoodApp_MVC.Repository
{
    public interface IUserRepository
    {
        bool RegisterUser(User user);
        User GetUserByUserName(string username);
        User LogIn(string username, string password);
        public List<FoodMenu> GetAllFood();

        bool AddFoodToCart(FoodCart item);
        void AddValues(int id);
    }
}
