﻿using BookingAPI.Context;
using BookingAPI.Models;
using Microsoft.EntityFrameworkCore;

namespace BookingAPI.Repository
{
    public class BookingRepository : IBookingRepository
    {
        static int ?GrandTotal;
        readonly BookingDbContext _bookingDbContext;
        public BookingRepository(BookingDbContext bookingDbContext)
        {
            _bookingDbContext = bookingDbContext;
        }

        /// <summary>
        /// Add The item to the Cart
        /// </summary>
        /// <param name="addItems"></param>
        public async Task <bool> AddItems(FoodCart addItems)
        {
            _bookingDbContext.addToCarts.Add(addItems);
            //_bookingDbContext.SaveChanges();
            addItems.Total =  addItems.Quantity * addItems.Price;
            //GrandTotal += addItems.Total;
            return await _bookingDbContext.SaveChangesAsync()==1?true:false;
            
        }

        /// <summary>
        /// Checking if the Item is already Present in the Cart for a Particular user
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public FoodCart CheckIfItemPresentForParticularUser(string productName,string userName)
        {
            return _bookingDbContext.addToCarts.Where(u => u.FoodName == productName&& u.Name == userName).FirstOrDefault();
        }

        /// <summary>
        /// Generate The bill
        /// </summary>
        /// <returns></returns>

        public async Task<int> GetTotalAmount(string userName)
        {
            int TotalPrice = 0;
            List<FoodCart> cartList = _bookingDbContext.addToCarts.Where(c => c.Name == userName).ToList();
            if (cartList != null)
            {
                foreach (var item in cartList)
                {
                    TotalPrice += (int)item.Total;
                }
                return TotalPrice;
            }
            return 0;
        }

        /// <summary>
        /// Get The Cart of a Particular user
        /// </summary>
        /// <returns></returns>

        public async Task< List<FoodCart>> GetCart(string userName)
        {
            return await _bookingDbContext.addToCarts.Where(cart=>cart.Name==userName).ToListAsync();
        }

        /// <summary>
        /// Check ifthe item is present to remove it
        /// </summary>
        /// <param name="removeItem"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        
        public async Task <bool> GetItemByCartId(int cartId)
        {
            FoodCart present = _bookingDbContext.addToCarts.Where(u => u.CartId==cartId).FirstOrDefault();
            if(present != null)
            {
                return  true;
            }
            return  false;
        }


        /// <summary>
        /// Paying the bill and emptying the cart
        /// </summary>
        public async Task <bool> PayBill(string userName)
        {
            List<FoodCart> cartList = _bookingDbContext.addToCarts.Where(cart => cart.Name == userName).ToList();
            if(cartList != null)
            {
                foreach (var item in cartList)
                {
                   _bookingDbContext.addToCarts.Remove(item);
                }
            }
            int checkChanges = await _bookingDbContext.SaveChangesAsync();
            if (checkChanges >= 1)
            {
                return true;
            }
            else
            {
                return false;
            }

        }


        /// <summary>
        /// If Quantity is greater than 1 then derease it else remove the item
        /// </summary>
        /// <param name="removeItem"></param>
        public async Task <bool> RemoveItem(int cartId)
        {
            FoodCart present = _bookingDbContext.addToCarts.Where(u =>u.CartId==cartId).FirstOrDefault();
            if(present.Quantity > 1)
            {
                present.Quantity -= 1;
                present.Total -= present.Price;
                return await _bookingDbContext.SaveChangesAsync()==1?true:false;
            }
            else
            {
                _bookingDbContext.Remove(present);
                return await _bookingDbContext.SaveChangesAsync()==1?true:false;
            }

        }

        /// <summary>
        /// If The Item is present Update the Quantity
        /// </summary>
        /// <param name="update"></param>
        public async Task< bool> UpdateQuantity(FoodCart foodItem)
        {
            FoodCart existingProductItem = _bookingDbContext.addToCarts.Where(u => u.Name == foodItem.Name && u.FoodName== foodItem.FoodName).FirstOrDefault();
            existingProductItem.Quantity += foodItem.Quantity;
            existingProductItem.Total = 0;
            existingProductItem.Total = existingProductItem.Quantity* foodItem.Price;  
            return await _bookingDbContext.SaveChangesAsync()==1?true:false;
            
        }
    }
}
