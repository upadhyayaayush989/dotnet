﻿using Admin_API.Exceptions;
using Admin_API.Model;
using Admin_API.Repository;

namespace Admin_API.Service
{
    public class AdminService:IAdminService
    {
        readonly IAdminRepository _adminRepository;
        public AdminService(IAdminRepository adminRepository)
        {
            _adminRepository = adminRepository;
        }
        public Admin LogIn(AdminLogin loginAdmin)
        {
            Admin admin = _adminRepository.LogIn(loginAdmin.UserName,loginAdmin.Password);
            if (admin != null)
            {

                return admin;
            }
            else
            {
                throw new AdminCredentialInValidException($"{loginAdmin.UserName} and details are invalid");
            }
        }
    }
}
