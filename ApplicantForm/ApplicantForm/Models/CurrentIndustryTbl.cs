﻿using System;
using System.Collections.Generic;

namespace ApplicantForm.Models
{
    public partial class CurrentIndustryTbl
    {
        public CurrentIndustryTbl()
        {
            ProfessionalDetailsTbls = new HashSet<ProfessionalDetailsTbl>();
        }

        public int CurrentIndustryId { get; set; }
        public string CurrentIndustryName { get; set; } = null!;

        public virtual ICollection<ProfessionalDetailsTbl> ProfessionalDetailsTbls { get; set; }
    }
}
