﻿namespace POC_1._2.Models
{
    public class FileUploadViewModel
    {
        public IFormFile XlsFile { get; set; }

        public StaffInfoViewModel StaffInfoViewModel { get; set; }
        public FileUploadViewModel()
        {
            StaffInfoViewModel = new StaffInfoViewModel();
        }
    }
}
