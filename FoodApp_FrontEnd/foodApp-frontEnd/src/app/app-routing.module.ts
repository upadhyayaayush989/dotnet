import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './authorize/auth.guard';
import { AddfoodComponent } from './components/admindashboard/addfood/addfood.component';
import { AdmindashboardComponent } from './components/admindashboard/admindashboard.component';
import { EditfoodComponent } from './components/admindashboard/editfood/editfood.component';
import { AdminloginComponent } from './components/adminlogin/adminlogin.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { GeneratebillComponent } from './components/dashboard/generatebill/generatebill.component';
import { ViewcartComponent } from './components/dashboard/viewcart/viewcart.component';
import { HomepageComponent } from './components/homepage/homepage.component';
import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';

const routes: Routes = [
  {path:'',component:HomepageComponent},
  {path:'login',component:LoginComponent},
  {path:'signup',component:SignupComponent},
  {path:'dashboard',component:DashboardComponent,canActivate:[AuthGuard]},
  {path:'admindashboard',component:AdmindashboardComponent},
  {path:'addfood',component:AddfoodComponent},
  {path:'editFood/:id',component:EditfoodComponent},
  {path:'viewCart',component:ViewcartComponent,canActivate:[AuthGuard]},
  {path:'generatebill',component:GeneratebillComponent,canActivate:[AuthGuard]},
  {path:'adminlogin',component:AdminloginComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
