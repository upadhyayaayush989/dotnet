﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FoodApp_MVC.Models
{
    public class FoodMenu
    {
        [Key,DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FoodId { get; set; }
        public string FoodName { get; set; }
        [Required]
        [ForeignKey("Categories")]
        public string FoodCategory { get; set; }
        public virtual Categories Categories { get; set; }
        public int Price { get; set; }
        public string ImgPath { get; set; }

    }
}
