﻿using System.Collections;

Console.WriteLine("Demo:: Queue");

Queue tickets = new Queue();

tickets.Enqueue("Ticket 1 ");
tickets.Enqueue("Ticket 2 ");
foreach(var item in tickets)
{
    Console.WriteLine(item);
}

Console.WriteLine($"Peek:: {tickets.Peek()}");
Console.WriteLine($"Count: {tickets.Count}");

tickets.Dequeue();

Console.WriteLine($"Count::{tickets.Count}");

