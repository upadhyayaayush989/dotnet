﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using POC_2.Models;
using POC_2.Service;
using StudentPersistantLayer;
using StudentPersistantLayer.Model;
using System.Diagnostics;

namespace POC_2.Controllers
{
    public class HomeController : Controller
    {
        readonly IMapper _mapper;
        readonly StudentDbContext _dbContext;
        readonly IStudentService _studentService;


        public HomeController(IMapper mapper,IStudentService studentService)
        {
            _mapper=mapper;
            _studentService=studentService;
        }

        [HttpGet]
        public IActionResult AddStudent()
        {
            return View();
        }

        [HttpPost]
        public IActionResult AddStudent(Student student)
        {
            bool addStatus = _studentService.AddStudent(student);
            if (addStatus)
            {
                return RedirectToAction("GetAllStudents");
            }
            else
            {
                return View();
            }
        }

        [HttpGet]
        public IActionResult EditProduct(int id)
        {
            Student student = _studentService.GetStudentById(id)
;
            return View(student);
        }
        [HttpPost]
        public IActionResult EditProduct(Student student)
        {
            bool editStatus = _studentService.EditStudent(student);
            if (editStatus)
                return RedirectToAction("GetAllProducts");
            else return View();
        }


        public IActionResult DeleteStudent(int id)
        {
            _studentService.DeleteStudent(id);
            return RedirectToAction("GetAllStudents");
        }

        public IActionResult GetAllStudents()
        {
            List<StudentViewModel> students = _studentService.GetAllStudents();
            return View(students);
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

       
    }
}