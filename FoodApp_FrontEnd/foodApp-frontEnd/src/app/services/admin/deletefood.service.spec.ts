import { TestBed } from '@angular/core/testing';

import { DeletefoodService } from './deletefood.service';

describe('DeletefoodService', () => {
  let service: DeletefoodService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DeletefoodService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
