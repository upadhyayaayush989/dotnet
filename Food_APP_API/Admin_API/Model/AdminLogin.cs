﻿namespace Admin_API.Model
{
    public class AdminLogin
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
