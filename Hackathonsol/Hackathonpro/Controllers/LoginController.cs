﻿using Hackathonpro.Models;
using Hackathonpro.Services;
using Microsoft.AspNetCore.Mvc;

namespace Hackathonpro.Controllers
{
    public class LoginController : Controller
    {   
        readonly IUserServices _userServices;
        public LoginController(IUserServices userServices)
        {
            _userServices = userServices;
        }
        public IActionResult LoginOrSignUp()
        {
            return View();
        }
        [HttpGet]
        public IActionResult Login()
        {
            LoginDet loginDet = new LoginDet();
            return View();
        }
        [HttpPost]
        public IActionResult Login(LoginDet loginDet)
        {
            bool ver = _userServices.VerifyUser( loginDet.UserName, loginDet.Password);
            if (ver)
            {
                return RedirectToAction("DashBoard", "Home");
            }
            return View();

        }
        [HttpGet]
        public IActionResult SignUp()
        {
            return View();

        }

        [HttpPost]
        public ActionResult SignUp(User user)
        {

            bool verifiedVal = _userServices.AddUser(user.CustName, user);

            if(verifiedVal == true)
            {
                return View("LogIn");
            }
            else return View("LoginOrSignUp");
        }
    }
}
