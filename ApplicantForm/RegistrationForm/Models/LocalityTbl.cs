﻿using System;
using System.Collections.Generic;

namespace RegistrationForm.Models
{
    public partial class LocalityTbl
    {
        public LocalityTbl()
        {
            PersonalDetailTbls = new HashSet<PersonalDetailTbl>();
        }

        public int CityId { get; set; }
        public int LocalityId { get; set; }
        public string LocalityName { get; set; } = null!;

        public virtual CityTbl City { get; set; } = null!;
        public virtual ICollection<PersonalDetailTbl> PersonalDetailTbls { get; set; }
    }
}
