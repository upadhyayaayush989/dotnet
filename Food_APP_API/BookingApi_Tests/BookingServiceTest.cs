using BookingAPI.Context;
using BookingAPI.Models;
using BookingAPI.Repository;
using BookingAPI.Services;
using Microsoft.Extensions.Configuration;

namespace BookingApi_Tests
{
    public class BookingServiceTest:IClassFixture<BookingDbFixture>
    {
        readonly IBookingService _bookingService;
        readonly IBookingRepository _bookingRepository;
        readonly IConfiguration _configuration;
        readonly BookingDbContext _bookingDbContext;
        public BookingServiceTest(BookingDbFixture bookingDbFixture)
        {
            _bookingRepository = new BookingRepository(bookingDbFixture._bookingDbContext);
            _bookingService = new BookingService(_bookingRepository,_configuration);
            _bookingDbContext = bookingDbFixture._bookingDbContext;



        }
        [Fact]
        public async Task AddItemsToCart_ShouldReturnTrue()
        {
            var expected = true;
            FoodCart foodCart = new FoodCart() { CartId = 1, FoodName = "Pulao2", FoodDiscription = "Pulao Rice", FoodType = "Veg", Price = 200, ImagePath = "string", Name = "aayush", Quantity = 1, Total = 200 };
            var actual = await _bookingService.AddItemsToCart(foodCart);
            Assert.Equal(expected, actual);
        }


        [Fact]
        public async Task DeleteItem_ShouldReturnTrue()
        {
            var excepted = true;
            var id = 1;
            var actual = await _bookingService.RemoveItem(id);
            Assert.Equal(excepted, actual);
        }

        [Fact]
        public async Task GetCartItems_ShouldReturnTheCartItemsFortheUser()
        {
            var expected = 1;
            var username = "aayush";
            var actual = await _bookingService.ViewCartItems(username);
            Assert.Equal(expected, actual.Count());
        }

        [Fact]
        public async Task GenerateBill_ShouldReturnAnInteger()
        {
            var expected = 200;
            var username = "aayush";
            var actual = await _bookingService.GenerateBill(username);
            Assert.Equal(expected, actual);
        }


        [Fact]
        public async Task PayBill_ShouldReturnTrue()
        {
            var expected = true;
            var username = "aayush";
            var actual = await _bookingService.PayBill(username);
            Assert.Equal(expected,actual);
        }



    }
}