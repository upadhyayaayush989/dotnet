import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user';
import { LoginRegisterService } from 'src/app/services/user/login-register.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
user:User
  constructor(private userService:LoginRegisterService,private router:Router) {
    console.log('within register user');
    this.user=new User();
    
   }

  ngOnInit(): void {
  }

  RegisterUser(){
    this.user.isBlocked=false;
    console.log(this.user);
    
    this.userService.RegisterUser(this.user).subscribe(res=>{
      if(res){
        console.log('registration success');
        Swal.fire(
          'User Registration',
          'Registration Success',
          'success'
        )
        this.router.navigate(['login']);
      }
      else{
        console.log('failed');
        
      }
    })
  }

}
