﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebAppFramework_Day13.Model;

namespace WebAppFramework_Day13.Repository
{
    public static class UserRepository
    {
        //1st create a list 
        //2nd - add user registered in the list 
        //3rd - Validation for userName and Password
        static List<User> users = new List<User>();


        public static  User GetUserByName(string userName)
        {
            return users.Find(u => u.Name == userName);
        }
        public static string AddUser(User user)
        {
            users.Add(user);
            return "success";
        }

    }
}