﻿using FoodApp_Mvc.Models;
using FoodApp_Mvc.Models.Account;
using Microsoft.EntityFrameworkCore;

namespace FoodApp_Mvc.Data
{
    public class ApplicationContext:DbContext
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options) { }
        public DbSet<Food> FoodMenu { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<FoodCart> FoodCart { get; set; }
    }
}
