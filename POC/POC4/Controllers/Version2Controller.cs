﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace POC4.Controllers
{
    
    [ApiController]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiVersion("2.0")]
    public class Version2Controller : ControllerBase
    {
        [HttpGet]
        
        public IActionResult GetInfo()
        {
            return Ok("V2 Successful");
        }
    }
}
