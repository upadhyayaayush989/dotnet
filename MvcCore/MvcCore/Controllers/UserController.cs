﻿using Microsoft.AspNetCore.Mvc;
using MvcCore.Models;
using MvcCore.Repository;

namespace MvcCore.Controllers
{
    public class UserController : Controller
    {
        //User user = new User() { Id = 1, Name = "user1", Password = "user1", Location = "Mumbai" };
        //public ActionResult Index()
        //{
        //    return View(user);
        //}
        //-----------------------------------------------
        //create interface first
        //try to create an identifier for your interface
        //-----------------------------------------------
        readonly IUserRepository _iuserrepo;
        //readonly Simplefunc _simplefunc;
        //-------------------------------------------------------
        //constructor and pass IUserRepository in the constructor
        //-------------------------------------------------------
        public UserController(IUserRepository iuserrepo)
        {
            //_simplefunc = simplefunc;
            _iuserrepo = iuserrepo;
        }
        //IUserRepository iuserrepo = (IUserRepository)new UserRepository();
        public ActionResult GetAllUsers()
        {
            //ViewBag.UserControllerGuid = _iuserrepo.ToString();
            //ViewBag.TotalGuid = _simplefunc.ToString(); 
            List<User> users = _iuserrepo.GetAllUsers();
            return View(users);
        }
        [HttpGet]
        public ActionResult AddUser()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddUser(User user)
        {
            bool addUserStatus = _iuserrepo.AddUsers(user);
            return RedirectToAction("GetAllUsers");
        }

        public ActionResult DeleteUser(int id)
        {
            _iuserrepo.DeleteUser(id);
            return RedirectToAction("GetAllUsers");
        }

    }
}
