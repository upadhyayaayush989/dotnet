﻿using System;
using System.Collections.Generic;

namespace RegistrationForm.Models
{
    public partial class StateTbl
    {
        public StateTbl()
        {
            CityTbls = new HashSet<CityTbl>();
            PersonalDetailTbls = new HashSet<PersonalDetailTbl>();
        }

        public int StateId { get; set; }
        public string? StateName { get; set; }
        public int CountryId { get; set; }

        public virtual CountryTbl Country { get; set; } = null!;
        public virtual ICollection<CityTbl> CityTbls { get; set; }
        public virtual ICollection<PersonalDetailTbl> PersonalDetailTbls { get; set; }
    }
}
