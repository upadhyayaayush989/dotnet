﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="WebAppFramework_Day13.Register1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">

        .auto-style4 {
            width: 100%;
        }
        .auto-style6 {
            height: 33px;
            width: 15px;
        }
        .auto-style3 {
            height: 33px;
            width: 128px;
        }
        .auto-style5 {
            width: 128px;
        }
        .auto-style7 {
            width: 15px;
        }
        </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
        <div>
            <table class="auto-style4">
                <tr>
                    <td class="auto-style6">
                        <asp:Label ID="Label1" runat="server" Text="UserName"></asp:Label>
                    </td>
                    <td class="auto-style3">
                        <asp:TextBox ID="txtUsername" runat="server"></asp:TextBox>
                    </td>
                    
                </tr>
                <tr>
                    <td class="auto-style7">
                        <asp:Label ID="Label2" runat="server" Text="Password"></asp:Label>
                    </td>
                    <td class="auto-style5">
                        <asp:TextBox ID="txtPassword" runat="server" TextMode="Password"></asp:TextBox>
                    </td>
                                        
                </tr>
                <tr>
                    <td class="auto-style7">
                        <asp:Label ID="Label3" runat="server" Text="CnfPassword"></asp:Label>
                    </td>
                    <td class="auto-style5">
                        <asp:TextBox ID="cnfPassword" runat="server" TextMode="Password"></asp:TextBox>
                    </td>
                                        
                </tr>
                <tr>
                    <td class="auto-style7">
                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                    </td>
                    <td class="auto-style5">
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" />
                    </td>
                    
                </tr>
            </table>
        </div>
    &nbsp;</div>
        <div>
        </div>
    &nbsp;</form>
    <p>
&nbsp;
        </p>
        <p>
&nbsp;&nbsp;
        </p>
        </body>
</html>
