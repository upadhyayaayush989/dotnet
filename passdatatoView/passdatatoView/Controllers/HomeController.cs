﻿using Microsoft.AspNetCore.Mvc;
using passdatatoView.Models;
using System.Diagnostics;

namespace passdatatoView.Controllers
{
    public class HomeController : Controller
    {
       //pass data from controller to view 
       //view data --- controller base 
       //viewbag
       //tempdata 

        public ActionResult Index()
        {
            ViewData["data1"] = "data comes from ViewData";//dictionary type
            ViewBag.data2 = "data comes from ViewBag";
            TempData["data3"] = "data comes from TempData";

            return View();
        }
        public ActionResult About()
        {
            TempData.Keep("data3");
            return View();
        }

        public ActionResult Contact()
        {
            TempData.Keep();
            return View();
        }

    }
}