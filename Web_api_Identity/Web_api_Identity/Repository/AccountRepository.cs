﻿using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Web_api_Identity.Model;

namespace Web_api_Identity.Repository
{
    public class AccountRepository: IAccountRepository
    {
        //inbuilt user manager class

        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IConfiguration _configuration;
        public AccountRepository(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, IConfiguration configuration)
        {
            _userManager=userManager;
            _signInManager=signInManager;
            _configuration=configuration;
        }
        
        public async Task<IdentityResult> RegisterUser(RegisterModel registerModel)
        {
            var user = new ApplicationUser()
            {
                Email = registerModel.Email,
                UserName = registerModel.UserName,

            };
            return await _userManager.CreateAsync(user,registerModel.Password);
        }
        public async Task<string> LoginUser(LoginModel loginModel)
        {
            var result = await _signInManager.PasswordSignInAsync(loginModel.UserName, loginModel.Password, false, false);
            if (!result.Succeeded)
            {
                return null;
            }
            else
            {
                var authclaims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, loginModel.UserName),
                    new Claim(JwtRegisteredClaimNames.Jti,Guid.NewGuid().ToString()),
                };

                var authSigninKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JwtValidationDetails:UserSecretKey"]));
                var token = new JwtSecurityToken(
                    issuer: _configuration["JwtValidationDetails:UserIssuer"],
                    audience: _configuration["JwtValidationDetails:UserAudience"],
                    expires:DateTime.Now.AddMinutes(10),
                    claims: authclaims,
                    signingCredentials: new SigningCredentials(authSigninKey, SecurityAlgorithms.HmacSha256)
                    );
                return new JwtSecurityTokenHandler().WriteToken(token);
            }
        }

    }
}
