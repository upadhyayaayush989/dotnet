﻿using FoodApp_Mvc.Data;
using FoodApp_Mvc.Models.Account;
using FoodApp_Mvc.Models.ViewModel;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace FoodApp_Mvc.Controllers.Account
{
    public class AccountController : Controller
    {
        readonly ApplicationContext _db;
        public AccountController(ApplicationContext db)
        {
            _db = db;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Login(LoginSignupViewModel login)
        {
            if (ModelState.IsValid)
            {
                var loginData = _db.Users.Where(e => e.UserName == login.UserName).SingleOrDefault();
                if (loginData != null)
                {
                    bool isPresent = loginData.UserName == login.UserName && loginData.Password == login.Password;
                    if (isPresent)
                    {
                        var identity = new ClaimsIdentity(new[] { new Claim(ClaimTypes.Name, login.UserName) },
                            CookieAuthenticationDefaults.AuthenticationScheme);
                        var principle = new ClaimsPrincipal(identity);
                        HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principle);
                        HttpContext.Session.SetString("UserName", login.UserName);
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        TempData["errorPassword"] = "Invalid Password";
                        return View(login);
                    }
                }
                else
                {
                    TempData["errorUsername"] = "Username Not found";
                    return View(login);
                }
            }
            else
            {
                return View(login);
            }


        }



        public IActionResult Logout()
        {
            HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            var storedCookies = Request.Cookies.Keys;
            foreach (var cookies in storedCookies)
            {
                Response.Cookies.Delete(cookies);
            }
            return RedirectToAction("Login", "Account");

        }


        [AcceptVerbs("Post", "Get")]
        public IActionResult UserNameExists(string username)
        {
            var data = _db.Users.Where(u => u.UserName == username).SingleOrDefault();
            if (data != null)
            {
                return Json($"Username {username} already in use");
            }
            else
            {
                return Json(true);
            }

        }


        public IActionResult SignUp()
        {
            return View();
        }
        [HttpPost]
        public IActionResult SignUp(SignUpUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                var data = new User()
                {
                    UserName = model.UserName,
                    Email = model.Email,
                    Password = model.Password,
                    Mobile = model.Mobile,
                    IsActive = model.IsActive
                };
                _db.Add(data);
                _db.SaveChanges();
                TempData["successMessage"] = "You are Registered Now, You can Login using your Credentials";
                return RedirectToAction("Login");
            }
            else
            {
                TempData["errorMessage"] = "Empty Form Can't be submitted";
                return View(model);
            }
        }
    }
}
