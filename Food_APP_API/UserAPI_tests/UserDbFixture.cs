﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using User_API.Context;
using User_API.Model;

namespace UserAPI_tests
{
    public class UserDbFixture
    {
        internal UserDbContext _userDbContext;
        public UserDbFixture()
        {
            var userDbContextOptions = new DbContextOptionsBuilder<UserDbContext>().UseInMemoryDatabase("UserDb").Options;
            _userDbContext = new UserDbContext(userDbContextOptions);
            _userDbContext.Add(new User() { Id = 1, UserName = "aayush", Password = "aayush", Email = "aayushupadhyay989@gmail.com", Location = "Mumbai", IsBlocked = false });
            _userDbContext.SaveChanges();
        }
    }
}
