﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using Microsoft.EntityFrameworkCore;

namespace ApplicantForm.Models.ViewModel
{
    [Keyless]
    public class ApplicantViewModel
    {

        #region LogInDetails
        [Required(AllowEmptyStrings = false, ErrorMessage = "Field is required")]
        [RegularExpression(".+@.+", ErrorMessage = "Field must include exactly 1 '@'")]
        [MaxLength(254, ErrorMessage = "Field can be at most 254 characters long")]
        [DataType(DataType.EmailAddress)]
        [DisplayName("Email")]
        public string Email { get; set; }

        [RegularExpression(".+@.+", ErrorMessage = "Field must include exactly 1 '@'")]
        [MaxLength(254, ErrorMessage = "Field can be at most 254 characters long")]
        [DataType(DataType.EmailAddress)]
        [DisplayName("Alternate Email")]
        public string? AlternateEmail { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Field is required")]
        [RegularExpression("^[A-Za-z]+$", ErrorMessage = "Field can only contain alphabets")]
        [MaxLength(50, ErrorMessage = "Field can be at most 50 characters long")]
        [DisplayName("First Name")]
        public string FirstName { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Field is required")]
        [RegularExpression("^[A-Za-z]+$", ErrorMessage = "Field can only contain alphabets")]
        [MaxLength(50, ErrorMessage = "Field can be at most 50 characters long")]
        [DisplayName("Last Name")]
        public string LastName { get; set; }


        [Required, RegularExpression("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,12}$", ErrorMessage = $"Please enter the Password in Proper formate\nPassword should contain aleast one Uppercase ,lowercase and atleast one spacial character !@#$%^&*() ")]
        [MinLength(8, ErrorMessage = "Field must be atleast 8 characters long")]
        [MaxLength(128, ErrorMessage = "Field can be  128 characters long")]

        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Field is required")]
        [Compare("Password", ErrorMessage = "Passwords don't match")]
        [DataType(DataType.Password)]
        [DisplayName("Confirm Password")]
        public string ConfirmPassword { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Field is required")]
        [DisplayName("Looking For")]
        //dropdown
        public string WorkTypeId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Field is required")]
        [DisplayName("Your Status")]
        //dropdown
        public int StatusId { get; set; }

        #endregion

        #region PersonalDetails

        [DisplayName("Phone Number")]
        [RegularExpression(@"^[0-9]{11}$", ErrorMessage = "Only Numbers allowed")]
        [MinLength(11, ErrorMessage = "Field can be at most 11 digits")]
        [MaxLength(11, ErrorMessage = "Field can be at most 11 digits")]
        public string? PhoneNumber { get; set; }



        [DisplayName("Mobile")]
        [RegularExpression(@"^[0-9]{10}$", ErrorMessage = "Only Numbers allowed")]
        [MinLength(10, ErrorMessage = "Field can be at most 10 digits")]
        [MaxLength(10, ErrorMessage = "Field can be at most 10 digits")]
        public string? MobileNumber { get; set; }

        #region DateOfBirth
        [Required(AllowEmptyStrings = false, ErrorMessage = "Field is required")]
        [DataType(DataType.Date)]
        [DateOfBirth(MinAge = 18, MaxAge = 59)]
        public DateTime DateofBirth { get; set; }

        public class DateOfBirthAttribute : ValidationAttribute
        {
            public int MinAge { get; set; }
            public int MaxAge { get; set; }

            public override bool IsValid(object value)
            {
                if (value == null)
                    return true;

                var val = (DateTime)value;

                if (val.AddYears(MinAge) > DateTime.Now)
                    return false;

                return (val.AddYears(MaxAge) > DateTime.Now);
            }
        }
        #endregion

        [DisplayName("Gender")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Field is required")]
        //dropdown
        public int GenderId { get; set; }

        [DisplayName("Marital")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Field is required")]
        //dropdown
        public int MaritalStatusId { get; set; }

        [DisplayName("Country")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Field is required")]
        //dropdown
        public int CountryId { get; set; }

        [DisplayName("State")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Field is required")]
        //dropdown
        public int StateId { get; set; }

        [DisplayName("District")]
        [MaxLength(50, ErrorMessage = "Field can be at most 50 characters long")]
        public string? District { get; set; }

        [DisplayName("City")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Field is required")]
        //dropdown
        public int CityId { get; set; }

        [DisplayName("Locality")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Field is required")]
        //dropdown
        public int LocalityId { get; set; }

        [DisplayName("ZipCode")]
        public int? ZipCode { get; set; }

        #endregion

        #region HighestQualificationDetails

        [Required(AllowEmptyStrings = false, ErrorMessage = "Field is required")]
        [DisplayName("Highest Qualification")]
        //dropdown
        public int HighestQualificationId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Field is required")]
        //dropdown
        public int SpecializationId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Field is required")]
        //dropdown
        public int InstituteId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Field is required")]
        [MaxLength(30, ErrorMessage = "Field can be at most 30 characters long")]
        public string? OtherInstitute { get; set; }

        #endregion

        #region ProfessionalDetails

        [DisplayName("Preferred Location")]
        public int? PreferedLocationId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Field is required")]
        [DisplayName("Are you ready to Relocate ?")]
        public bool RelocationStatus { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Field is required")]
        [Range(0, 30, ErrorMessage = "Experience must be between 0 and 30")]
        [DisplayName("Experience in Years")]
        //dropdown
        public int TotalExperienceInYears { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Field is required")]
        [Range(0, 11, ErrorMessage = "Experience must be between 0 and 11")]
        [DisplayName("Experience in Months")]
        //dropdown
        public int TotalExperienceInMonths { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Field is required")]
        [DisplayName("Job Category")]
        public int JobCategoryId { get; set; }
        public int? EmployeeId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Field is required")]
        public string? KeySkills { get; set; }

        [DisplayName("Current Industry")]
        //dropdown
        public int? CurrentIndustryId { get; set; }

        [DisplayName("Current Employer")]
        public string? CurrentEmployer { get; set; }

        [DisplayName("Current Designation")]
        public string? CurrentDesignation { get; set; }

        [DisplayName("Previous Employer ")]
        public string? PreviousEmployer { get; set; }

        [DisplayName("Previous Designation ")]
        public string? PreviousDesignation { get; set; }

        [DisplayName("Notice Period")]
        //dropdown
        public int? NoticePeriod { get; set; }

        [DisplayName("Cost To Company (C.T.C.)")]
        public int? CtcinLakh { get; set; }
        public int? CtcinThousand { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Field is required")]
        [DisplayName("Resume Title")]
        public string? ResumeTitle { get; set; }

        [DisplayName("Attach Your Resume")]
        public string? ResumePath { get; set; }

        [DisplayName("Type In Your Resume")]
        public string? CopyResume { get; set; }
        #endregion
    }
}
