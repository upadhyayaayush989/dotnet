﻿namespace FoodApp_MVC.Exceptions
{
    public class UserCredentialInvalidException:ApplicationException
    {
        public UserCredentialInvalidException()
        {

        }
        public UserCredentialInvalidException(string msg) : base(msg)
        {

        }
    }
}
