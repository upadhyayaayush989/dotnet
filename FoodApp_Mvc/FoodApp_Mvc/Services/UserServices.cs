﻿using FoodApp_Mvc.Models;
using FoodApp_Mvc.Repository;

namespace FoodApp_Mvc.Services
{
    public class UserServices:IUserServices
    {
        IUserRepository _userRepo;
        public UserServices(IUserRepository userRepo)
        {
            _userRepo = userRepo;
        }

        public void AddValues(int id)
        {
            _userRepo.AddValuesToCart(id);
        }
        public void DeleteItemFromCart(int id)
        {
            _userRepo.DeleteElement(id);
        }

        public int GenerateBill()
        {
            return _userRepo.GenerateBill();
        }

        public List<FoodCart> GetCart()
        {
            return _userRepo.GetCart();
        }
        public List<Food> GetProducts()
        {
            return _userRepo.GetAllProducts();
        }

        public void RemoveItems()
        {
            _userRepo.RemoveElements();
        }
    }
}
