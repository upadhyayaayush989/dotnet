﻿using Food_API.Model;
using Microsoft.EntityFrameworkCore;

namespace Food_API.Context
{
    public class FoodDbContext:DbContext
    {
        public FoodDbContext(DbContextOptions<FoodDbContext>options):base(options)
        {

        }
        public DbSet<FoodItems> FoodItems { get; set; }
    }
}
