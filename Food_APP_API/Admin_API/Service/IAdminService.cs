﻿using Admin_API.Model;

namespace Admin_API.Service
{
    public interface IAdminService
    {
        Admin LogIn(AdminLogin loginAdmin);
    }
}
