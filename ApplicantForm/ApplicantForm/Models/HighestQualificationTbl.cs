﻿using System;
using System.Collections.Generic;

namespace ApplicantForm.Models
{
    public partial class HighestQualificationTbl
    {
        public HighestQualificationTbl()
        {
            EducationalDetailTbls = new HashSet<EducationalDetailTbl>();
            SpecializationTbls = new HashSet<SpecializationTbl>();
        }

        public int HighestQualificationId { get; set; }
        public string QualificationName { get; set; } = null!;

        public virtual ICollection<EducationalDetailTbl> EducationalDetailTbls { get; set; }
        public virtual ICollection<SpecializationTbl> SpecializationTbls { get; set; }
    }
}
