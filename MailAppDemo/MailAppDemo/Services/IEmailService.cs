﻿using MailAppDemo.Model;

namespace MailAppDemo.Services
{
    public interface IEmailService
    {
        void SendEmail(EmailDetails request,string userEmail);
    }
}
