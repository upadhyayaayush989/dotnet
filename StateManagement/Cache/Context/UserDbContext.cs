﻿using Microsoft.EntityFrameworkCore.SqlServer;
using Microsoft.EntityFrameworkCore;
using Cache.Models;

namespace Cache.Context
{
    public class UserDbContext:DbContext
    {
        public UserDbContext(DbContextOptions<UserDbContext> context):base(context) 
        {

        }
        public DbSet<User> UserList { get; set; }
    }
}
