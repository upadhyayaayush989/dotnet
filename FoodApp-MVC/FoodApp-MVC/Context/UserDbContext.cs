﻿using FoodApp_MVC.Models;
using Microsoft.EntityFrameworkCore;

namespace FoodApp_MVC.Context
{
    public class UserDbContext:DbContext
    {
        public UserDbContext(DbContextOptions<UserDbContext> options ):base(options)
        {

        }
       public DbSet<User> UserCredentials { get; set; }
        public DbSet<Categories> Categories { get; set; }
       public DbSet<FoodMenu> FoodMenuTable { get; set; }
       public DbSet<FoodCart> FoodCart { get; set; }
    }
}
