﻿using Collections.Exceptions;
using Collections.Model;
using Collections.Repository;



ProductRepository productRepository = new ProductRepository();

//Get all products from the list 

List<Product> productList = productRepository.GetAllProducts();

foreach (Product item in productList)
{
    Console.WriteLine(item);
}

//Add product to the list   

Console.WriteLine("Product list after addition of the new product");

//create Product object

Product product = new Product() { Id=3,Name="LAPTOP",Price=60000};

try
{
    productRepository.AddProduct(product);

    foreach (Product item in productList)
    {
        Console.WriteLine(item);
    }
}
catch(ProductAlreadyExistsException paex)
{
    Console.WriteLine(paex.Message);
}

//Delete a Product

Console.WriteLine("Delete product");

Console.WriteLine("Enter The product to be deleted");

var itemTobeDeleted = Console.ReadLine();
Console.WriteLine(productRepository.DeleteProductByName(itemTobeDeleted));

foreach(Product item in productList)
{
    Console.WriteLine(item);
}