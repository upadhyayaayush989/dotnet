﻿using MvcCore.Repository;

namespace MvcCore.Models
{
    public class Simplefunc
    {
        public IUserRepository UserRepository { get; set; }
        public Simplefunc(IUserRepository repo)
        {
            UserRepository = repo;
        }
        public int Total => UserRepository.GetAllUsers().Count();
    }
}
