﻿using Microsoft.AspNetCore.Identity;

namespace Web_api_Identity.Model
{
    public class ApplicationUser:IdentityUser
    {
        public string? City { get; set; }
    }
}
