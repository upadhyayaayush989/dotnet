﻿using System;
using System.Collections.Generic;

namespace ApplicantForm.Models
{
    public partial class CountryTbl
    {
        public CountryTbl()
        {
            PersonalDetailTbls = new HashSet<PersonalDetailTbl>();
            StateTbls = new HashSet<StateTbl>();
        }

        public int CountryId { get; set; }
        public string CountryName { get; set; } = null!;

        public virtual ICollection<PersonalDetailTbl> PersonalDetailTbls { get; set; }
        public virtual ICollection<StateTbl> StateTbls { get; set; }
    }
}
