﻿using BookingAPI.Context;
using BookingAPI.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookingApi_Tests
{
    public class BookingDbFixture
    {
        internal BookingDbContext _bookingDbContext;
        public BookingDbFixture()
        {
            var bookingDbContextOptions = new DbContextOptionsBuilder<BookingDbContext>().UseInMemoryDatabase("BookingDb").Options;
            _bookingDbContext = new BookingDbContext(bookingDbContextOptions);
            _bookingDbContext.Add(new FoodCart() {CartId=1,FoodName="Pulao",FoodDiscription="Pulao Rice",FoodType="Veg",Price=200,ImagePath="string",Name="aayush",Quantity=1,Total=200 });
            _bookingDbContext.SaveChanges();
        }
    }
}
