using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Mvc;
using POC4.Context;
using POC4.Model;
using Serilog;
namespace POC4.Controllers
{
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiVersion("1.0")]
    [ApiController]
    
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
        "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
    };

        private readonly ILogger<WeatherForecastController> _logger;
        private readonly IDataProtector _dataProtector;
        private readonly UserDbContext _userDbContext;

        public WeatherForecastController(ILogger<WeatherForecastController> logger, IDataProtectionProvider dataProtector, UserDbContext userDbContext)
        {
            _logger = logger;
            _dataProtector = dataProtector.CreateProtector("");
            _userDbContext = userDbContext;
        }

        //[ApiVersion("1")]
        //[Route("api/v{version:apiVersion}/[controller]")]
        [HttpGet(Name = "GetWeatherForecast")]
        
        public IEnumerable<WeatherForecast> Get()
        {
            Log.Information($"Get forecast called at {DateTime.Now}");
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = Random.Shared.Next(-20, 55),
                Summary = Summaries[Random.Shared.Next(Summaries.Length)]
            })
            .ToArray();
        }

        [Route("LogIn")]
        [HttpPost]
        public IActionResult Login(User user)
        {
            bool addStatus = false;
            if (ModelState.IsValid)
            {
                user.Password = _dataProtector.Protect(user.Password);
                //user.Password = _dataProtector.CreateProtector(user.Password).ToString();
                _userDbContext.Users.Add(user);
                addStatus = _userDbContext.SaveChanges() == 1 ? true : false;
                Log.Information($"{user.Name} logged in at {DateTime.Now}");
                string decodePassword = _dataProtector.Unprotect(user.Password);
                Console.WriteLine(decodePassword);
            }
            return Ok(addStatus);
        }


    }
}