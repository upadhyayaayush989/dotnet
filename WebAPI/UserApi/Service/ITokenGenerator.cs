﻿using System.Security.Claims;

namespace UserApi.Service
{
    public interface ITokenGenerator
    {
        string GenerateToken(int id , string name);
        public bool IsTokenValid(string userSecretKey, string userIssuer, string userAudience, string userToken);
        //string GenerateRefreshToken();
        //ClaimsPrincipal GetPrincipalFromExpiredToken(string token);
    }
}
