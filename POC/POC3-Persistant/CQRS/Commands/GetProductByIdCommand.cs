﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using POC3_Persistant.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POC3_Persistant.CQRS.Commands
{
    public class GetProductByIdCommand:IRequest<Product>
    {
        public int Id { get; set; }

        public class GetProductByIdHandler : IRequestHandler<GetProductByIdCommand, Product>
        {
            private ProductDbContext _context;
            public GetProductByIdHandler(ProductDbContext context)
            {
                _context = context;
            }

            public async Task<Product> Handle(GetProductByIdCommand command, CancellationToken cancellationToken)
            {
                var product = await _context.Products.Where(p => p.Id == command.Id).FirstOrDefaultAsync();
                return product;
            }
        }
    }
}
