﻿using Day5.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day5.Repository
{
    internal class UserRepository : IUserRepository,IFile
    {
        List<User> users;
        //Constructor

        public UserRepository()
        {
            users = new List<User>();
        }

        public bool IsUsernameExists(string userName, string filename)
        {
            bool isUserAvaiable = false;
            using (StreamReader sr = new StreamReader(filename))
            {
                string rowLine;
                while ((rowLine = sr.ReadLine()) != null)
                {
                    string[] rowSplittedValues= rowLine.Split(',');
                    foreach(string userIndividual in rowSplittedValues)
                    {
                        if (userIndividual == userName)
                        {
                            isUserAvaiable = true;
                            break;
                        }
                    }
                }
            }return isUserAvaiable;
        }

        public List<string> ReadContentsFromFile(string fileName)
        {
            List<string> rowvalues = new List<string>();
            using (StreamReader sr = new StreamReader(fileName))
            {
                string rowLine;
                while ((rowLine = sr.ReadLine()) != null)
                {
                    rowvalues.Add(rowLine);
                }
                return rowvalues; 
            }
        }

        public bool RegisterUser(User user)
        {
            users.Add(user);
            string fileName = "userDatabase.txt";
            bool isUsernameExists = IsUsernameExists(user.Name, fileName);
            if (isUsernameExists)
            {
                return false;
            }
            else
            {
                WriteContentsToFile(user, fileName);
                return true;    
            }
           
        }

        public void WriteContentsToFile(User user, string fileName)
        {
            using (StreamWriter sw = new StreamWriter(fileName,true))
            {
                sw.Write($"{user}");
            }
        }
    }
}
