﻿using System;
using System.Collections.Generic;

namespace RegistrationForm.Models
{
    public partial class ProfessionalDetailsTbl
    {
        public int ProfessionalDetailId { get; set; }
        public string RelocationStatus { get; set; } = null!;
        public int TotalExperienceInYears { get; set; }
        public int TotalExperienceInMonths { get; set; }
        public string? CurrentDesignation { get; set; }
        public string? PreviousDesignation { get; set; }
        public string? ResumeTitle { get; set; }
        public string? ResumePath { get; set; }
        public string? CopyResume { get; set; }
        public int UserId { get; set; }
        public int? PreferedLocationId { get; set; }
        public int JobCategoryId { get; set; }
        public int? CurrentIndustryId { get; set; }
        public int? NoticePeriodId { get; set; }
        public int? CtcinLakhPerAnumId { get; set; }
        public int? CtcinThousandPerAnumId { get; set; }
        public int? CurrentEmployerId { get; set; }
        public int? PreviousEmployerId { get; set; }

        public virtual CtcinLakhsTbl? CtcinLakhPerAnum { get; set; }
        public virtual CtcinThousandTbl? CtcinThousandPerAnum { get; set; }
        public virtual CurrentEmployerTbl? CurrentEmployer { get; set; }
        public virtual CurrentIndustryTbl? CurrentIndustry { get; set; }
        public virtual JobCategoryTbl JobCategory { get; set; } = null!;
        public virtual NoticePeriodTbl? NoticePeriod { get; set; }
        public virtual PreferedLocationTbl? PreferedLocation { get; set; }
        public virtual PreviousEmployerTbl? PreviousEmployer { get; set; }
        public virtual LoginDetailTbl User { get; set; } = null!;
    }
}
