﻿using Food_API.Context;
using Food_API.Model;
using Microsoft.EntityFrameworkCore;

namespace Food_API.Repository
{
    public class FoodRepository:IFoodRepository
    {
        readonly FoodDbContext _foodDbContext;
        public FoodRepository(FoodDbContext foodDbContext)
        {
            _foodDbContext=foodDbContext;
        }

        public int AddFood(FoodItems food)
        {
            _foodDbContext.FoodItems.Add(food);
            return _foodDbContext.SaveChanges();
        }

        public int EditFood(FoodItems food,FoodItems foodToUpdate)
        {
            //_foodDbContext.Entry(food).State = EntityState.Modified;
            foodToUpdate.FoodName = food.FoodName;
            foodToUpdate.FoodType = food.FoodType;
            foodToUpdate.Price = food.Price;
            foodToUpdate.ImagePath = food.ImagePath;
            foodToUpdate.FoodDiscription = food.FoodDiscription;
            return _foodDbContext.SaveChanges();
        }
        public async Task<List<FoodItems>> GetAllFood()
        {
            return await _foodDbContext.FoodItems.ToListAsync();
        }
        public int DeleteFood(FoodItems foodExists)
        {
            _foodDbContext.FoodItems.Remove(foodExists);
            return _foodDbContext.SaveChanges();
        }
        public FoodItems GetFoodById(int id)
        {
            return _foodDbContext.FoodItems.Where(u => u.FoodId == id).FirstOrDefault();
        }

      

    }
}
