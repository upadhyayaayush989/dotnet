﻿using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;

namespace FoodApp_Mvc.Models.ViewModel
{
    public class LoginSignupViewModel
    {
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Password { get; set; }

        [Display(Name = "Remember Me")]
        public bool IsRemember { get; set; }
    }
}
