﻿using System;
using System.Collections.Generic;

namespace RegistrationForm.Models
{
    public partial class InstituteTbl
    {
        public InstituteTbl()
        {
            EducationalDetailTbls = new HashSet<EducationalDetailTbl>();
        }

        public int InstituteId { get; set; }
        public string? InstituteName { get; set; }
        public int SpecializationId { get; set; }

        public virtual SpecializationTbl Specialization { get; set; } = null!;
        public virtual ICollection<EducationalDetailTbl> EducationalDetailTbls { get; set; }
    }
}
