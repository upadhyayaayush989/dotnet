﻿using Admin_API.Model;
using Admin_API.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Admin_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AdminController : ControllerBase
    {
        readonly IAdminService _adminService;
        public AdminController(IAdminService adminService)
        {
            _adminService = adminService;
        }

        [Route("LogIn")]
        [HttpPost]
        public ActionResult LogIn(AdminLogin  loginAdmin)
        {
            var admin = _adminService.LogIn(loginAdmin);
            //string userToken = _tokenGenerator.GenerateToken(user.Id, user.Name);

            return Ok(admin);
        }
    }
}
