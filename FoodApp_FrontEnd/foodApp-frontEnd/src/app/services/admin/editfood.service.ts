import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Food } from 'src/app/models/food';

@Injectable({
  providedIn: 'root'
})
export class EditfoodService {

  constructor(private httpClient:HttpClient) { }

  EditFood(food:Food,id:any):Observable<boolean>{
    return this.httpClient.put<boolean>('https://localhost:7039/api/Food/EditFood/'+id,food);
  }
  GetFoodById(id:number):Observable<Food>{
    return this.httpClient.get<Food>('https://localhost:7039/api/Food/GetFoodById/'+id);
  }
}
