import { TestBed } from '@angular/core/testing';

import { EditfoodService } from './editfood.service';

describe('EditfoodService', () => {
  let service: EditfoodService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EditfoodService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
