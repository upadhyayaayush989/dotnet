﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using RegistrationForm.LoggingClass;
using RegistrationForm.Models;
using RegistrationForm.Models.ViewModel;
using RegistrationForm.Service;
using System.Diagnostics;
using IHostingEnvironment = Microsoft.AspNetCore.Hosting.IHostingEnvironment;

namespace RegistrationForm.Controllers
{
    [ServiceFilter(typeof(ApplicantFormLogger))]
    public class HomeController : Controller
    {
        internal static string alternateEmailCheck;
        readonly ApplicantDatabaseContext _applicantDatabaseContext;
        readonly IRegisterService _registerService;
        readonly IHostingEnvironment _hostingEnvironment;
        public HomeController(ApplicantDatabaseContext applicantDatabaseContext, IRegisterService registerService,IHostingEnvironment hostingEnvironment)
        {
            _applicantDatabaseContext = applicantDatabaseContext;
            _registerService = registerService;
            _hostingEnvironment = hostingEnvironment;
        }
        
        
        [HttpGet]
        public IActionResult ApplicantForm()
        {
            ViewBag.CountryList = new SelectList(GetAllCountries(), "CountryId", "CountryName");
            ViewBag.QualificationList = new SelectList(GetHighestQualificationList(), "HighestQualificationId", "QualificationName");
            ViewBag.EmployeeStatusList = new SelectList(GetAllStatusOptions(), "YourStatusId", "StatusType");
            ViewBag.GenderList = new SelectList(GetAllGenderOptions(), "GenderId", "Gender");
            ViewBag.MaritalList = new SelectList(GetAllMaritalStatus(), "MaritalStatusId", "MaritalStatus"); 
            ViewBag.PreferredLocationList = new SelectList(GetPrefferedLocationOptions(), "PreferedLocationId", "OfficeLocation");
            ViewBag.JobList = new SelectList(GetAllJobCategory(), "JobCategoryId", "JobCategory");
            ViewBag.IndustryList = new SelectList(GetAllCurrentIndustry(), "CurrentIndustryId", "CurrentIndustryName");
            ViewBag.WorkTypeList = new SelectList(GetLookingForWorkType(), "LookingForId", "WorkType");
            ViewBag.NoticePeriodList = new SelectList(GetNoticePeriod(), "NoticePeriodid", "TotalNoticePeriod");
            ViewBag.KeySkillsList = new SelectList(GetAllSkills(), "KeySkillId", "KeySkillName");
            ViewBag.CtcInLakhList = new SelectList(GetAllCtcInLakhPerAnum(), "CtcinLakhPerAnumId", "CtcinLakhPerAnum");
            ViewBag.CtcInThousandList = new SelectList(GetAllCtcInThousandPerAnum(), "CtcinThousandPerAnumId", "CtcinThousandPerAnum");
            ViewBag.CurrentEmployerList = new SelectList(GetCurrentEmployerList(), "CurrentEmployerId", "CurrentEmployerName");
            return View();
        }

        [HttpPost]
        public IActionResult ApplicantForm(ApplicantViewModel userEnteredData)
        {           

            if (ModelState.IsValid)
            {
                string filePath = null;
                string uniqueFileName = null;
                if (userEnteredData.ResumePath != null)
                {                   
                    string uploadsFolder = Path.Combine(_hostingEnvironment.WebRootPath, "resumes");
                    uniqueFileName = Guid.NewGuid().ToString() + "_" + userEnteredData.ResumePath.FileName;
                     filePath = Path.Combine(uploadsFolder, uniqueFileName);
                    userEnteredData.ResumePath.CopyTo(new FileStream(filePath, FileMode.Create));                    
                }
                bool status = _registerService.RegistertheCandidateData(userEnteredData, filePath);
                return RedirectToAction("Dashboard");


            }

            return View();
        }
        public IActionResult Dashboard()
        {
            return View();
        }

        #region Check Mobile,Email and Alternate Email

        [AcceptVerbs("Post", "Get")]
        public IActionResult MobileNumberAlreadyExists(string mobileNumber)
        {
            var mobileExists = _applicantDatabaseContext.PersonalDetailTbls.Where(m => m.MobileNumber == mobileNumber).FirstOrDefault();
            if (mobileExists != null)
            {
                return Json($"Mobile Number {mobileExists.MobileNumber} already in use");
            }
            else
            {
                return Json(true);
            }
        }

        [AcceptVerbs("Post", "Get")]
        public IActionResult EmailAlreadyExists(string email)
        {
            var emailExists = _applicantDatabaseContext.LoginDetailTbls.Where(e => e.Email == email).FirstOrDefault();
            if (emailExists != null)
            {
                return Json($"Email {emailExists.Email} already in use");
            }
            else
            {
                alternateEmailCheck = email;
                return Json(true);
            }
        }


        public IActionResult AlternateEmailCheck(string alternateEmail)
        {
            if (alternateEmail == alternateEmailCheck)
            {
                return Json($"Email and Alternate Email Can't be same");
            }
            else
            {
                return Json(true);
            }
        }
        #endregion

        #region GetAllLoginDetails DropDown List
        public List<LookingForTbl> GetLookingForWorkType()
        {
            List<LookingForTbl> lookingForWorkType = _applicantDatabaseContext.LookingForTbls.ToList();
            return lookingForWorkType;
        }

        public List<YourStatusTbl> GetAllStatusOptions()
        {
            List<YourStatusTbl> yourStatusOptions = _applicantDatabaseContext.YourStatusTbls.ToList();
            return yourStatusOptions;
        }
        #endregion

        #region GetAllPersonalDetail Related List
        public List<GenderTbl> GetAllGenderOptions()
        {
            List<GenderTbl> genderOptions = _applicantDatabaseContext.GenderTbls.ToList();
            return genderOptions;
        }
        public List<MaritalStatusTbl> GetAllMaritalStatus()
        {
            List<MaritalStatusTbl> genderOptions = _applicantDatabaseContext.MaritalStatusTbls.ToList();
            return genderOptions;
        }

        public List<CountryTbl> GetAllCountries()
        {

            List<CountryTbl> countries = _applicantDatabaseContext.CountryTbls.ToList();
            return countries;
        }

        public IActionResult GetAllStates(int CountryId)
        {
            List<StateTbl> StateList = _applicantDatabaseContext.StateTbls.Where(x => x.CountryId == CountryId).ToList();
            ViewBag.StateList = new SelectList(StateList, "StateId", "StateName");
            return PartialView("DisplayState");
        }

        public IActionResult GetAllCities(int StateId)
        {
            List<CityTbl> CityList = _applicantDatabaseContext.CityTbls.Where(x => x.StateId == StateId).ToList();
            ViewBag.CityList = new SelectList(CityList, "CityId", "CityName");
            return PartialView("DisplayCity");
        }

        public IActionResult GetAllLocalities(int CityId)
        {
            List<LocalityTbl> LocalityList = _applicantDatabaseContext.LocalityTbls.Where(x => x.CityId == CityId).ToList();
            ViewBag.LocalityList = new SelectList(LocalityList, "LocalityId", "LocalityName");
            return PartialView("DisplayLocality");
        }
        #endregion

        #region GetAllEducational Related List

        public List<HighestQualificationTbl> GetHighestQualificationList()
        {

            List<HighestQualificationTbl> qualification = _applicantDatabaseContext.HighestQualificationTbls.ToList();
            return qualification;
        }

        public IActionResult GetAllSpecializations(int HighestQualificationId)
        {
            List<SpecializationTbl> SpecializationList = _applicantDatabaseContext.SpecializationTbls.Where(x => x.HighestQualificationId == HighestQualificationId).ToList();
            ViewBag.SpecializationList = new SelectList(SpecializationList, "SpecializationId", "SpecializationName");
            return PartialView("DisplaySpecialization");
        }

        public IActionResult GetAllInstituteList(int SpecializationId)
        {
            List<InstituteTbl> InstituteList = _applicantDatabaseContext.InstituteTbls.Where(x => x.SpecializationId == SpecializationId).ToList();
            ViewBag.InstituteList = new SelectList(InstituteList, "InstituteId", "InstituteName");
            return PartialView("DisplayInstitute");
        }
        #endregion


        #region GetAllProfessionalDetails Related List
        public List<PreferedLocationTbl> GetPrefferedLocationOptions()
        {
            List<PreferedLocationTbl> preffedLocations = _applicantDatabaseContext.PreferedLocationTbls.ToList();
            return preffedLocations;
        }
        public List<NoticePeriodTbl> GetNoticePeriod()
        {
            List<NoticePeriodTbl> noticePeriodList = _applicantDatabaseContext.NoticePeriodTbls.ToList();
            return noticePeriodList;
        }

        public List<CurrentIndustryTbl> GetAllCurrentIndustry()
        {
            List<CurrentIndustryTbl> currentIndustryOptions = _applicantDatabaseContext.CurrentIndustryTbls.ToList();
            return currentIndustryOptions;
        }

        public List<JobCategoryTbl> GetAllJobCategory()
        {
            List<JobCategoryTbl> jobCategories = _applicantDatabaseContext.JobCategoryTbls.ToList();
            return jobCategories;
        }

        public List<KeySkillsTbl> GetAllSkills()
        {
            List<KeySkillsTbl> keySkills = _applicantDatabaseContext.KeySkillsTbls.ToList();
            return keySkills;
        }

        public List<CtcinLakhsTbl> GetAllCtcInLakhPerAnum()
        {
            List<CtcinLakhsTbl> ctcinLakhList = _applicantDatabaseContext.CtcinLakhsTbls.ToList();
            return ctcinLakhList;
        }

        public List<CtcinThousandTbl> GetAllCtcInThousandPerAnum()
        {
            List<CtcinThousandTbl> ctcinThousandList = _applicantDatabaseContext.CtcinThousandTbls.ToList();
            return ctcinThousandList;
        }

        public List<CurrentEmployerTbl> GetCurrentEmployerList()
        {
            List<CurrentEmployerTbl> currentEmployer = _applicantDatabaseContext.CurrentEmployerTbls.ToList();
            return currentEmployer;
        }

        public IActionResult GetPreviousEmployer(int CurrentEmployerId)
        {
            List<PreviousEmployerTbl> previousEmployerList = _applicantDatabaseContext.PreviousEmployerTbls.Where(x => x.CurrentEmployerId == CurrentEmployerId).ToList();
            ViewBag.PreviousEmployerList = new SelectList(previousEmployerList, "PreviousEmployerId", "PreviousEmployerName");
            return PartialView("DisplayPreviousEmployer");
        }
        #endregion


       
        

       







        

       


















        //private readonly ILogger<HomeController> _logger;

        //public HomeController(ILogger<HomeController> logger)
        //{
        //    _logger = logger;
        //}

        public IActionResult Index()
        {
            return View();
        }

        //public IActionResult Privacy()
        //{
        //    return View();
        //}

        //[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        //public IActionResult Error()
        //{
        //    return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        //}



    }
}