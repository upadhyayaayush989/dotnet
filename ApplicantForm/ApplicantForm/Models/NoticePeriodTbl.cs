﻿using System;
using System.Collections.Generic;

namespace ApplicantForm.Models
{
    public partial class NoticePeriodTbl
    {
        public NoticePeriodTbl()
        {
            ProfessionalDetailsTbls = new HashSet<ProfessionalDetailsTbl>();
        }

        public int NoticePeriodid { get; set; }
        public string TotalNoticePeriod { get; set; } = null!;

        public virtual ICollection<ProfessionalDetailsTbl> ProfessionalDetailsTbls { get; set; }
    }
}
