﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collections.Exceptions
{
    internal class ProductAlreadyExistsException:ApplicationException
    {
        public ProductAlreadyExistsException()
        {

        }
        public ProductAlreadyExistsException(string msg):base(msg)
        {

        }

    }
}
