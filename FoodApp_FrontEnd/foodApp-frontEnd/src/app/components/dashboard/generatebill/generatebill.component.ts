import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Route, Router } from '@angular/router';
import { Bill } from 'src/app/models/bill';
import { BookingService } from 'src/app/services/user/booking.service';
import Swal from 'sweetalert2';
import * as pdfMake from 'pdfmake/build/pdfmake';
import * as pdfFonts from "pdfmake/build/vfs_fonts";  
(<any>pdfMake).vfs = pdfFonts.pdfMake.vfs;
import htmlToPdfmake from "html-to-pdfmake";
import jsPDF from 'jspdf';
import { EmailService } from 'src/app/services/user/email.service';
import { EmailDetails } from 'src/app/models/email-details';
import { DatePipe } from '@angular/common';
import { FoodCart } from 'src/app/models/food-cart';
//import jsPDF from 'jspdf';





@Component({
  selector: 'app-generatebill',
  templateUrl: './generatebill.component.html',
  styleUrls: ['./generatebill.component.css']
})
export class GeneratebillComponent implements OnInit {

  public serverHtml:string="";

  bill?:Bill[];
  userName:string=localStorage.getItem("username")!;
  userEmail:string=localStorage.getItem("useremail")!;
  emailDetails:EmailDetails;
  viewCart?:FoodCart[];

  pipe = new DatePipe('en-US');
  now = Date.now(); 
  currentDate = this.pipe.transform(this.now, 'dd/MM/yyyy');




  constructor(private bookingServie:BookingService,private router:Router,private emailService:EmailService) {
    this.emailDetails = new EmailDetails();
   }

  ngOnInit(): void {
    this.GenerateBill();
      this.GetCart();
    }
    GetCart(){
      this.bookingServie.ViewCart(this.userName).subscribe(res=>{
        console.log(res);
        this.viewCart = res;
      })
    }
    GenerateBill(){
      this.bookingServie.GenerateBill(this.userName).subscribe(res=>{
        this.bill=res;
        if(res){
          
          console.log("success");
          
        }
        else{
          console.log("failed");
          
        }
      })
    }

    PayBill(emailBody?:any){
      this.bookingServie.PayBill(this.userName).subscribe(res=>{
        if(res){
          // let s = ``;
          // this.viewCart?.forEach(r=>{

          // })
          this.emailDetails.body=emailBody?.toString();
          console.log("Bill payment successful");
          this.emailService.SendEmail(this.userEmail,this.emailDetails).subscribe(result =>{
            console.log(result)
          }
          );
          Swal.fire(
            `${this.userName} thankyou for shopping with us`,
            ``,
            'success'
          )
          // this.router.navigate(['dashboard']);
          
        }
        else{
          console.log("Bill payment failed");
          
        }
      })
    }
    

  @ViewChild('pdfTable') pdfTable!: ElementRef;
  PrintBill(){
    const doc = new jsPDF();
    //get table html
    const pdfTable = this.pdfTable.nativeElement;
    //html to pdf format
    var html = htmlToPdfmake(pdfTable.innerHTML);
   
    const documentDefinition = { content: html };
    pdfMake.createPdf(documentDefinition).download('Invoice.pdf');
    // pdfMake.createPdf(documentDefinition)
    //doc.save('Invoice.pdf');
  }

    
    GotoDashboard(){
      this.router.navigate(['dashboard']);
    }


  }
  


