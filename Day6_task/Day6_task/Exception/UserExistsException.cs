﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day6_task.Exception
{
    internal class UserExistsException:ApplicationException
    {
        public UserExistsException()
        {

        }
        public UserExistsException(string msg) : base(msg)
        {

        }
    }
}
