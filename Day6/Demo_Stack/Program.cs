﻿using System.Collections;

Console.WriteLine("demo::Stacks");

Stack logs = new Stack();

logs.Push("Request Came at 11 am");
logs.Push("DatabaseConnection");

logs.Push("Logged In");

foreach(var log in logs)
{
    Console.WriteLine(log);
}

Console.WriteLine("Latest Log Message::");

Console.WriteLine(logs.Peek());

Console.WriteLine(logs.Pop());

Console.WriteLine($"count:: {logs.Count}");
foreach(var item in logs)
{
    Console.WriteLine(item);
}