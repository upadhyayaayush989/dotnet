﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace Sql_day11task
{
    public static class Stud
    {
        #region Display
        public static void display()
        {
            string cs = @"Data source =NEOLAP000001620\SQLEXPRESS  ; Database=student; integrated Security=True";
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            string query = "select * from Student";
            SqlCommand cmd = new SqlCommand(query, con);

            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Console.WriteLine($"student id::{reader["S_id"]}\t Student Name:: {reader["Name"]}\t Student Rollno::{reader["Roll_no"]}");
            }
            con.Close();
        }
        #endregion

        #region Insert
        public static void insert()
        {
            string cs = @"Data source =NEOLAP000001620\SQLEXPRESS  ; Database=student; integrated Security=True";
            SqlConnection con = new SqlConnection(cs);
            Console.WriteLine("Enter Student Id to add in the table");
            int Id = int.Parse(Console.ReadLine());

            Console.WriteLine("Enter Student Name to add in the table");
            string Name = (Console.ReadLine());

            Console.WriteLine("Enter Student Rollno to add in the table");
            int Rollno = int.Parse(Console.ReadLine());

            con.Open();

            string query = "insert into Student values(@Id,@Name,@Rollno)";
            SqlCommand cmd = new SqlCommand(query, con);

            cmd.Parameters.AddWithValue("@id", Id);
            cmd.Parameters.AddWithValue("@Name", Name);
            cmd.Parameters.AddWithValue("@Rollno", Rollno);


            int insertionresult = cmd.ExecuteNonQuery();

            if (insertionresult > 0)
            {
                Console.WriteLine("inserted successfully");
            }
            else
            {
                Console.WriteLine("Not inserted");
            }
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Console.WriteLine($"student id::{reader["S_id"]}\t Student Name:: {reader["Name"]}\t Student Rollno::{reader["Roll_no"]}");
            }
            con.Close();

        }
        #endregion

        #region Update
        public static void update()
        {
            string cs = @"Data source =NEOLAP000001620\SQLEXPRESS  ; Database=student; integrated Security=True";
            SqlConnection con = new SqlConnection(cs);

            Console.WriteLine("Enter Student Id to update in the table");
            int Id = int.Parse(Console.ReadLine());

            Console.WriteLine("Enter Student Name to update in the table");
            string Name = (Console.ReadLine());

            Console.WriteLine("Enter Student Rollno to update in the table");
            int Rollno = int.Parse(Console.ReadLine());

            con.Open();
            string query = "update Student set S_id=@Id,Name=@Name,Roll_no=@Rollno where S_id=@Id";
            SqlCommand cmd = new SqlCommand(query, con);

            cmd.Parameters.AddWithValue("@Id", Id);
            cmd.Parameters.AddWithValue("@Name", Name);
            cmd.Parameters.AddWithValue("@Rollno", Rollno);

            int insertionresult = cmd.ExecuteNonQuery();

            if (insertionresult > 0)
            {
                Console.WriteLine("updated successfully");
            }
            else
            {
                Console.WriteLine("Not updated");
            }

            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Console.WriteLine($"student id::{reader["S_id"]}\t Student Name:: {reader["Name"]}\t Student Rollno::{reader["Roll_no"]}");
            }
            con.Close();

        }
        #endregion

        #region Delete
        public static void delete()
        {
            string cs = @"Data source =NEOLAP000001620\SQLEXPRESS  ; Database=student; integrated Security=True";
            SqlConnection con = new SqlConnection(cs);

            Console.WriteLine("Enter Student Id to delete in the table");
            int Id = int.Parse(Console.ReadLine());



            con.Open();
            string query = "delete from Student where S_id=@Id";
            SqlCommand cmd = new SqlCommand(query, con);

            cmd.Parameters.AddWithValue("@Id", Id);


            int insertionresult = cmd.ExecuteNonQuery();

            if (insertionresult > 0)
            {
                Console.WriteLine("Deleted successfully");
            }
            else
            {
                Console.WriteLine("Not Deleted");
            }
            con.Close();
        }
        #endregion

        #region Displaying using DataAdapter and Dataset
        public static void view()
        {
            string cs = @"Data source =NEOLAP000001620\SQLEXPRESS  ; Database=student; integrated Security=True";
            SqlConnection con = new SqlConnection(cs);

            SqlDataAdapter adapter = new SqlDataAdapter("select * from Student", con);
            DataSet dataSet = new DataSet();
            adapter.Fill(dataSet);
            foreach (DataRow row in dataSet.Tables[0].Rows)
            {
                Console.WriteLine($"student id::{row["S_id"]}\t Student Name:: {row["Name"]}\t Student Rollno::{row["Roll_no"]}");
            }
        }
        #endregion

        #region Displaying Using DataAdapter and Datatable
        public static void show()
        {

            string cs = @"Data source =NEOLAP000001620\SQLEXPRESS  ; Database=student; integrated Security=True";
            SqlConnection con = new SqlConnection(cs); 
            SqlDataAdapter adapter = new SqlDataAdapter("select * from Student", con);
            DataTable datatable = new DataTable();
            adapter.Fill(datatable);    
            foreach(DataRow row in datatable.Rows)
              {
                Console.WriteLine($"student id::{row["S_id"]}\t Student Name:: {row["Name"]}\t Student Rollno::{row["Roll_no"]}");
              }

        }
        #endregion
}
}

