﻿namespace Food_API.Exceptions
{
    public class FoodNotAvailableException:ApplicationException
    {
        public FoodNotAvailableException()
        {

        }
        public FoodNotAvailableException(string msg):base(msg)
        {

        }
    }
}
