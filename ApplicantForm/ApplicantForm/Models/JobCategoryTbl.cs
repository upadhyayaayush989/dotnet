﻿using System;
using System.Collections.Generic;

namespace ApplicantForm.Models
{
    public partial class JobCategoryTbl
    {
        public JobCategoryTbl()
        {
            ProfessionalDetailsTbls = new HashSet<ProfessionalDetailsTbl>();
        }

        public int JobCategoryId { get; set; }
        public string JobCategory { get; set; } = null!;

        public virtual ICollection<ProfessionalDetailsTbl> ProfessionalDetailsTbls { get; set; }
    }
}
