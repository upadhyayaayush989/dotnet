﻿using System;
using System.Collections.Generic;

namespace ApplicantForm.Models
{
    public partial class GenderTbl
    {
        public GenderTbl()
        {
            PersonalDetailTbls = new HashSet<PersonalDetailTbl>();
        }

        public int GenderId { get; set; }
        public string Gender { get; set; } = null!;

        public virtual ICollection<PersonalDetailTbl> PersonalDetailTbls { get; set; }
    }
}
