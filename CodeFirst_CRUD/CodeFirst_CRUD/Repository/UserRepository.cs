﻿using CodeFirst_CRUD.Data;
using CodeFirst_CRUD.Models;

namespace CodeFirst_CRUD.Repository
{
    public class UserRepository : IUserRepository
    {
        static int GrandTotal;
        ApplicationContext _foodDb;
        public UserRepository(ApplicationContext foodDb)
        {
            _foodDb = foodDb;
        }

        public void AddValuesToCart(int id)
        {
            if (_foodDb.FoodCart.Count() == 0)
            {
                GrandTotal = 0;
            }
            Food newItem = _foodDb.FoodMenu.Where(w => w.Foodid == id).FirstOrDefault();
            string foodName = newItem.FoodName;
            string img = newItem.ImgPath;
            int price = newItem.Price;
            int Quant = 1;
            int totalPrice = price * Quant;
            GrandTotal += totalPrice;
            FoodCart newFood = new FoodCart()
            {
                FoodName = foodName,
                Quantity = Quant,
                Total = totalPrice,
                ImgPath = img
            };
            FoodCart itemExists = _foodDb.FoodCart.Where(u => u.FoodName == foodName).FirstOrDefault();
            if (itemExists != null)
            {
                itemExists.Quantity += 1;
                itemExists.Total += price;

                _foodDb.FoodCart.Update(itemExists);
                _foodDb.SaveChanges();
            }
            else
            {
                _foodDb.FoodCart.Add(newFood);
                _foodDb.SaveChanges();
            }
        }

        public void DeleteElement(int id)
        {
            FoodCart deletedItem = _foodDb.FoodCart.Where(x => x.Id == id).FirstOrDefault();
            int price = deletedItem.Total / deletedItem.Quantity;
            if (GrandTotal > 60)
            {
                GrandTotal -= price;
            }
            if (deletedItem.Quantity > 1)
            {
                deletedItem.Quantity -= 1;
                deletedItem.Total -= price;
                _foodDb.FoodCart.Update(deletedItem);
                _foodDb.SaveChanges();
            }
            else
            {
                _foodDb.FoodCart.Remove(deletedItem);
                _foodDb.SaveChanges();
            }
        }
        public int GenerateBill()
        {

            return GrandTotal;

        }

        public List<Food> GetAllProducts()
        {
            return _foodDb.FoodMenu.ToList();
        }
        public List<FoodCart> GetCart()
        {
            return _foodDb.FoodCart.ToList();
        }

        public void RemoveElements()
        {
            foreach (FoodCart items in _foodDb.FoodCart)
            {
                _foodDb.FoodCart.Remove(items);

            }
            GrandTotal = 0;
            _foodDb.SaveChanges();

        }
    }
}
