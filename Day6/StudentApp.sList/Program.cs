﻿using StudentApp;
using StudentApp.sList;
using System.Collections;

// Non-generic Sorted List

Console.WriteLine("Sorted List");
SortedList sortedList = new SortedList();
sortedList.Add(101, new Student { Id = 101, Name = "Aayush", Age = 22 });
sortedList.Add(90, new Student { Id = 90, Name = "Aakash", Age = 25 });


foreach(DictionaryEntry item in sortedList)
{
    Console.WriteLine(item.Value);
}
