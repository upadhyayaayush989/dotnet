﻿using FoodApp_MVC.Context;
using FoodApp_MVC.Models;
using FoodApp_MVC.Services;
using Microsoft.AspNetCore.Mvc;

namespace FoodApp_MVC.Controllers
{
    public class FoodController : Controller
    {
        private readonly UserDbContext _userDbContext;
        private readonly IUserServices _userServices;
        public FoodController(UserDbContext userDbContext, IUserServices userServices)
        {
            _userDbContext = userDbContext;
            _userServices = userServices;
        }

        public IActionResult ShowMenu()
        {
            List<FoodMenu> mainMenu = _userServices.GetAllFood();
            return View(mainMenu);
        }
        public ActionResult AddFoodToCart(int id)
        {

            return RedirectToAction("ShowMenu");
        }


        
        //public IActionResult Index()
        //{
        //    var displayMenu = _userDbContext.FoodMenuTable.ToList();
        //    return View(displayMenu);
        //}
    }
}
