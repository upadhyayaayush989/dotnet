﻿

using System.Collections;

Console.WriteLine("Array List in C#");

//creating ArrayList 

ArrayList arrayList = new ArrayList();

arrayList.Add(101);
arrayList.Add("Aayush");
arrayList.Add(true);

foreach(var item in arrayList)
{
    Console.WriteLine(item);
}

//Get Individual element from ArrayList
Console.WriteLine("Geting Individual elements");
string secondElement = (string)arrayList[1];
Console.WriteLine(secondElement);

//inserting values to ArrayList

Console.WriteLine("Adding Values");
arrayList.Insert(1, 5000);
arrayList.Insert(2, "Mumbai");

foreach(var item in arrayList)
{
    Console.WriteLine(item);
}

//Insert Range of values

ArrayList arrayList1 = new ArrayList() { "Sv road","Sector1",12345678};
arrayList.InsertRange(0,arrayList1);

Console.WriteLine("After inserting range of values");

foreach(var item in arrayList)
{
    Console.WriteLine(item);
}

//Remove Element 

arrayList.Remove("Sv road");
arrayList.RemoveAt(1);



Console.WriteLine("After Remove");
foreach(var item in arrayList)
{
    Console.WriteLine(item);
}

Console.WriteLine("Remove Range");
arrayList.RemoveRange(0, 2);

foreach(var item in arrayList)
{
    Console.WriteLine(item);
}