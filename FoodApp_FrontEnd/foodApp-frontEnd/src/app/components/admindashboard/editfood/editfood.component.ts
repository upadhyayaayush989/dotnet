import { Component, OnInit } from '@angular/core';
import { Food } from 'src/app/models/food';
import { EditfoodService } from 'src/app/services/admin/editfood.service';
import { ActivatedRoute, Router } from '@angular/router';
import { param } from 'jquery';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-editfood',
  templateUrl: './editfood.component.html',
  styleUrls: ['./editfood.component.css']
})
export class EditfoodComponent implements OnInit {
  food:Food
  id: any;
  constructor(private editFoodService:EditfoodService,private route: ActivatedRoute,private router:Router) { 
    this.food=new Food();
  }
 
  ngOnInit(): void {
    this.route.paramMap.subscribe(parms=>{
      this.id=parms.get('id');
      console.log(parms);
      this.editFoodService.GetFoodById(this.id).subscribe(res=>{
        this.food=res;
      })
    })
  }
  EditFood(){
    this.editFoodService.EditFood(this.food,this.id).subscribe(res=>{
      if(res){
        Swal.fire(
          ' ',
          'Edit Was Successful',
          'success'
        )
          console.log(res);
          this.router.navigate(['admindashboard'])
      }
      
    })
  }

  
 

}
