﻿


using Demo_LinQ.Model;

Product[] productArray = new Product[]
{
    new Product(){Name="Nokia",Category="Mobile",Make="Nokia",Price=2000},
    new Product(){Name="Apple",Category="Mobile",Make="Appple",Price=10000},
    new Product(){Name="Realme",Category="Mobile",Make="Realme",Price=20000},
    new Product(){Name="Realme",Category="Laptop",Make="Realme",Price=50000}
};


BestSeller[] bestProduct = new BestSeller[]
{
    new BestSeller(){Name="Apple",Rank=1},
    new BestSeller(){Name="Realme",Rank=2}
};


Console.WriteLine("wiithout LINQ");
foreach (Product item in productArray)
{
    if (item.Price > 2000 && item.Price < 20000)
    {
        Console.WriteLine(item);
    }
}

Console.WriteLine("-----Using LINQ MEthod Syntax-----");
Product[] products = productArray.Where(P => P.Price > 2000 && P.Price < 20000).ToArray();

foreach (Product item in products)
{
    Console.WriteLine(item);
}

Console.WriteLine("--------LINQ Query Syntax");

var filterProduct = (from p in productArray
                     where p.Price > 2000 && p.Price < 20000
                     select p).ToArray();

foreach (Product item in filterProduct)
{
    Console.WriteLine(item);
}

//get product by Category

Console.WriteLine("----------Get Products of Mobile Category");

Product[] getProductByCategory = productArray.Where(p => p.Category == "Mobile").ToArray();

foreach (Product item in getProductByCategory)
{
    Console.WriteLine(item);
}

// ----- Get Matching value from the Array
Console.WriteLine("Get First Matching Value");
Product firstOccurance = productArray.FirstOrDefault(p => p.Name == "Realme");

Console.WriteLine(firstOccurance);

//Orderby

Console.WriteLine("------Order By logic");
var orderByLogic = from p in productArray
                   orderby p.Name descending
                   select p;

foreach (Product item in orderByLogic)
{
    Console.WriteLine(item);
}

//Grouping Operations
Console.WriteLine("Groping Operations");

var groupByCategory = from p in productArray
                      group p by p.Category;

foreach (var item in groupByCategory)
{
    Console.WriteLine($"groupByCategory:: {item.Key}");
    foreach (var item1 in item) { Console.WriteLine(item1); }
}

//Join Operations
// product name-product rank
Console.WriteLine("Join Operations");
var result = from b in bestProduct
             join p in productArray
             on b.Name equals p.Name
             select new
             {
                 productName = p.Name,
                 bestProductName = b.Rank
             };


foreach (var item in result)
{
    Console.WriteLine(item.productName + "\t" + item.bestProductName);
}

//Maxby() MinBy()
Console.WriteLine("MaxBy()  MinBy()");

var highestPrice = productArray.MaxBy(p => p.Price);

Console.WriteLine(highestPrice);

//Count elements
Console.WriteLine("----------Count Items---------");

var countItems = productArray.Count();
Console.WriteLine(countItems);


List<int> myValues = new List<int> { 101, 102, 103, 104, 105 };

int getEvenCount = myValues.Count(e => e % 2 == 0);
Console.WriteLine(getEvenCount);


string[] cities = { "Banglore", "Bhopal", "Mysore", "Delhi", "Bombay" };

//filtering data having b in it

var result1 = from city in cities
              where city.Contains('B')
              select city;

foreach(var c in result1)
{
    Console.WriteLine(c);
}

Console.WriteLine("------Take Funtion-----");
var result2 = cities.Take(3);
foreach(var city in result2)
{
    Console.WriteLine(city);
}