﻿using HandlebarsDotNet;
using MailKit.Security;
using MimeKit.Text;
using MimeKit;
using NuGet.Protocol.Core.Types;
using RegistrationForm.Models;
using RegistrationForm.Models.ViewModel;
using RegistrationForm.Repository;
using System.Text;
using System.Transactions;
using MailKit.Net.Smtp;

namespace RegistrationForm.Service
{
    
    public class RegisterService:IRegisterService
    {
        readonly IConfiguration _configuration;
        readonly IRegisterRepository _registerRepository;
        public RegisterService(IRegisterRepository registerRepository, IConfiguration configuration)
        {
            _registerRepository = registerRepository;
            _configuration = configuration;
        }

        public string encryptPassword(string password)
        {
            string msg = "";
            byte[] encode = new byte[password.Length];
            encode = Encoding.UTF8.GetBytes(password);
            msg = Convert.ToBase64String(encode);
            return msg;
        }

        public bool RegistertheCandidateData(ApplicantViewModel userEnteredData, string filePath)
        {
            bool userExistStatus = _registerRepository.IsUserEmailExist(userEnteredData.Email);
            if (userExistStatus == false)
            {
                using (TransactionScope transactionScope = new TransactionScope())
                {
                    try
                    {

                    LoginDetailTbl loginDetails = new LoginDetailTbl()
                    {
                        Email = userEnteredData.Email,
                        AlternateEmail = userEnteredData.AlternateEmail,
                        FirstName = userEnteredData.FirstName,
                        LastName = userEnteredData.LastName,
                        Password = encryptPassword(userEnteredData.Password),
                        WorkTypeId = userEnteredData.WorkTypeId,
                        StatusId = userEnteredData.YourStatusId

                    };
                    int userId = _registerRepository.SaveDataToLoginDetailsTbl(loginDetails);


                    PersonalDetailTbl personalDetails = new PersonalDetailTbl()
                    {
                        PhoneNumber = userEnteredData.PhoneNumber,
                        MobileNumber = userEnteredData.MobileNumber,
                        DateofBirth = userEnteredData.DateofBirth,
                        GenderId = userEnteredData.GenderId,
                        MaritalStatusId = userEnteredData.MaritalStatusId,
                        UserId = userId,
                        CountryId = userEnteredData.CountryId,
                        StateId = userEnteredData.StateId,
                        District = userEnteredData.District,
                        CityId = userEnteredData.CityId,
                        LocalityId = userEnteredData.LocalityId,
                        ZipCode = userEnteredData.ZipCode,

                    };

                    _registerRepository.SaveDataToPersonalDetailsTbl(personalDetails);



                    int userIntituteId = userEnteredData.InstituteId;
                    if (userEnteredData.OtherInstitute != null && userEnteredData.InstituteId == 0)
                    {
                        _registerRepository.AddNewIntitute(userEnteredData.OtherInstitute, userEnteredData.SpecializationId);
                        userIntituteId = _registerRepository.GetIntituteId(userEnteredData.OtherInstitute);
                    }

                    EducationalDetailTbl educationalDetails = new EducationalDetailTbl()
                    {
                        UserId = userId,
                        HighestQualificationId = userEnteredData.HighestQualificationId,
                        SpecializationId = userEnteredData.SpecializationId,
                        InstituteId = userIntituteId,
                        OtherInstitute = userEnteredData.OtherInstitute

                    };
                    _registerRepository.SaveDataToEducationalDetailsTbl(educationalDetails);



                    ProfessionalDetailsTbl professionalDetails = new ProfessionalDetailsTbl()
                    {
                        UserId = userId,
                        PreferedLocationId = userEnteredData.PreferedLocationId,
                        RelocationStatus = userEnteredData.RelocationStatus == true ? "Yes" : "No",
                        TotalExperienceInYears = userEnteredData.TotalExperienceInYears,
                        TotalExperienceInMonths = userEnteredData.TotalExperienceInMonths,
                        JobCategoryId = userEnteredData.JobCategoryId,
                        //KeySkillId = String.Join(", ", userEnteredData.KeySkillId),
                        CurrentIndustryId = userEnteredData.CurrentIndustryId,
                        CurrentEmployerId = userEnteredData.CurrentEmployerId,
                        CurrentDesignation = userEnteredData.CurrentDesignation,
                        PreviousEmployerId = userEnteredData.PreviousEmployerId,
                        PreviousDesignation = userEnteredData.PreviousDesignation,
                        NoticePeriodId = userEnteredData.NoticePeriodId,
                        CtcinLakhPerAnumId = userEnteredData.CtcinLakhPerAnumId,
                        CtcinThousandPerAnumId = userEnteredData.CtcinThousandPerAnumId,
                        ResumeTitle = userEnteredData.ResumeTitle,
                        ResumePath = filePath,
                        CopyResume = userEnteredData.CopyResume

                    };
                    _registerRepository.saveDataToProfessionalDetailsTbl(professionalDetails);

                    for (int i = 0; i < userEnteredData.KeySkillId.Length; i++)
                    {
                        CandidateKeySkillsTbl candidateKeySkills = new CandidateKeySkillsTbl()
                        {
                            UserId = userId,
                            KeySkillId = userEnteredData.KeySkillId[i]
                        };
                        _registerRepository.saveDataToCandidateKeySkillsTbl(candidateKeySkills);
                    }
                    transactionScope.Complete();
                    transactionScope.Dispose();
                    SendEmail(userEnteredData.Email, userEnteredData.FirstName, userEnteredData.LastName);
                    return true;
                    }
                    catch
                    {
                        transactionScope.Dispose();
                    }
                }
                

            }

            return false;
        }

        public string htmlTemplate()
        {

            string html = @"<div class=""entry""><div style=""display:flex ;flex-direction:row""><span><img src=""https://cdn-icons-png.flaticon.com/512/8212/8212602.png""></span<span<h1><strong>Congrats Successfully Registered On Job Portal!!</strong><br></h1<h4>First Name.<strong style=""color:white"">{{EmployeeFirstName}}</strong></h4></span><br></div><hr<h4>Last Name<strong style=""color:white""> {{EmployeeLastName}}</strong></div>";
            return html;
        }

        public void SendEmail(string Email, string FirstName, string LastName)
        {
            var email = new MimeMessage();
            email.From.Add(MailboxAddress.Parse(_configuration.GetSection("EmailUserName").Value));
            email.To.Add(MailboxAddress.Parse(Email));
            email.Subject = "Registration Success";

            var data = new
            {
                EmployeeFirstName = FirstName,
                EmployeeLastName = LastName
            };

            var template = Handlebars.Compile(htmlTemplate());
            var result = template(data);

            email.Body = new TextPart(TextFormat.Html) { Text = result };

            var smtp = new SmtpClient();
            smtp.Connect(_configuration.GetSection("EmailHost").Value, 587, SecureSocketOptions.StartTls);//host and port
            smtp.Authenticate(_configuration.GetSection("EmailUserName").Value, _configuration.GetSection("EmailPassword").Value);
            smtp.Send(email);
            smtp.Disconnect(true);
        }
    }
}
