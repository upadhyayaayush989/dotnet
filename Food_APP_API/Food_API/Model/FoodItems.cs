﻿using System.ComponentModel.DataAnnotations;

namespace Food_API.Model
{
    public class FoodItems
    {
        [Key]
        public int FoodId { get; set; }
        public string FoodName { get; set; }
        public string? FoodDiscription { get; set; }
        public string FoodType { get; set; }
        public int Price { get; set; }

        public string? ImagePath { get; set; }
    }
}
