﻿using System;
using System.Collections.Generic;

namespace RegistrationForm.Models
{
    public partial class CurrentEmployerTbl
    {
        public CurrentEmployerTbl()
        {
            PreviousEmployerTbls = new HashSet<PreviousEmployerTbl>();
            ProfessionalDetailsTbls = new HashSet<ProfessionalDetailsTbl>();
        }

        public int CurrentEmployerId { get; set; }
        public string? CurrentEmployerName { get; set; }

        public virtual ICollection<PreviousEmployerTbl> PreviousEmployerTbls { get; set; }
        public virtual ICollection<ProfessionalDetailsTbl> ProfessionalDetailsTbls { get; set; }
    }
}
