﻿using BookingAPI.Models;

using Newtonsoft.Json.Linq;

namespace BookingAPI.Services
{
    public interface IBookingService
    {
        Task<bool> AddItemsToCart(FoodCart addItems);
        //List<Menu> GetAllFoods();
        
        Task<bool> PayBill(string userName);
        Task<bool> RemoveItem(int cartId);
        Task<List<FoodCart>> ViewCartItems(string UserName);
         Task<int> GenerateBill(string userName);
        string InVoiceNo();
        public void SendEmail( string userEmail, EmailDetails request);


    }
}
