﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebAppFramework_Day13.Model;
using WebAppFramework_Day13.Repository;

namespace WebAppFramework_Day13
{
    public partial class Register1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            User userExists = UserRepository.GetUserByName(txtUsername.Text);
            if (userExists == null)
            {
                User user = new User();
                user.Name = txtUsername.Text;
                user.Password = txtPassword.Text;
                UserRepository.AddUser(user);
                Response.Redirect("Login.aspx");
            }
        }
    }
}