﻿using System;
using System.Collections.Generic;

namespace RegistrationForm.Models
{
    public partial class CandidateKeySkillsTbl
    {
        public int CandidateKeySkillid { get; set; }
        public int UserId { get; set; }
        public int KeySkillId { get; set; }

        public virtual KeySkillsTbl KeySkill { get; set; } = null!;
        public virtual LoginDetailTbl User { get; set; } = null!;
    }
}
