﻿using RegistrationForm.Models;

namespace RegistrationForm.Repository
{
    public interface IRegisterRepository
    {
        public bool IsUserEmailExist(string email);
        public int SaveDataToLoginDetailsTbl(LoginDetailTbl loginDetailTbl);
        public bool SaveDataToPersonalDetailsTbl(PersonalDetailTbl personalDetailTbl);
        public bool SaveDataToEducationalDetailsTbl(EducationalDetailTbl educationDetailTbl);

        public bool saveDataToProfessionalDetailsTbl(ProfessionalDetailsTbl professionalDetailsTbl);
        public void AddNewIntitute(string otherInstitute, int? SpecializationId);
        public int GetIntituteId(string otherInstitute);
        public bool saveDataToCandidateKeySkillsTbl(CandidateKeySkillsTbl candidateKeySkills);
    }
}
