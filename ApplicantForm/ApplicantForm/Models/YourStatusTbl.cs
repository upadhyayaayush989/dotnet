﻿using System;
using System.Collections.Generic;

namespace ApplicantForm.Models
{
    public partial class YourStatusTbl
    {
        public YourStatusTbl()
        {
            LoginDetailTbls = new HashSet<LoginDetailTbl>();
        }

        public int YourStatusId { get; set; }
        public string StatusType { get; set; } = null!;

        public virtual ICollection<LoginDetailTbl> LoginDetailTbls { get; set; }
    }
}
