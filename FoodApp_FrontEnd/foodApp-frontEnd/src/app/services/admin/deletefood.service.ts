import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Food } from 'src/app/models/food';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DeletefoodService {

  constructor(private httpClient:HttpClient) { }

  DeleteFood(id?:number):Observable<boolean>{
    return this.httpClient.delete<boolean>('https://localhost:7039/api/Food/DeleteFood?id='+id);
  }
}
