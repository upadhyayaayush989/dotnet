﻿using CookieStatemanagement.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace CookieStatemanagement.Controllers
{
    public class HomeController : Controller
    {

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Index(User user)
        {
            //set the key value in cookie -cookie is a key-valued pair
            CookieOptions option = new CookieOptions();
            option.Expires = DateTime.Now.AddMinutes(10);
            Response.Cookies.Append("Name",$"{user.Name}",option);
            return RedirectToAction("Dashboard");
        }

        public ActionResult Dashboard()
        {
            ViewBag.Username=Request.Cookies["Name"];
            return View();
        }

        public ActionResult RemoveCookie()
        {
            Response.Cookies.Delete("Name");
            return View("Index");
        }

        //public IActionResult Privacy()
        //{
        //    return View();
        //}

        //[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        //public IActionResult Error()
        //{
        //    return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        //}
    }
}