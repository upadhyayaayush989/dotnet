﻿using Hackathonpro.Models;
using Hackathonpro.Repository;

namespace Hackathonpro.Services
{
    public class UserSevices : IUserServices
    {
        readonly IUserRepo _userRepo;
        public UserSevices(IUserRepo userRepo)
        {
            _userRepo = userRepo;
        }
        public bool AddUser(string Name,User user)
        {
            bool verify;
            bool check = _userRepo.GetUserByName(Name);
            if (check == false)
            {
                _userRepo.AddUser(user);
                verify = true;
                return verify;
            }
            else verify = false;
            return verify;
            
            
        }

        public void AddValues(int id)
        {
            _userRepo.AddValuesToCart(id);
        }

        public void DeleteItemFromCart(int id)
        {
            _userRepo.DeleteElement(id);
        }

        public int GenerateBill()
        {
           return _userRepo.GenerateBill();
        }

        public List<AddToCart> GetCart()
        {
           return _userRepo.GetCart();
        }

        public List<Food> GetProducts()
        {
            return _userRepo.GetAllProducts();
        }

        public void RemoveItems()
        {
            _userRepo.RemoveElements();
        }

        public bool VerifyUser(string userName, string password)
        {
            bool userver = _userRepo.GetUserByUserName(userName,password);
           // bool passver = _userRepo.GetUserByUserPassword(password);
            if(userver )
            {
                return true;
            }
            return false;
        }
        
        
      }
}
