﻿using User_API.Model;

namespace User_API.Repository
{
    public interface IUserRepository
    {
        int BlockUnBlockUser(bool blockUnBlockUser, User userExists);
        int DeleteUser(User userExists);
        int EditUser(User user);
        Task<List<User>> GetAllUsers();
        User GetUserById(int id);
        User GetUserByName(string name);
        string GetUserEmailByUserName(string userName);
        User LogIn(string name, string password);
        int RegisterUser(User user);
    }
}
