﻿using RegistrationForm.Models.ViewModel;

namespace RegistrationForm.Service
{
    public interface IRegisterService
    {
        public bool RegistertheCandidateData(ApplicantViewModel userEnteredData, string filePath);
        string encryptPassword(string password);
        public void SendEmail(string Email, string FirstName, string LastName);

    }
}
