﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegate_example
{
    delegate void ProcessOrder();
    internal class CoffeeMaker
    {
        public void MakeCofee(ProcessOrder handler)
        {
            Console.WriteLine("Coffee is being Prepared");
            handler();
            Console.ReadLine();
        }
    }
}
