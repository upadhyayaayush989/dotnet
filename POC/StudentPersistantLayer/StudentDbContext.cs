﻿using Microsoft.EntityFrameworkCore;
using StudentPersistantLayer.Model;

namespace StudentPersistantLayer
{
    public class StudentDbContext:DbContext
    {
       
        protected override void OnConfiguring(DbContextOptionsBuilder dbContextOptionsBuilder)
        {
            dbContextOptionsBuilder.UseSqlServer(@"Data Source=DESKTOP-IKB2EQL;Database=POC2_Student_Db;Integrated Security=True");
        }

        public DbSet<Student> StudentDetails { get; set; }
    }
}