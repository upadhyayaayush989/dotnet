﻿using FoodApp_Mvc.Models;

namespace FoodApp_Mvc.Services
{
    public interface IUserServices
    {
        List<Food> GetProducts();
        void AddValues(int id);

        List<FoodCart> GetCart();
        void DeleteItemFromCart(int id);

        int GenerateBill();
        void RemoveItems();
    }
}
