﻿using Day5.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day5.Repository
{
    internal interface IFile
    {

        void WriteContentsToFile(User user, string fileName);
        List<string> ReadContentsFromFile(string fileName);

        bool IsUsernameExists(string userName, string filename);
    }
}
