import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from 'src/app/models/user';

@Injectable({
  providedIn: 'root'
})
export class LoginRegisterService {

  constructor(private httpClient:HttpClient) { }
  RegisterUser(user:User):Observable<boolean>{
    return this.httpClient.post<boolean>('https://localhost:7072/api/User/RegisterUser',user)
  }
  LoginUser(loginUser:User):Observable<boolean>{
    return this.httpClient.post<boolean>('https://localhost:7072/api/User/LogIn',loginUser);
  }

}
