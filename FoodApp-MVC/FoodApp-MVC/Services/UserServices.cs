﻿using FoodApp_MVC.Exceptions;
using FoodApp_MVC.Models;
using FoodApp_MVC.Repository;

namespace FoodApp_MVC.Services
{
    public class UserServices:IUserServices
    {
        readonly IUserRepository _userRepository;
        public UserServices(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        public bool RegisterUser(User user)
        {
            var userPresent = _userRepository.GetUserByUserName(user.UserName);
            if(userPresent == null)
            {
                return _userRepository.RegisterUser(user);
            }    
            return false;
        }

        public User LogIn(User logInfo)
        {
            User user = _userRepository.LogIn(logInfo.UserName, logInfo.Password);
            if (user != null)
            {
                return user;
            }
            else
            {
               
                throw new UserCredentialInvalidException($"{logInfo.UserName} and other details invalid");
                //return 
            }
        }

        public List<FoodMenu> GetAllFood()
        {
            return _userRepository.GetAllFood();
        }

        public void AddValues(int id)
        {
            _userRepository.AddValues(id);
        }
    }
}
