﻿using CodeFirst_CRUD.Models;
using CodeFirst_CRUD.Models.Account;
using Microsoft.EntityFrameworkCore;

namespace CodeFirst_CRUD.Data
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options) { }
        public DbSet<Food> FoodMenu { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<FoodCart> FoodCart { get; set; }
    }
}
