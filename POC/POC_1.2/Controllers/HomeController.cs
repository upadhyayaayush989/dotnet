﻿using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using POC_1._2.LoggingClass;
using POC_1._2.Models;
using POC_1._2.Service;
using System.Diagnostics;
using IHostingEnvironment = Microsoft.AspNetCore.Hosting.IHostingEnvironment;


namespace POC_1._2.Controllers
{
    [ServiceFilter(typeof(ApplicationLogger))]
    public class HomeController : Controller
    {
        private readonly IWebHostEnvironment _hostingEnvironment;
        readonly IFileService _fileService;
        public HomeController(IWebHostEnvironment hostingEnvironment, IFileService fileService)
        {
            _hostingEnvironment = hostingEnvironment;
            _fileService = fileService;
        }

        public ActionResult File()
        {
            var model =  _fileService.CreateNewInstanceFileViewModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult File(FileUploadViewModel model)
        {
            _fileService.GetDataFromExcelFile(model);
            

            return View(model);
        }

    }
}