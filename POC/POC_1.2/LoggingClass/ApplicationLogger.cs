﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace POC_1._2.LoggingClass
{
    public class ApplicationLogger : ActionFilterAttribute, IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            if (!context.ExceptionHandled)
            {
                context.ExceptionHandled = true;
                context.Result = new ViewResult()
                {
                    ViewName = "ErrorDisplayView"
                };
            }
        }
    }
}
