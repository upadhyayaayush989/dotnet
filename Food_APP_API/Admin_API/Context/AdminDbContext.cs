﻿using Admin_API.Model;
using Microsoft.EntityFrameworkCore;

namespace Admin_API.Context
{
    public class AdminDbContext:DbContext
    {
        public AdminDbContext(DbContextOptions<AdminDbContext> options) : base(options)
        {

        }
        public DbSet<Admin> AdminUsers { get; set; }
    }
}
