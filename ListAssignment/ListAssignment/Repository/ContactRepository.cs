﻿using ListAssignment.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListAssignment.Repository
{
    internal class ContactRepository
    {
        List<Contact> contacts;

        public ContactRepository()
        {
            contacts = new List<Contact>()
            {
                new Contact(){ Name = "Aayush", Address="Maharashtra", City="Mumbai", Phone="1234567890"},
                new Contact(){Name="Manoj", Address="Rajasthan", City="Udaipur", Phone="9988776655"},
                new Contact(){Name="Priti", Address="Gujarat", City="Surat", Phone="3344556677"}
            };
        }

        public List<Contact> GetAllDetails()
        {
            return contacts;
        }

        public string AddContact(Contact contact)
        {
            var isContact = GetDetailByContact(contact.Name);
            if (isContact == null)
            {
                contacts.Add(contact);
                return $"Contact Added Successfully";
            }
            else
            {
                return $"Contact Not added";
            }
        }

        private Contact GetDetailByContact(string name)
        {
            return contacts.Where(c => c.Name == name).FirstOrDefault();
        }

        public bool DeleteContactWithName(string name)
        {
            var contact = GetDetailByContact(name);
            return contact != null ? contacts.Remove(contact) : false;
        }

        public List<Contact> UpdateContactByName(string name)
        {
            var contact = GetDetailByContact(name);
            if (contact == null)
            {
                Console.WriteLine("Contact does not exist");
            }
            else
            {
                Console.WriteLine("Enter New Name");
                contact.Name = Console.ReadLine();
                Console.WriteLine("Enter New Address");
                contact.Address = Console.ReadLine();
                Console.WriteLine("Enter New City");
                contact.City = Console.ReadLine();
                Console.WriteLine("Enter New Phone");
                contact.Phone = Console.ReadLine();
            }
            return contacts;

        }
    }
}
