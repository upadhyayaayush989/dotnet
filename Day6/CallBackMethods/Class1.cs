﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CallBackMethods
{
    internal class Class1
    {
        //delegate declartion 
        public delegate void Callback(int i);

        public void LongRunningProcess(Callback obj)
        {
            for(int i = 0; i < 100; i++)
            {
                obj(i);
            }
        }
    }
}
