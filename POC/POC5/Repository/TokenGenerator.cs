﻿using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;

namespace POC5.Repository
{
    public class TokenGenerator:ITokenGenerator
    {
        readonly IConfiguration _configuration;
        public TokenGenerator(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public string GenerateRefreshToken()
        {
            var randomNumber = new byte[32];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(randomNumber);
                return Convert.ToBase64String(randomNumber);
            }


        }

        public string GenerateToken(IEnumerable<Claim> claims)
        {
            var userSecurityKey = Encoding.UTF8.GetBytes(_configuration["JwtValidationDetails:UserSecretKey"]);
            var userSymmetricSecurity = new SymmetricSecurityKey(userSecurityKey);
            var userSiginCredentials = new SigningCredentials(userSymmetricSecurity, SecurityAlgorithms.HmacSha256);
            var userJwtSecurityToken = new JwtSecurityToken
                (
                   issuer: _configuration["JwtValidationDetails:UserIssuer"],
                   audience: _configuration["JwtValidationDetails:UserAudience"],
                   claims: claims,
                   expires: DateTime.UtcNow.AddMinutes(1),
                   signingCredentials: userSiginCredentials
                );
            var userSecurityTokenHandler = new JwtSecurityTokenHandler().WriteToken(userJwtSecurityToken);
            return userSecurityTokenHandler;
        }

        public ClaimsPrincipal GetPricipalFromExpiredToken(string token)
        {
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateAudience = false,
                ValidateIssuer = false,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JwtValidationDetails:UserSecretKey"])),
                ValidateLifetime = false

            };
            var tokenHandler = new JwtSecurityTokenHandler();
            SecurityToken securityToken;
            var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out securityToken);
            var jwtSecurityToken = securityToken as JwtSecurityToken;
            if (jwtSecurityToken == null || !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase))
            {
                throw new SecurityTokenException("Invalid Token");

            }
            return principal;
        }






    }
}
