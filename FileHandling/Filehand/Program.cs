﻿using Filehand;


string rootPath = @"C:\Users\user\source\repos\FileHandling";

//string[] dirs = Directory.GetDirectories(rootPath,"*",SearchOption.AllDirectories);

//foreach (string dir in dirs)
//{
//    Console.WriteLine(dir);
//}


//var files  = Directory.GetFiles(rootPath,"*.*",SearchOption.TopDirectoryOnly);
//foreach(string file in files)
//{
//    //Console.WriteLine(file);
//    //Console.WriteLine(Path.GetFileName(file));
//    //Console.WriteLine(Path.GetFileNameWithoutExtension(file));
//    //Console.WriteLine(Path.GetDirectoryName(file));
//    var info = new FileInfo(file);
//    Console.WriteLine($"{Path.GetFileName(file)}: {info.Length} bytes");
//}


// verifiying if a directory exists 
//string newPath = @"C:\Users\user\source\repos\FileHandling\SubFolderC\SubSubFolderD";


//shortcut method which preserves files too if the directory is there but still you are creating it 
//Directory.CreateDirectory(newPath);

//bool directoryExists = Directory.Exists(newPath);//case insensitive search
////Console.WriteLine(directoryExists);
//if (directoryExists)
//{
//    Console.WriteLine("The Directory exists");
//}
//else
//{
//    Console.WriteLine("Directory doesn't exists");
//    Directory.CreateDirectory(newPath);
//}


//copying files 

string[] files = Directory.GetFiles(rootPath);
string destinationFolder = @"C:\Users\user\source\repos\FileHandling\SubFolderA\";


//foreach (string file in files)
//{
//    File.Copy(file, $"{destinationFolder}{Path.GetFileName(file)}", true);
//}

//for(int i = 0; i< files.Length; i++)
//{
//    File.Copy(files[i], $"{destinationFolder}{i}.txt", true);//true or false if you want to overwirte file
//}

//moving files

//foreach (string file in files)
//{
//    File.Move(file, $"{destinationFolder}{Path.GetFileName(file)}");
//}




Console.ReadLine();
