﻿using System.ComponentModel.DataAnnotations;

namespace POC_2.Models
{
    public class StudentViewModel
    {

        public int StudentId { get; set; }
        public string StudentName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        public string StudentLocation { get; set; }
        public string StudentBranch { get; set; }
    }
}
