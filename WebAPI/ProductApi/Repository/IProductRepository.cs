﻿using ProductApi.Model;

namespace ProductApi.Repository
{
    public interface IProductRepository
    {
        bool AddProduct(Product product);
        bool EditProduct(int id,Product productUpdate);
        List<Product> GetAllProducts();
        object GetProductById(int id);
        object DeleteProductById(int id);
    }
}
