﻿using System;
using System.Collections.Generic;

namespace ApplicantForm.Models
{
    public partial class SpecializationTbl
    {
        public SpecializationTbl()
        {
            EducationalDetailTbls = new HashSet<EducationalDetailTbl>();
            InstituteTbls = new HashSet<InstituteTbl>();
        }

        public int SpecializationId { get; set; }
        public string SpecializationName { get; set; } = null!;
        public int HighestQualificationId { get; set; }

        public virtual HighestQualificationTbl HighestQualification { get; set; } = null!;
        public virtual ICollection<EducationalDetailTbl> EducationalDetailTbls { get; set; }
        public virtual ICollection<InstituteTbl> InstituteTbls { get; set; }
    }
}
