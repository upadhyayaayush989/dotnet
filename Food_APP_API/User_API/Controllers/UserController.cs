﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using User_API.Model;
using User_API.Repository;
using User_API.Service;

namespace User_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        readonly IUserService _userService;
        readonly ITokenGenerator _tokenGenerator;
        readonly IUserRepository _userRepository;
        public UserController(IUserService userService,ITokenGenerator tokenGenerator,IUserRepository userRepository)
        {
            _userService = userService;
            _tokenGenerator = tokenGenerator;
            _userRepository = userRepository;
        }

        [Route("api/GetAllUsers")]
        [HttpGet]
        public async Task<ActionResult> GetAllUsers()
        {
            List<User> users = await _userService.GetAllUsers();
            return Ok(users);
        }
        [Route("RegisterUser")]
        [HttpPost]
        public ActionResult RegisterUser(User user)
        {
            bool regsiterUserStatus = _userService.RegisterUser(user);
            return Ok(regsiterUserStatus);
        }

        [Route("DeleteUser")]
        [HttpDelete]
        public ActionResult DeleteUser(int id)
        {
            bool userDeleteStatus = _userService.DeleteUser(id);
            return Ok(userDeleteStatus);
        }
        [Route("EditUser/{id:int}")]
        [HttpPut]
        public ActionResult EditUser(int id, User user)
        {
            bool userEditStatus = _userService.EditUser(id, user);
            return Ok(userEditStatus);
        }

        [Route("BlockUnBlockUser")]
        [HttpPut]
        public ActionResult BlockUnBlockUser(int id, bool blockUnBlockUser)
        {
            bool blockUnblockStatus = _userService.BlockUnBlockUser(id, blockUnBlockUser);
            return Ok(blockUnblockStatus);
        }

        [Route("LogIn")]
        [HttpPost]
        public ActionResult LogIn(LoginUser loginUser)
        {
            User user = _userService.LogIn(loginUser);
            
            //string userToken = _tokenGenerator.GenerateToken(user.Id, user.Name);
            string userToken = _tokenGenerator.GenerateToken(user.Id, user.UserName,user.Email);

            return Ok(userToken);
        }
        

        //[Route("SendEmail")]
        //[HttpPost]
        //public IActionResult SendEmail(EmailDetails request, string userName)
        //{
        //    _userService.SendEmail(request, userName);
        //    return Ok();
        //}

    }
}
