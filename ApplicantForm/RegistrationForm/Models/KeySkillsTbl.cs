﻿using System;
using System.Collections.Generic;

namespace RegistrationForm.Models
{
    public partial class KeySkillsTbl
    {
        public KeySkillsTbl()
        {
            CandidateKeySkillsTbls = new HashSet<CandidateKeySkillsTbl>();
        }

        public int KeySkillId { get; set; }
        public string KeySkillName { get; set; } = null!;

        public virtual ICollection<CandidateKeySkillsTbl> CandidateKeySkillsTbls { get; set; }
    }
}
