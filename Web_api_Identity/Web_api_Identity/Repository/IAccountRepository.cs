﻿using Microsoft.AspNetCore.Identity;
using Web_api_Identity.Model;

namespace Web_api_Identity.Repository
{
    public interface IAccountRepository
    {
        Task<IdentityResult> RegisterUser(RegisterModel registerModel);
        Task<string> LoginUser(LoginModel loginModel);
    }
}
