﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;

ConnectionFactory connectionFactory = new ConnectionFactory();
connectionFactory.Uri = new Uri("ampq://aayush:aayush@localhost:5672");
var connection = connectionFactory.CreateConnection();
var channel = connection.CreateModel();
channel.QueueDeclare("Aayush-Fanout-Queue",true,false,false);
channel.QueueBind("Aayush-Fanout-Queue", "Aayush-BookingExchange","");
var eventingBasicConsumer = new EventingBasicConsumer(channel);

eventingBasicConsumer.Received += (Sender, EventArgs) =>
{
    var bookingResponseMessage = Encoding.UTF8.GetString(EventArgs.Body.ToArray());
    Console.WriteLine(bookingResponseMessage);
};

channel.BasicConsume("Aayush-Fanout-Queue",true,eventingBasicConsumer);
Console.ReadLine();