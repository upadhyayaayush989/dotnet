﻿using Food_API.Model;

namespace Food_API.Repository
{
    public interface IFoodRepository
    {
        public int AddFood(FoodItems food);
        public int EditFood(FoodItems food,FoodItems foodToUpdate);
        Task<List<FoodItems>> GetAllFood();

        public int DeleteFood(FoodItems foodExists);

        public FoodItems GetFoodById(int id);
       
    }
}
