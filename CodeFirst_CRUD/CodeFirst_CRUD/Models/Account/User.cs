﻿using System.ComponentModel.DataAnnotations;

namespace CodeFirst_CRUD.Models.Account
{
    public class User
    {
        [Key]
        public int UserId { get; set; }

        [Required(ErrorMessage = "* Please Enter Username")]
        public string UserName { get; set; }
        [DataType(DataType.EmailAddress)]
        [Required(ErrorMessage = "* Please Enter Email")]
        public string Email { get; set; }
        public long Mobile { get; set; }
        [Required(ErrorMessage = "* Please Enter Password")]
        public string Password { get; set; }
        public bool IsActive { get; set; }


    }
}
