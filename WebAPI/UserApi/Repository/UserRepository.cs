﻿using Microsoft.EntityFrameworkCore;
using System.Xml.Linq;
using UserApi.Context;
using UserApi.Model;

namespace UserApi.Repository
{
    public class UserRepository : IUserRepository
    {
        readonly UserDbContext _userDbContext;
        public UserRepository(UserDbContext userDbContext)
        {
            _userDbContext=userDbContext;
        }

        public int BlockUnBlockUser(bool blockUnBlockUser, User userExists)
        {
            userExists.IsBlocked = blockUnBlockUser;
            _userDbContext.Entry(userExists).State = EntityState.Modified;
            return _userDbContext.SaveChanges();
        }

        public int DeleteUser(User userExists)
        {
            _userDbContext.UserList.Remove(userExists);
            return _userDbContext.SaveChanges();
        }

        public int EditUser(User user)
        {
            _userDbContext.Entry(user).State = EntityState.Modified;
            return _userDbContext.SaveChanges();
        }

        public async Task<List<User>> GetAllUsers()
        {
            return await _userDbContext.UserList.ToListAsync();
        }

        public User GetUserById(int id)
        {
            return _userDbContext.UserList.Where(u => u.Id == id).FirstOrDefault();
        }

        public User GetUserByName(string name)
        {
            return _userDbContext.UserList.Where(u => u.Name == name).FirstOrDefault();
        }

        public int RegisterUser(User user)
        {
            _userDbContext.UserList.Add(user);
            return _userDbContext.SaveChanges();
        }

        public User LogIn(string name,string password)
        {
            return _userDbContext.UserList.Where(u => u.Name==name && u.Password == password).FirstOrDefault();
        }
        
    }
}
