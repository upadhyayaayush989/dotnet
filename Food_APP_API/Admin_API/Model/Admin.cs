﻿using System.ComponentModel.DataAnnotations;

namespace Admin_API.Model
{
    public class Admin
    {
        [Key]
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string? Email { get; set; }

        public string? Location { get; set; }
    }
}
