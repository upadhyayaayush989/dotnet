﻿using System;
using System.Collections.Generic;

namespace ApplicantForm.Models
{
    public partial class LoginDetailTbl
    {
        public LoginDetailTbl()
        {
            EducationalDetailTbls = new HashSet<EducationalDetailTbl>();
            PersonalDetailTbls = new HashSet<PersonalDetailTbl>();
            ProfessionalDetailsTbls = new HashSet<ProfessionalDetailsTbl>();
        }

        public int UserId { get; set; }
        public string Email { get; set; } = null!;
        public string? AlternateEmail { get; set; }
        public string FirstName { get; set; } = null!;
        public string LastName { get; set; } = null!;
        public string Password { get; set; } = null!;
        public string WorkTypeId { get; set; } = null!;
        public int StatusId { get; set; }

        public virtual YourStatusTbl Status { get; set; } = null!;
        public virtual ICollection<EducationalDetailTbl> EducationalDetailTbls { get; set; }
        public virtual ICollection<PersonalDetailTbl> PersonalDetailTbls { get; set; }
        public virtual ICollection<ProfessionalDetailsTbl> ProfessionalDetailsTbls { get; set; }
    }
}
