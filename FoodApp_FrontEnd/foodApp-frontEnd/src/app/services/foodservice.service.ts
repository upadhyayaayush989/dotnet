import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Food } from '../models/food';
@Injectable({
  providedIn: 'root'
})
export class FoodserviceService {
  

  constructor(private httpClient:HttpClient) { }
  getAllFood() {
    return this.httpClient.get<Food[]>('https://localhost:7039/api/Food/api/GetAllFood')
  }
}
