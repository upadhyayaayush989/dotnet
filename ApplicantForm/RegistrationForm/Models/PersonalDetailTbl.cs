﻿using System;
using System.Collections.Generic;

namespace RegistrationForm.Models
{
    public partial class PersonalDetailTbl
    {
        public int UserId { get; set; }
        public int PersonalDetailId { get; set; }
        public string? PhoneNumber { get; set; }
        public string? MobileNumber { get; set; }
        public DateTime DateofBirth { get; set; }
        public string? District { get; set; }
        public int? ZipCode { get; set; }
        public int GenderId { get; set; }
        public int MaritalStatusId { get; set; }
        public int CountryId { get; set; }
        public int StateId { get; set; }
        public int CityId { get; set; }
        public int LocalityId { get; set; }

        public virtual CityTbl City { get; set; } = null!;
        public virtual CountryTbl Country { get; set; } = null!;
        public virtual GenderTbl Gender { get; set; } = null!;
        public virtual LocalityTbl Locality { get; set; } = null!;
        public virtual MaritalStatusTbl MaritalStatus { get; set; } = null!;
        public virtual StateTbl State { get; set; } = null!;
        public virtual LoginDetailTbl User { get; set; } = null!;
    }
}
