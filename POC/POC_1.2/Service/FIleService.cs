﻿using HandlebarsDotNet;
using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.Extensions.Configuration;
using MimeKit;
using Newtonsoft.Json;
using OfficeOpenXml;
using POC_1._2.Models;

namespace POC_1._2.Service
{
    public class FIleService : IFileService
    {
        private readonly IWebHostEnvironment _hostingEnvironment;
        readonly IConfiguration _configuration;
        public FIleService(IWebHostEnvironment hostingEnvironment,IConfiguration configuration)
        {
            _hostingEnvironment=hostingEnvironment;
            _configuration=configuration;
        }
        public FileUploadViewModel CreateNewInstanceFileViewModel()
        {
            FileUploadViewModel model = new FileUploadViewModel();
            return model;
        }

        public void GetDataFromExcelFile(FileUploadViewModel model)
        {
            List<StaffInfoViewModel> details = null;
            string rootFolder = _hostingEnvironment.WebRootPath;
            string fileName = Guid.NewGuid().ToString() + model.XlsFile.FileName;
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            FileInfo file = new FileInfo(Path.Combine(rootFolder, fileName));
            using (var stream = new MemoryStream())
            {
                model.XlsFile.CopyToAsync(stream);
                using (var package = new ExcelPackage(stream))
                {
                    package.SaveAs(file);
                }
            }

            using (ExcelPackage package = new ExcelPackage(file))
            {

                ExcelWorksheet worksheet = package.Workbook.Worksheets.FirstOrDefault();
                if (worksheet == null)
                {

                    //return or alert message here
                }
                else
                {

                    var rowCount = worksheet.Dimension.Rows;
                    for (int row = 2; row <= rowCount; row++)
                    {
                        model.StaffInfoViewModel.StaffList.Add(new StaffInfoViewModel
                        {
                            FirstName = (worksheet.Cells[row, 1].Value ?? string.Empty).ToString().Trim(),
                            LastName = (worksheet.Cells[row, 2].Value ?? string.Empty).ToString().Trim(),
                            Email = (worksheet.Cells[row, 3].Value ?? string.Empty).ToString().Trim(),
                        });
                    }
                     details = model.StaffInfoViewModel.StaffList;
                }
            }
            
        }

     

        
        
    }
}
