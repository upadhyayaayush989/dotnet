﻿using System.ComponentModel.DataAnnotations;

namespace FoodApp_Mvc.Models
{
    public class Food
    {
        [Key]
        public int Foodid { get; set; }

        [Required(ErrorMessage = "* Food Name Cannot be Empty")]

        public string FoodName { get; set; }

        [Required(ErrorMessage = "* Please Enter Food Type")]
        public string FoodType { get; set; }

        [Required(ErrorMessage = "* Please Enter the Price ")]
        public int Price { get; set; }

        [Required(ErrorMessage = "* Image Path is Required")]
        public string ImgPath { get; set; }
    }
}
