﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FoodApp_MVC.Models
{
    public class FoodCart
    {
        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int OrderId { get; set; }

        [Required]
        [ForeignKey("FoodMenu")]
        public int FoodId { get; set; }
        public virtual FoodMenu FoodMenu { get; set; }
        public int TotalPrice { get; set; }
        public int Quantity { get; set; }
        [Required]
        [ForeignKey("User")]
        public int UserId { get; set; }
        public virtual User User { get; set; }


    }
}
