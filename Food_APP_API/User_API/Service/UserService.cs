﻿using User_API.Exceptions;
using User_API.Model;
using User_API.Repository;
using User_API.Exceptions;
using Microsoft.Extensions.Configuration;
using System.Net.Mail;
using MailKit.Security;
using MimeKit.Text;
using MimeKit;
using MailKit.Net.Smtp;
using SmtpClient = MailKit.Net.Smtp.SmtpClient;

namespace User_API.Service
{
    public class UserService:IUserService
    {
        readonly IUserRepository _userRepository;
        //readonly IConfiguration _configuration;
        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
            //_configuration = configuration;
        }
        public bool BlockUnBlockUser(int id, bool blockUnBlockUser)
        {
            User userExists = _userRepository.GetUserById(id);
            if (userExists == null)
            {
                return false;
            }
            else
            {
                int blockStatus = _userRepository.BlockUnBlockUser(blockUnBlockUser, userExists);
                if (blockStatus == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public bool DeleteUser(int id)
        {
            User userExists = _userRepository.GetUserById(id);
            if (userExists != null)
            {
                int userDeleteStatus = _userRepository.DeleteUser(userExists);
                if (userDeleteStatus == 1)
                {
                    return true;
                }
                else return false;
            }
            else
            {
                //return false;
                throw new UserDoesnotExistsException ($"User with Id{id} not exists");
            }
        }
        public bool EditUser(int id, User user)
        {
            //User userExists = _userRepository.GetUserById(id);
            //if(userExists == null)
            //{
            //    return false;
            //}
            //else
            //{
            user.Id = id;
            int userEditStatus = _userRepository.EditUser(user);
            if (userEditStatus == 1)
            {
                return true;
            }
            else return false;

            //}
        }

        public async Task<List<User>> GetAllUsers()
        {
            return await _userRepository.GetAllUsers();
        }

        public User LogIn(LoginUser loginUser)
        {
            User user = _userRepository.LogIn(loginUser.UserName, loginUser.Password);
            if (user != null)
            {
                return user;
            }
            else
            {
                throw new UserCredentialInValidException($"{loginUser.UserName} and details are invalid");
            }
        }

        public bool RegisterUser(User user)
        {
            var userExists = _userRepository.GetUserByName(user.UserName);
            if (userExists == null)
            {
                int userRegisterStatus = _userRepository.RegisterUser(user);
                if (userRegisterStatus == 1)
                {
                    return true;
                }
                else return false;
                return true;
            }
            else
            {
                return false;
            }
        }

        //public void SendEmail(EmailDetails request,string userEmail)
        //{
            
        //    var email = new MimeMessage();
        //    email.From.Add(MailboxAddress.Parse(_configuration.GetSection("EmailUserName").Value));
        //    email.To.Add(MailboxAddress.Parse(userEmail));
        //    email.Subject = "Invoice Details";
        //    email.Body = new TextPart(TextFormat.Html) { Text = request.Body };
        //    var smtp = new SmtpClient();
        //    smtp.Connect(_configuration.GetSection("EmailHost").Value, 587, SecureSocketOptions.StartTls);//host and port
        //    smtp.Authenticate(_configuration.GetSection("EmailUserName").Value, _configuration.GetSection("EmailPassword").Value);
        //    smtp.Send(email);
        //    smtp.Disconnect(true);
        //}
    }
}
