﻿using FoodApp_MVC.Context;
using FoodApp_MVC.Exceptions;
using FoodApp_MVC.Models;
using FoodApp_MVC.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace FoodApp_MVC.Controllers
{
    public class UserController : Controller
    {

        private readonly UserDbContext _db;
        private readonly IUserServices _userServices;
        public UserController(UserDbContext db, IUserServices userServices)
        {
            _db = db;
            _userServices = userServices;
        }

        public ActionResult RegisterUser()
        {
            return View();
        }
        [HttpPost]
        public ActionResult RegisterUser(User user)
        {
            if (ModelState.IsValid)
            {
                _userServices.RegisterUser(user);
                TempData["Alert"] = "User Added Successfully. Please LogIn ";
                return View(user);

                //return RedirectToAction("LogIn");
            }
            else
            {
                return View(user);
            }
        }
        public ActionResult LogIn()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(User logInfo)
        {
            try
            {
                User user = _userServices.LogIn(logInfo);
               
                return RedirectToAction("ShowMenu","Food");
            }
            catch (UserCredentialInvalidException ucie)
            {
                return StatusCode(500, ucie.Message);
            }
        }

        

        public IActionResult Index()
        {
            //var displayMenu = _db.FoodMenuTable.ToList();
            //return View(displayMenu);
            return View();
        }

       
    }
}
