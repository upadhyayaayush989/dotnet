﻿using AutoMapper;
using POC_2.Models;
using StudentPersistantLayer.Model;

namespace POC_2
{
    public class StudentMapper:Profile
    {
        public StudentMapper()
        {
            CreateMap<Student,StudentViewModel>();
            
        }

    }
}
