﻿using Microsoft.EntityFrameworkCore;
using RegistrationForm.Models;

namespace RegistrationForm.Repository
{
    public class RegisterRepository : IRegisterRepository
    {
        readonly ApplicantDatabaseContext _applicantDatabaseContext;
        public RegisterRepository(ApplicantDatabaseContext applicantDatabaseContext)
        {
            _applicantDatabaseContext = applicantDatabaseContext;
        }

        public bool IsUserEmailExist(string email)
        {
            bool userExistStatus = _applicantDatabaseContext.LoginDetailTbls.Any(x => x.Email == email);
            return userExistStatus;
        }

        public void AddNewIntitute(string otherInstitute, int? SpecializationId)
        {
            if (!_applicantDatabaseContext.InstituteTbls.Any(x => x.InstituteName == otherInstitute))
            {
                InstituteTbl newInstitute = new InstituteTbl() { InstituteName = otherInstitute,SpecializationId=(int)SpecializationId };
                _applicantDatabaseContext.InstituteTbls.Add(newInstitute);
                _applicantDatabaseContext.SaveChanges();
            }
        }
        public int GetIntituteId(string otherInstitute)
        {
            return _applicantDatabaseContext.InstituteTbls.First(x => x.InstituteName == otherInstitute).InstituteId;
        }

        public bool SaveDataToEducationalDetailsTbl(EducationalDetailTbl educationDetailTbl)
        {
            _applicantDatabaseContext.EducationalDetailTbls.Add(educationDetailTbl);
            return _applicantDatabaseContext.SaveChanges() == 1 ? true : false;
        }

        public int SaveDataToLoginDetailsTbl(LoginDetailTbl loginDetail)
        {
            _applicantDatabaseContext.LoginDetailTbls.Add(loginDetail);
            _applicantDatabaseContext.SaveChanges();
            int userId = _applicantDatabaseContext.LoginDetailTbls.First(x => x.Email == loginDetail.Email).UserId;
            return userId;
        }

        public bool SaveDataToPersonalDetailsTbl(PersonalDetailTbl personalDetail)
        {
            _applicantDatabaseContext.PersonalDetailTbls.Add(personalDetail);
            return _applicantDatabaseContext.SaveChanges() == 1 ? true : false;
        }

        public bool saveDataToProfessionalDetailsTbl(ProfessionalDetailsTbl professionalDetails)
        {
            _applicantDatabaseContext.ProfessionalDetailsTbls.Add(professionalDetails);
            return _applicantDatabaseContext.SaveChanges() == 1 ? true : false;
        }

        public bool saveDataToCandidateKeySkillsTbl(CandidateKeySkillsTbl candidateKeySkills)
        {
            _applicantDatabaseContext.CandidateKeySkillsTbls.Add(candidateKeySkills);
            return _applicantDatabaseContext.SaveChanges() == 1 ? true : false;
        }

       

    }
}
