﻿using Microsoft.AspNetCore.Identity;

namespace POC5.Model
{
    public class ApplicationUser:IdentityUser
    {
        public string? RefreshToken { get; set; }
        public DateTime RefreshTokenExpiryTime { get; set; }
    }
}
