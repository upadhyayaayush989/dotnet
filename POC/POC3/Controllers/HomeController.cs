﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using POC3.Models;
using POC3_Persistant.CQRS.Commands;
using POC3_Persistant.CQRS.Queries;
using POC3_Persistant.Model;
using System.Diagnostics;

namespace POC3.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IMediator _mediator;

        public HomeController(ILogger<HomeController> logger,IMediator mediator)
        {
            _logger = logger;
            _mediator = mediator;
        }


        public async Task<IActionResult> AddProduct()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> AddProduct(CreateProductCommand command)
        {
            await _mediator.Send(command);
            return View();
        }

        [HttpGet]
        public async Task <IActionResult> GetAllProducts(GetAllProducts request)
        {
            List<Product> products = await _mediator.Send(request);
            return View(products);
        }

        [HttpGet]
        public async  Task<IActionResult> UpdateProduct(GetProductByIdCommand command)
        {
            
            return View(await _mediator.Send(command));
        }

        [HttpPost]
        public async Task<IActionResult> UpdateProduct(UpdateProductCommand command)
        {
            await _mediator.Send(command);
            return RedirectToAction("GetAllProducts");
        }

        public async Task<IActionResult> DeleteProduct(DeleteProductCommand command)
        {
            await _mediator.Send(command);
            return RedirectToAction("GetAllProducts");
        }








        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        
    }
}