import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Adminlogin } from 'src/app/models/adminlogin';

@Injectable({
  providedIn: 'root'
})
export class AdminloginService {

  constructor(private httpClient:HttpClient) { }

  LoginAdmin(loginAdmin:Adminlogin):Observable<Adminlogin>{
    return this.httpClient.post<Adminlogin>('https://localhost:7129/api/Admin/LogIn',loginAdmin);
  }
}
