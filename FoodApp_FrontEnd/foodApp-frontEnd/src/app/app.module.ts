import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AdmindashboardComponent } from './components/admindashboard/admindashboard.component';
import { AddfoodComponent } from './components/admindashboard/addfood/addfood.component';
import { FormsModule } from '@angular/forms';
import { EditfoodComponent } from './components/admindashboard/editfood/editfood.component';
import { ViewcartComponent } from './components/dashboard/viewcart/viewcart.component';
import { HomepageComponent } from './components/homepage/homepage.component';
import { GeneratebillComponent } from './components/dashboard/generatebill/generatebill.component';
import { Pipe, PipeTransform } from '@angular/core';
import { AdminloginComponent } from './components/adminlogin/adminlogin.component';
import { FooterComponent } from './components/footer/footer.component';
import { AuthService } from './authorize/services/auth.service';
import { AuthGuard } from './authorize/auth.guard';
import { TokenInterceptorService } from './services/token-interceptor.service';
import {MatButtonModule} from '@angular/material/button';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';



@NgModule({
  declarations: [
    HomepageComponent,
    AppComponent,
    NavbarComponent,
    LoginComponent,
    SignupComponent,
    DashboardComponent,
    AdmindashboardComponent,
    AddfoodComponent,
    EditfoodComponent,
    ViewcartComponent,

    GeneratebillComponent,
    AdminloginComponent,
    FooterComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    MatButtonModule
  ],
  providers: [AuthService,AuthGuard,{
    provide:HTTP_INTERCEPTORS,
    useClass:TokenInterceptorService,
    multi:true
  }],
  
  bootstrap: [AppComponent]
})
export class AppModule { }
