import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FoodCart } from 'src/app/models/food-cart';
import { BookingService } from 'src/app/services/user/booking.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-viewcart',
  templateUrl: './viewcart.component.html',
  styleUrls: ['./viewcart.component.css']
})
export class ViewcartComponent implements OnInit {

  viewCart?:FoodCart[];
  userName:string = localStorage.getItem("username")!;
  constructor(private bookingService:BookingService,private router:Router) { 

  }

  ngOnInit(): void {
    this.ViewCartList();
    }

  ViewCartList(){
    this.bookingService.ViewCart(this.userName).subscribe(res => {
      console.log(res);
      this.viewCart = res;

  })
  }


  DeleteItem(id?:number) {
    
      Swal.fire({
        title: 'Are you sure?',
        text: "",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result)=>{
        if(result.isConfirmed){
          this.bookingService.RemoveFromCart(id).subscribe(res => {
          console.log(res);
        });
        Swal.fire(
          'Deleted!',
          'Food removed from cart.',
          'success'
        );
      }
      this.ViewCartList();
    
    // this.router.navigate(['userCart']);

}
 )}
 reloadCurrentPage(){
   window.location.reload();
 }
GotoGenerateBill(){
  this.router.navigate(['generatebill']);
}
GotoMenu(){
  this.router.navigate(['dashboard']);
}
  
}
