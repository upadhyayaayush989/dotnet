﻿using CodeFirst_CRUD.Models;
using CodeFirst_CRUD.Services;
using Microsoft.AspNetCore.Mvc;

namespace CodeFirst_CRUD.Controllers
{
    public class FoodMenuController : Controller
    {
        IUserServices _userServices;
        public FoodMenuController(IUserServices userServices)
        {
            _userServices = userServices;
        }

        public IActionResult Dasboard()
        {
            List<Food> viewFood = _userServices.GetProducts();
            return View(viewFood);

        }

        public IActionResult AddToCart(int id)
        {
            _userServices.AddValues(id);


            return RedirectToAction("Index");

        }

        public IActionResult ViewCart()
        {
            List<FoodCart> viewCart = _userServices.GetCart();

            return View(viewCart);
        }

        public IActionResult Delete(int id)
        {
            _userServices.DeleteItemFromCart(id);
            return RedirectToAction("ViewCart");
        }
        public IActionResult GenerateBill()
        {
            int gen = _userServices.GenerateBill();
            List<FoodCart> viewCart = _userServices.GetCart();
            ViewBag.bill = gen;
            return View(viewCart);
        }

        public IActionResult RemoveElements()
        {
            _userServices.RemoveItems();
            return RedirectToAction("Index");
        }

        public IActionResult Index()
        {
            List<Food> viewFood = _userServices.GetProducts();
            return View(viewFood);

        }
    }
}
