﻿using CodeFirst_CRUD.Models;

namespace CodeFirst_CRUD.Repository
{
    public interface IUserRepository
    {
        List<Food> GetAllProducts();

        void AddValuesToCart(int id);
        List<FoodCart> GetCart();

        void DeleteElement(int id);
        int GenerateBill();
        void RemoveElements();
    }
}
