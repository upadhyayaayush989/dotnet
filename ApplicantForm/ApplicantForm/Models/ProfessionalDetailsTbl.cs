﻿using System;
using System.Collections.Generic;

namespace ApplicantForm.Models
{
    public partial class ProfessionalDetailsTbl
    {
        public int ProfessionalDetailId { get; set; }
        public bool RelocationStatus { get; set; }
        public int TotalExperienceInYears { get; set; }
        public int TotalExperienceInMonths { get; set; }
        public string? KeySkills { get; set; }
        public string? CurrentEmployer { get; set; }
        public string? CurrentDesignation { get; set; }
        public string? PreviousEmployer { get; set; }
        public string? PreviousDesignation { get; set; }
        public string? ResumeTitle { get; set; }
        public int? CtcinLakh { get; set; }
        public int? CtcinThousand { get; set; }
        public string? ResumePath { get; set; }
        public string? CopyResume { get; set; }
        public int UserId { get; set; }
        public int? PreferedLocationId { get; set; }
        public int JobCategoryId { get; set; }
        public int? CurrentIndustryId { get; set; }
        public int? NoticePeriodId { get; set; }

        public virtual CurrentIndustryTbl? CurrentIndustry { get; set; }
        public virtual JobCategoryTbl JobCategory { get; set; } = null!;
        public virtual NoticePeriodTbl? NoticePeriod { get; set; }
        public virtual PreferedLocationTbl? PreferedLocation { get; set; }
        public virtual LoginDetailTbl User { get; set; } = null!;
    }
}
