﻿using FoodApp_MVC.Context;
using FoodApp_MVC.Models;
using System.Xml.Linq;

namespace FoodApp_MVC.Repository
{
    public class UserRepository:IUserRepository
    {
        UserDbContext _db;
        public UserRepository(UserDbContext db)
        {
            _db = db;
        }
        public List<User> GetAllUsers()
        {
            return _db.UserCredentials.ToList();
        }

        public User GetUserByUserName(string username)
        {
            return _db.UserCredentials.Where(u => u.UserName == username).FirstOrDefault();
        }


        public bool RegisterUser(User user)
        {
            _db.UserCredentials.Add(user);
            return _db.SaveChanges() == 1 ? true : false;
        }
        public User LogIn(string username, string password)
        {
            return _db.UserCredentials.Where(u => u.UserName == username && u.Password == password).FirstOrDefault();
        }

        public List<FoodMenu> GetAllFood()
        {
            return _db.FoodMenuTable.ToList();
        }

        public bool AddFoodToCart(FoodCart item)
        {
            _db.FoodCart.Add(item);
            return _db.SaveChanges()==1? true : false;
        }

        public void AddValues(int id)
        {
            FoodMenu newItem = _db.FoodMenuTable.Where(w => w.FoodId == id).FirstOrDefault();
            string ItemName = newItem.FoodName;
            string img = newItem.ImgPath;
            int price = newItem.Price;
            int quantity = 1;
            int totalPrice = quantity * price;
            FoodCart newFood = new FoodCart() { FoodId=id,Quantity=quantity,TotalPrice=totalPrice};
        }
    }
}
