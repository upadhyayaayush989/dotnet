﻿using System;
using System.Collections.Generic;

namespace RegistrationForm.Models
{
    public partial class PreferedLocationTbl
    {
        public PreferedLocationTbl()
        {
            ProfessionalDetailsTbls = new HashSet<ProfessionalDetailsTbl>();
        }

        public int PreferedLocationId { get; set; }
        public string OfficeLocation { get; set; } = null!;

        public virtual ICollection<ProfessionalDetailsTbl> ProfessionalDetailsTbls { get; set; }
    }
}
