import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { EmailDetails } from 'src/app/models/email-details';

@Injectable({
  providedIn: 'root'
})
export class EmailService {

  constructor(private httpClient:HttpClient) { }
  SendEmail(userEmail:string,emailDetails:EmailDetails):Observable<void>{
    return this.httpClient.post<void>('https://localhost:7072/api/User/SendEmail?userName='+userEmail,emailDetails)
  }

}
