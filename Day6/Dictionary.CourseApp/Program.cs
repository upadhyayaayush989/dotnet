﻿//write a program to store Course Codes and Course Names in a dictionary 


Console.WriteLine("Demo Dictionary");

//creating a dictionary in c#

Dictionary<int,string> courses = new Dictionary<int,string>();
//add items in dictionary

courses.Add(101, "Progamming in c#");
courses.Add(102, "Python beginner to expert");
courses.Add(103, "Web developement using ASP .net ");

foreach(KeyValuePair<int,string> course in courses)
{
    Console.WriteLine($"Key::{course.Key}\t Values:: {course.Value}");
}

Console.WriteLine($"Course Key Available:: {courses.ContainsKey(102)}");


courses.Remove(101);

foreach (KeyValuePair<int, string> course in courses)
{
    Console.WriteLine($"Key::{course.Key}\t Values:: {course.Value}");
}

