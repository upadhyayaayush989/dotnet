﻿

using ServiceStack.DataAnnotations;

namespace Hackathonpro.Models
{
    public class Food
    {
        public int Id { get; set; }
        public string FoodName { get; set; }
        public int Price { get; set; }
        public string ImgSource { get; set; }
    }
}
