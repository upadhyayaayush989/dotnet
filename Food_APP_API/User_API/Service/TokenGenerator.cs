﻿using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace User_API.Service
{
    public class TokenGenerator : ITokenGenerator
    {
        public string GenerateToken(int id, string username,string userEmail)
        {
            var userClaims = new Claim[]
             {
                 new Claim(JwtRegisteredClaimNames.Jti,new Guid().ToString()),
                 new Claim(JwtRegisteredClaimNames.UniqueName,username)

             };

            var userSecurityKey = Encoding.UTF8.GetBytes("ssskljkjsiNHDisnjaaanjkdhc");
            var userSymmetricSecurity = new SymmetricSecurityKey(userSecurityKey);
            var userSiginCredentials = new SigningCredentials(userSymmetricSecurity, SecurityAlgorithms.HmacSha256);
            var userJwtSecurityToken = new JwtSecurityToken
                (
                   issuer: "MVCCore",
                   audience: "CoreMVCCore",
                   claims: userClaims,
                   expires: DateTime.UtcNow.AddMinutes(60),
                   signingCredentials: userSiginCredentials
                );
            var userSecurityTokenHandler = new JwtSecurityTokenHandler().WriteToken(userJwtSecurityToken);
            string userJwtSecutrityTokenHandler = JsonConvert.SerializeObject(new { Token = userSecurityTokenHandler, Name = username,Email=userEmail});
            return userJwtSecutrityTokenHandler;
        }

        public bool IsTokenValid(string userSecretKey, string userIssuer, string userAudience, string userToken)
        {
            var userSecretKeyinBytes = Encoding.UTF8.GetBytes(userSecretKey);
            var userSymmetricSecurity = new SymmetricSecurityKey(userSecretKeyinBytes);
            var tokenValidationParameters = new TokenValidationParameters()
            {
                ValidateIssuer = true,
                ValidIssuer = userIssuer,

                ValidateAudience = true,
                ValidAudience = userAudience,

                ValidateIssuerSigningKey = true,
                IssuerSigningKey = userSymmetricSecurity,

                ValidateLifetime = true
            };
            JwtSecurityTokenHandler tokenValidationHandler = new JwtSecurityTokenHandler();
            try
            {
                tokenValidationHandler.ValidateToken(userToken, tokenValidationParameters, out SecurityToken securityToken);
                return true;
            }
            catch
            {
                return false;
            }

        }
    }
}
