﻿using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;

namespace UserApi.Service
{
    public class TokenGenerator : ITokenGenerator
    {
        //public string GenerateRefreshToken()
        //{
        //    var randomNumber = new byte[32];
        //    using var rng = RandomNumberGenerator.Create())
        //    {
        //        rng.GetBytes(randomNumber);
        //        return

        //    }


        //}

        public string GenerateToken(int id, string name)
        {
            var userClaims = new Claim[]
             {
                 new Claim(JwtRegisteredClaimNames.Jti,new Guid().ToString()),
                 new Claim(JwtRegisteredClaimNames.UniqueName,name)

             };

            var userSecurityKey = Encoding.UTF8.GetBytes("ssskljkjsiNHDisnjaaanjkdhc");
            var userSymmetricSecurity = new SymmetricSecurityKey(userSecurityKey);
            var userSiginCredentials = new SigningCredentials(userSymmetricSecurity, SecurityAlgorithms.HmacSha256);
            var userJwtSecurityToken = new JwtSecurityToken
                (
                   issuer: "MVCCore",
                   audience: "CoreMVCCore",
                   claims: userClaims,
                   expires: DateTime.UtcNow.AddMinutes(10),
                   signingCredentials: userSiginCredentials
                );
            var userSecurityTokenHandler = new JwtSecurityTokenHandler().WriteToken(userJwtSecurityToken);
            string userJwtSecutrityTokenHandler = JsonConvert.SerializeObject(new {Token=userSecurityTokenHandler,Name=name});
            return userJwtSecutrityTokenHandler;
        }

        public ClaimsPrincipal GetPrincipalFromExpiredToken(string token)
        {
            throw new NotImplementedException();
        }

        public bool IsTokenValid(string userSecretKey, string userIssuer, string userAudience, string userToken)
        {
            var userSecretKeyinBytes = Encoding.UTF8.GetBytes(userSecretKey);
            var userSymmetricSecurity = new SymmetricSecurityKey(userSecretKeyinBytes);
            var tokenValidationParameters = new TokenValidationParameters()
            {
                ValidateIssuer = true,
                ValidIssuer = userIssuer,

                ValidateAudience = true,
                ValidAudience = userAudience,

                ValidateIssuerSigningKey = true,
                IssuerSigningKey = userSymmetricSecurity,

                ValidateLifetime = true
            };
            JwtSecurityTokenHandler tokenValidationHandler = new JwtSecurityTokenHandler();
            try
            {
                tokenValidationHandler.ValidateToken(userToken, tokenValidationParameters, out SecurityToken securityToken);
                return true;
            }
            catch
            {
                return false;
            }

            


            //if (userToken != null)
            //{
            //    return true;
            //}
            //else return false;
        }

        //public ClaimsPrincipal GetPrincipalFromExpiredToken(string token)
        //{

        //}

    }
}
