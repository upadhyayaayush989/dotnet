﻿namespace WebApi_Intro
{
    public class CustomMiddleware : IMiddleware
    {
        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            await context.Response.WriteAsync("Executing from Custom middleware");
            await next(context);
            await context.Response.WriteAsync("Executing 2nd Time from Custom middleware");

        }
    }
}
