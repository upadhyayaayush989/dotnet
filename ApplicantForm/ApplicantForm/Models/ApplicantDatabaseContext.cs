﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace ApplicantForm.Models
{
    public partial class ApplicantDatabaseContext : DbContext
    {
        public ApplicantDatabaseContext()
        {
        }

        public ApplicantDatabaseContext(DbContextOptions<ApplicantDatabaseContext> options)
            : base(options)
        {
        }

        public virtual DbSet<CityTbl> CityTbls { get; set; } = null!;
        public virtual DbSet<CountryTbl> CountryTbls { get; set; } = null!;
        public virtual DbSet<CurrentIndustryTbl> CurrentIndustryTbls { get; set; } = null!;
        public virtual DbSet<EducationalDetailTbl> EducationalDetailTbls { get; set; } = null!;
        public virtual DbSet<GenderTbl> GenderTbls { get; set; } = null!;
        public virtual DbSet<HighestQualificationTbl> HighestQualificationTbls { get; set; } = null!;
        public virtual DbSet<InstituteTbl> InstituteTbls { get; set; } = null!;
        public virtual DbSet<JobCategoryTbl> JobCategoryTbls { get; set; } = null!;
        public virtual DbSet<LocalityTbl> LocalityTbls { get; set; } = null!;
        public virtual DbSet<LoginDetailTbl> LoginDetailTbls { get; set; } = null!;
        public virtual DbSet<LookingForTbl> LookingForTbls { get; set; } = null!;
        public virtual DbSet<MaritalStatusTbl> MaritalStatusTbls { get; set; } = null!;
        public virtual DbSet<NoticePeriodTbl> NoticePeriodTbls { get; set; } = null!;
        public virtual DbSet<PersonalDetailTbl> PersonalDetailTbls { get; set; } = null!;
        public virtual DbSet<PreferedLocationTbl> PreferedLocationTbls { get; set; } = null!;
        public virtual DbSet<ProfessionalDetailsTbl> ProfessionalDetailsTbls { get; set; } = null!;
        public virtual DbSet<SpecializationTbl> SpecializationTbls { get; set; } = null!;
        public virtual DbSet<StateTbl> StateTbls { get; set; } = null!;
        public virtual DbSet<YourStatusTbl> YourStatusTbls { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=DESKTOP-IKB2EQL;Database='ApplicantDatabase';Trusted_Connection=True");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CityTbl>(entity =>
            {
                entity.HasKey(e => e.CityId)
                    .HasName("PK__CityTbl__F2D21B76CC3D463F");

                entity.ToTable("CityTbl");

                entity.Property(e => e.CityId).ValueGeneratedNever();

                entity.Property(e => e.CityName)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.HasOne(d => d.State)
                    .WithMany(p => p.CityTbls)
                    .HasForeignKey(d => d.StateId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Fk_StateTbl_Id");
            });

            modelBuilder.Entity<CountryTbl>(entity =>
            {
                entity.HasKey(e => e.CountryId)
                    .HasName("PK__CountryT__10D1609F595EEA15");

                entity.ToTable("CountryTbl");

                entity.Property(e => e.CountryId).ValueGeneratedNever();

                entity.Property(e => e.CountryName)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<CurrentIndustryTbl>(entity =>
            {
                entity.HasKey(e => e.CurrentIndustryId)
                    .HasName("PK__CurrentI__E18DC26633AFC77A");

                entity.ToTable("CurrentIndustryTbl");

                entity.Property(e => e.CurrentIndustryId).ValueGeneratedNever();

                entity.Property(e => e.CurrentIndustryName)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<EducationalDetailTbl>(entity =>
            {
                entity.ToTable("EducationalDetailTbl");

                entity.Property(e => e.OtherInstitute)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.HasOne(d => d.HighestQualification)
                    .WithMany(p => p.EducationalDetailTbls)
                    .HasForeignKey(d => d.HighestQualificationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Fk_HighestQualificationTblForEducationDetailTbl_Id");

                entity.HasOne(d => d.Institute)
                    .WithMany(p => p.EducationalDetailTbls)
                    .HasForeignKey(d => d.InstituteId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Fk_InstituteTblForEducationDetailTbl_InstituteId");

                entity.HasOne(d => d.Specialization)
                    .WithMany(p => p.EducationalDetailTbls)
                    .HasForeignKey(d => d.SpecializationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Fk_SpecializationTblForEducationDetailTbl_SpecializationId");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.EducationalDetailTbls)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Fk_LoginDetailTbl_UserIdForEducationDetailTbl");
            });

            modelBuilder.Entity<GenderTbl>(entity =>
            {
                entity.HasKey(e => e.GenderId)
                    .HasName("PK__GenderTb__4E24E9F78B475D87");

                entity.ToTable("GenderTbl");

                entity.Property(e => e.GenderId).ValueGeneratedNever();

                entity.Property(e => e.Gender)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<HighestQualificationTbl>(entity =>
            {
                entity.HasKey(e => e.HighestQualificationId)
                    .HasName("PK__HighestQ__7348BE2A2DD92D30");

                entity.ToTable("HighestQualificationTbl");

                entity.Property(e => e.HighestQualificationId).ValueGeneratedNever();

                entity.Property(e => e.QualificationName)
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<InstituteTbl>(entity =>
            {
                entity.HasKey(e => e.InstituteId)
                    .HasName("PK__Institut__09EC0DBB5147DCFE");

                entity.ToTable("InstituteTbl");

                entity.Property(e => e.InstituteId).ValueGeneratedNever();

                entity.Property(e => e.InstituteName)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.HasOne(d => d.Specialization)
                    .WithMany(p => p.InstituteTbls)
                    .HasForeignKey(d => d.SpecializationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Fk_SpecializationTbl_SpecializationId");
            });

            modelBuilder.Entity<JobCategoryTbl>(entity =>
            {
                entity.HasKey(e => e.JobCategoryId)
                    .HasName("PK__JobCateg__302BAD2D94F6AFC9");

                entity.ToTable("JobCategoryTbl");

                entity.Property(e => e.JobCategoryId).ValueGeneratedNever();

                entity.Property(e => e.JobCategory)
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LocalityTbl>(entity =>
            {
                entity.HasKey(e => e.LocalityId)
                    .HasName("PK__Locality__179EA7221E9817F8");

                entity.ToTable("LocalityTbl");

                entity.Property(e => e.LocalityId).ValueGeneratedNever();

                entity.Property(e => e.LocalityName)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.HasOne(d => d.City)
                    .WithMany(p => p.LocalityTbls)
                    .HasForeignKey(d => d.CityId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Fk_CityTbl_Id");
            });

            modelBuilder.Entity<LoginDetailTbl>(entity =>
            {
                entity.HasKey(e => e.UserId)
                    .HasName("PK__LoginDet__1788CC4CFDDA24A0");

                entity.ToTable("LoginDetailTbl");

                entity.Property(e => e.UserId).ValueGeneratedNever();

                entity.Property(e => e.AlternateEmail)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FirstName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.WorkTypeId)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.LoginDetailTbls)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Fk_YourStatusTbl_Id");
            });

            modelBuilder.Entity<LookingForTbl>(entity =>
            {
                entity.HasKey(e => e.LookingForId)
                    .HasName("PK__LookingF__DF7500E9995C757A");

                entity.ToTable("LookingForTbl");

                entity.Property(e => e.LookingForId).ValueGeneratedNever();

                entity.Property(e => e.WorkType)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<MaritalStatusTbl>(entity =>
            {
                entity.HasKey(e => e.MaritalStatusId)
                    .HasName("PK__MaritalS__C8B1BA72BE805D9C");

                entity.ToTable("MaritalStatusTbl");

                entity.Property(e => e.MaritalStatusId).ValueGeneratedNever();

                entity.Property(e => e.MaritalStatus)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<NoticePeriodTbl>(entity =>
            {
                entity.HasKey(e => e.NoticePeriodid)
                    .HasName("PK__NoticePe__A3BB7B205684FFAF");

                entity.ToTable("NoticePeriodTbl");

                entity.Property(e => e.TotalNoticePeriod)
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<PersonalDetailTbl>(entity =>
            {
                entity.HasKey(e => e.PersonalDetailId)
                    .HasName("PK__Personal__D8893CE010A3689F");

                entity.ToTable("PersonalDetailTbl");

                entity.Property(e => e.PersonalDetailId).ValueGeneratedNever();

                entity.Property(e => e.DateofBirth).HasColumnType("date");

                entity.Property(e => e.District)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.MobileNumber)
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneNumber)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.HasOne(d => d.City)
                    .WithMany(p => p.PersonalDetailTbls)
                    .HasForeignKey(d => d.CityId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Fk_CityTblForPersonalDetailTbl_Id");

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.PersonalDetailTbls)
                    .HasForeignKey(d => d.CountryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Fk_CountryTblForPersonalDetailTbl_Id");

                entity.HasOne(d => d.Gender)
                    .WithMany(p => p.PersonalDetailTbls)
                    .HasForeignKey(d => d.GenderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Fk_GenderTbl_Id");

                entity.HasOne(d => d.Locality)
                    .WithMany(p => p.PersonalDetailTbls)
                    .HasForeignKey(d => d.LocalityId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Fk_LocalityTbl_Id");

                entity.HasOne(d => d.MaritalStatus)
                    .WithMany(p => p.PersonalDetailTbls)
                    .HasForeignKey(d => d.MaritalStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Fk_MaritalStatusTbl_Id");

                entity.HasOne(d => d.State)
                    .WithMany(p => p.PersonalDetailTbls)
                    .HasForeignKey(d => d.StateId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Fk_StateTblForPersonalDetailTbl_Id");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.PersonalDetailTbls)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Fk_LoginDetailTbl_UserId");
            });

            modelBuilder.Entity<PreferedLocationTbl>(entity =>
            {
                entity.HasKey(e => e.PreferedLocationId)
                    .HasName("PK__Prefered__95A748DDEBBB8E1D");

                entity.ToTable("PreferedLocationTbl");

                entity.Property(e => e.PreferedLocationId).ValueGeneratedNever();

                entity.Property(e => e.OfficeLocation)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ProfessionalDetailsTbl>(entity =>
            {
                entity.HasKey(e => e.ProfessionalDetailId)
                    .HasName("PK__Professi__134B9220C44072B1");

                entity.ToTable("ProfessionalDetailsTbl");

                entity.Property(e => e.CopyResume).IsUnicode(false);

                entity.Property(e => e.CtcinLakh).HasColumnName("CTCInLakh");

                entity.Property(e => e.CtcinThousand).HasColumnName("CTCInThousand");

                entity.Property(e => e.CurrentDesignation)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CurrentEmployer)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.KeySkills)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.PreviousDesignation)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PreviousEmployer)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ResumePath)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ResumeTitle)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CurrentIndustry)
                    .WithMany(p => p.ProfessionalDetailsTbls)
                    .HasForeignKey(d => d.CurrentIndustryId)
                    .HasConstraintName("Fk_CurrentIndustryTblForProfessionalDetailTbl_CurrentIndustryId");

                entity.HasOne(d => d.JobCategory)
                    .WithMany(p => p.ProfessionalDetailsTbls)
                    .HasForeignKey(d => d.JobCategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Fk_JobCategoryTbl_JobCategoryId");

                entity.HasOne(d => d.NoticePeriod)
                    .WithMany(p => p.ProfessionalDetailsTbls)
                    .HasForeignKey(d => d.NoticePeriodId)
                    .HasConstraintName("Fk_NoticePeriodTblForPersonalDetailTbl_NoticePeriodId");

                entity.HasOne(d => d.PreferedLocation)
                    .WithMany(p => p.ProfessionalDetailsTbls)
                    .HasForeignKey(d => d.PreferedLocationId)
                    .HasConstraintName("Fk_PreferedLocationTbl_PreferedLocationId");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.ProfessionalDetailsTbls)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Fk_LoginDetailTblForProfessionalDetailTbl_UserId");
            });

            modelBuilder.Entity<SpecializationTbl>(entity =>
            {
                entity.HasKey(e => e.SpecializationId)
                    .HasName("PK__Speciali__5809D86FC94326BB");

                entity.ToTable("SpecializationTbl");

                entity.Property(e => e.SpecializationId).ValueGeneratedNever();

                entity.Property(e => e.SpecializationName)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.HasOne(d => d.HighestQualification)
                    .WithMany(p => p.SpecializationTbls)
                    .HasForeignKey(d => d.HighestQualificationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Fk_HighestQualificationTbl_HighestQualificationId");
            });

            modelBuilder.Entity<StateTbl>(entity =>
            {
                entity.HasKey(e => e.StateId)
                    .HasName("PK__StateTbl__C3BA3B3ACB3B0742");

                entity.ToTable("StateTbl");

                entity.Property(e => e.StateName)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.StateTbls)
                    .HasForeignKey(d => d.CountryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CountryTbl_Id");
            });

            modelBuilder.Entity<YourStatusTbl>(entity =>
            {
                entity.HasKey(e => e.YourStatusId)
                    .HasName("PK__YourStat__C6FD580E8C25ED93");

                entity.ToTable("YourStatusTbl");

                entity.Property(e => e.YourStatusId).ValueGeneratedNever();

                entity.Property(e => e.StatusType)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
