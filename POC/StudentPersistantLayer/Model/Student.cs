﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentPersistantLayer.Model
{
    public  class Student
    {
        public int StudentId { get; set; }
        public string StudentName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        public string StudentLocation { get; set; }
        public string StudentBranch { get; set; }
    }
}
