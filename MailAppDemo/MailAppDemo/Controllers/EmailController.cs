﻿using MailAppDemo.Model;
using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MimeKit;
using MimeKit.Text;

namespace MailAppDemo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmailController : ControllerBase
    {
        readonly IEmailService _emailService;
        public EmailController(IEmailService emailService)
        {
            _emailService=emailService;
        }
        [HttpPost]
        public IActionResult SendEmail(EmailDetails request,string userEmail)
        {
            _emailService.SendEmail(request,userEmail);
            return Ok();


        }
    }
}
