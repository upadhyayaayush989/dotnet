﻿namespace Admin_API.Exceptions
{
    public class AdminCredentialInValidException:ApplicationException
    {
        public AdminCredentialInValidException()
        {

        }
        public AdminCredentialInValidException(string msg):base(msg)
        {

        }
    }
}
