﻿using Microsoft.EntityFrameworkCore;
using User_API.Context;
using User_API.Model;

namespace User_API.Repository
{
    public class UserRepository:IUserRepository
    {
        readonly UserDbContext _userDbContext;
        public UserRepository(UserDbContext userDbContext)
        {
            _userDbContext = userDbContext;
        }

        public int BlockUnBlockUser(bool blockUnBlockUser, User userExists)
        {
            userExists.IsBlocked = blockUnBlockUser;
            _userDbContext.Entry(userExists).State = EntityState.Modified;
            return _userDbContext.SaveChanges();
        }

        public int DeleteUser(User userExists)
        {
            _userDbContext.Users.Remove(userExists);
            return _userDbContext.SaveChanges();
        }

        public int EditUser(User user)
        {
            _userDbContext.Entry(user).State = EntityState.Modified;
            return _userDbContext.SaveChanges();
        }

        public async Task<List<User>> GetAllUsers()
        {
            return await _userDbContext.Users.ToListAsync();
        }

        public User GetUserById(int id)
        {
            return _userDbContext.Users.Where(u => u.Id == id).FirstOrDefault();
        }

        public User GetUserByName(string name)
        {
            return _userDbContext.Users.Where(u => u.UserName == name).FirstOrDefault();
        }

        public int RegisterUser(User user)
        {
            _userDbContext.Users.Add(user);
            return _userDbContext.SaveChanges();
        }

        public User LogIn(string name, string password)
        {
            return _userDbContext.Users.Where(u => u.UserName == name && u.Password == password).FirstOrDefault();
        }

        public string GetUserEmailByUserName(string userName)
        {
           User user = _userDbContext.Users.Where(u => u.UserName == userName).FirstOrDefault();
            return user.Email;
        }
    }
}
