import { Component, OnInit } from '@angular/core';
import { AddfoodService } from 'src/app/services/admin/addfood.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Food } from 'src/app/models/food';
import { Route, Router } from '@angular/router';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-addfood',
  templateUrl: './addfood.component.html',
  styleUrls: ['./addfood.component.css']
})
export class AddfoodComponent implements OnInit {

  food:Food
  constructor(private adminFoodService:AddfoodService,private router:Router) { 
    this.food=new Food();
  }
  

  ngOnInit(): void {
    // this.AddFood();
  }

  AddFood(){
    this.adminFoodService.AddFood(this.food).subscribe(res=>{
      if(res){
        console.log("added Succesfully");
        this.router.navigate(["admindashboard"]);
        Swal.fire(
          'Added Successfully',
          'Food Successfully Added to the Menu ',
          'success'
        );
        
      }
      else{
        console.log("Unsuccessful");
        
      }
      
    })
  }


}
