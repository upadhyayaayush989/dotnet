﻿namespace User_API.Service
{
    public interface ITokenGenerator
    {
        string GenerateToken(int id, string username,string userEmail);

        public bool IsTokenValid(string userSecretKey, string userIssuer, string userAudience, string userToken);
    }
}
