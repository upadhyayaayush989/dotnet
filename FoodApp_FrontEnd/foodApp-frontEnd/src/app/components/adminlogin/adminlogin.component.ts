import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Adminlogin } from 'src/app/models/adminlogin';
import { AdminloginService } from 'src/app/services/admin/adminlogin.service';

@Component({
  selector: 'app-adminlogin',
  templateUrl: './adminlogin.component.html',
  styleUrls: ['./adminlogin.component.css']
})
export class AdminloginComponent implements OnInit {

  loginAdmin:Adminlogin;
  constructor(private adminLoginService:AdminloginService,private router:Router) {
    this.loginAdmin=new Adminlogin();
   }

  ngOnInit(): void {
  }
  LoginAdmin(){
   this.adminLoginService.LoginAdmin(this.loginAdmin).subscribe(res=>{
    if(res){
      let adminName = this.loginAdmin.userName;
      let jsonObject = JSON.stringify(res);
      let jsonToken = JSON.parse(jsonObject);
      localStorage.setItem("userToken", jsonToken["Token"]);
      localStorage.setItem("username",adminName! );
      console.log("admin login successful");
      
      this.router.navigate(['admindashboard']);
    }
    else{
      console.log("admin login unsuccessfull");
      
    }
   }) 
  }

}
