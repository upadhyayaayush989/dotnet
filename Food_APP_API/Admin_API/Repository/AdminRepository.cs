﻿using Admin_API.Context;
using Admin_API.Model;

namespace Admin_API.Repository
{
    public class AdminRepository:IAdminRepository
    {
        readonly AdminDbContext _adminDbContext;
        public AdminRepository(AdminDbContext adminDbContext)
        {
            _adminDbContext = adminDbContext;
        }

        public Admin? LogIn(string username, string password)
        {
            return _adminDbContext.AdminUsers.Where(u => u.UserName == username && u.Password == password).FirstOrDefault();
        }
    }
}
