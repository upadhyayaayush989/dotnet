﻿using MediatR;
using POC3_Persistant.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POC3_Persistant.CQRS.Queries
{
    public class GetAllProducts : IRequest<List<Product>>
    {
        
    }

    public class GetAllProductsQueryHandler : IRequestHandler<GetAllProducts, List<Product>>
    {
        private readonly ProductDbContext _context;
        public GetAllProductsQueryHandler(ProductDbContext context)
        {
            _context = context;
        }
        public async Task<List<Product>> Handle(GetAllProducts request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_context.Products.ToList());
        }
    }
}
