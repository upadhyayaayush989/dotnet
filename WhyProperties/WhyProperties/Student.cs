﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WhyProperties
{
    public class Student
    {
        private int _id;
        private string _Name;
        private int _passMark = 35;
        public string Email { get; set; }
        public string City { get; set; }

        public int PassMark
        {
            get
            { 
                
                return this._passMark;
            }
            
        }

        public string Name //read / write property
        {
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new Exception("Name cannot be null or empty");
                }
                this._Name = value;
            }
            get
            {
                return string.IsNullOrEmpty(this._Name) ? "No Name" : this._Name;
            }
        }
       public int Id
        {
            set
            {
                if (value < 0)
                {
                    throw new Exception("Student id can't be negative");
                }
                this._id = value;
            }
            get
            {
                return this._id;
            }
        }
    }
}
