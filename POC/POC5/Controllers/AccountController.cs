﻿using CoreApiResponse;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using POC5.Model;
using POC5.Repository;
using System.Net;

namespace POC5.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : BaseController
    {
        readonly IAccountRepository _accountRepository;
        //readonly ITokenGenerator _tokenGenerator;
        
        public AccountController(IAccountRepository accountRepository/*, ITokenGenerator tokenGenerator*/)
        {
            _accountRepository = accountRepository;
            //_tokenGenerator = tokenGenerator;
        }

        [HttpPost]
        [Route("RegisterUser")]
        public async Task<IActionResult> RegisterUser(RegisterUser registerModel)
        {
            var result = await _accountRepository.RegisterUser(registerModel);
            if (result.Succeeded)
            {
               var userData = await _accountRepository.GetUserByName(registerModel.UserName);
                return CustomResult("The Status Code was 200, User was registered successfully",data:userData.UserName,HttpStatusCode.OK);
            }
            return Unauthorized();
        }

        [HttpPost]
        [Route("Login")]
        public async Task<IActionResult> Login(LoginUser loginModel)
        {
            var userToken = await _accountRepository.LoginUser(loginModel);
            
            if (string.IsNullOrEmpty(userToken))
            {
                return  CustomResult($"Sorry you are not authorized to view this please register first", HttpStatusCode.Unauthorized);
            }
            string refreshToken=await _accountRepository.GetRefreshToken(loginModel.UserName);

            return CustomResult("You are Logged In Successful",new AuthenticatedResponses
            {
                Token = userToken,
                RefreshToken = refreshToken
            },HttpStatusCode.OK);


        }

        [HttpGet]
        [Route("GetAllUsers")]
        public async Task<IActionResult> GetAllUsers()
        {
           var users = await _accountRepository.GetAllUsers();
            return CustomResult("The Status code is 200", data: users, HttpStatusCode.OK);
        }

    }
}
