﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using POC5.Context;
using POC5.Model;
using POC5.Repository;

namespace POC5.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TokenController : ControllerBase
    {
        readonly ITokenGenerator _tokenGenerator;
        readonly ApplicationDbContext _context;
        public TokenController(ITokenGenerator tokenGenerator,ApplicationDbContext applicationDbContext)
        {
            _tokenGenerator = tokenGenerator;
            _context = applicationDbContext;
        }

        [Route("Refresh")]
        [HttpPost]
        public ActionResult Refresh(TokenApiModel tokenApiModel)
        {
            if (tokenApiModel == null)
                return BadRequest("Invalid ClientRequest");
            string accessToken = tokenApiModel.AccessToken;
            string refreshToken = tokenApiModel.RefreshToken;
            var principal = _tokenGenerator.GetPricipalFromExpiredToken(accessToken);
            var username = principal.Identity.Name;
            var user = _context.Users.SingleOrDefault(u => u.UserName == username);
            //var user = _context.Users.FirstOrDefault(u => u.UserName == username);
            if (user is null || user.RefreshToken != refreshToken || user.RefreshTokenExpiryTime <= DateTime.Now)
                return BadRequest("Invalid Client Request");
            var newAccessToken = _tokenGenerator.GenerateToken(principal.Claims);
            var newRefreshToken = _tokenGenerator.GenerateRefreshToken();
            user.RefreshToken = newRefreshToken;
            _context.SaveChanges();
            return Ok(new AuthenticatedResponses()
            {
                Token = newAccessToken,
                RefreshToken = newRefreshToken
            });

        }
    }
}
