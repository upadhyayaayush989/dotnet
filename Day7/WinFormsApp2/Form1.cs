namespace WinFormsApp2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private async void btnReadFile_Click(object sender, EventArgs e)
        {
            Task<int> task = new Task<int>(CountCharacters);
            task.Start();
            //CountCharacters();
            int countCharacters = await task;
            label1.Text = "Reading Contents from the File";
        }

        private int CountCharacters()
        {
            using StreamReader reader = new StreamReader("C:\\Users\\user\\source\\repos\\Test.txt");
            int countCharacters = 0;
            {
                
                string content = reader.ReadToEnd();
                countCharacters = content.Length;
                Thread.Sleep(5000);
            }
            return countCharacters;
        }
    }
}