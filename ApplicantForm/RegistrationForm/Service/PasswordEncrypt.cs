﻿using System.Security.Cryptography;

namespace RegistrationForm.Service
{
    public class PasswordEncrypt
    {
        public static string GetMD5Hash(string input)
        {
            input = $"Aayush$2000";
            using(MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider())
            {
                byte[] bytesArray = System.Text.Encoding.UTF8.GetBytes(input);
                bytesArray = md5.ComputeHash(bytesArray);
                System.Text.StringBuilder stringBuilder = new System.Text.StringBuilder();
                foreach(byte b in bytesArray)
                {
                    stringBuilder.Append(b.ToString("x2"));
                }
                return stringBuilder.ToString();
            }
        }
    }
}
