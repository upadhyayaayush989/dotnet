﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProductApi.Model;
using ProductApi.Repository;

namespace ProductApi.Controllers
{
    [Authorize]
    //attribute based routing 
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        readonly IProductRepository _productRepository;
        public ProductController(IProductRepository productRepository)
        {
            _productRepository=productRepository;
        }
        [Route("GetAllProducts")]
        [HttpGet]
        public ActionResult GetAllProducts()
        {
            List<Product>productList = _productRepository.GetAllProducts();
            return Ok(productList);
        }

        [Route("AddProduct")]
        [HttpPost]
        public ActionResult AddProduct(Product product)
        {
            bool addProductStatus = _productRepository.AddProduct(product);
            return Created("api/created",addProductStatus);
        }

        [Route("GetProductById/{id:int}")]
        [HttpGet]
        public ActionResult GetProductById(int id)
        {
           var getProduct = _productRepository.GetProductById(id);
            if(getProduct == null)
            {
                return BadRequest($"Product with Id {id} not Found");
            }
            else
            {
                return Ok(getProduct);
            }
        }
        #region Edit product
        [Route("EditProduct")]
        [HttpPost]
        public ActionResult EditProduct(int id,Product product)
        {
            var productEditStatus = _productRepository.EditProduct(id, product);
            return Ok(productEditStatus);
        }
        #endregion
        [Route("DeleteProductById/{id:int}")]
        [HttpDelete]
        public ActionResult DeleteProductById(int id) 
        {
            var getProduct = _productRepository.DeleteProductById(id);
            if(getProduct == null)
            {
                return NotFound($"Product with Id {id} not found");
            }
            else
            {
                return Ok(getProduct);
            }
        }


    }
}
