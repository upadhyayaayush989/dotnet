﻿using System;
using System.Collections.Generic;

namespace RegistrationForm.Models
{
    public partial class EducationalDetailTbl
    {
        public int Id { get; set; }
        public string? OtherInstitute { get; set; }
        public int UserId { get; set; }
        public int HighestQualificationId { get; set; }
        public int SpecializationId { get; set; }
        public int InstituteId { get; set; }

        public virtual HighestQualificationTbl HighestQualification { get; set; } = null!;
        public virtual InstituteTbl Institute { get; set; } = null!;
        public virtual SpecializationTbl Specialization { get; set; } = null!;
        public virtual LoginDetailTbl User { get; set; } = null!;
    }
}
