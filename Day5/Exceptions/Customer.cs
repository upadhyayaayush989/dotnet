﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exceptions
{
    internal class Customer
    {
        int max = 200;

        internal void CheckLimit(int myValue)
        {
            try
            {
                if (myValue > max)
                {
                    throw new Exception("Limit Exceeded");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);//textual error description
                Console.WriteLine(ex.StackTrace);//sequence calls that has triggered exceptions     
                Console.WriteLine(ex.TargetSite);//name of the method which threw the exceptions 
                Console.WriteLine(ex.TargetSite.DeclaringType);//projectname.classname
                Console.WriteLine(ex.Source);//project name or assembly
            }
            finally
            {
                Console.WriteLine("Always executed");
            }
        }
    }
}
