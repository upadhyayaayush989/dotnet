import { Component, OnInit } from '@angular/core';
import { Route, Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
userName=localStorage.getItem('username');
  constructor(private router:Router) { }

  ngOnInit(): void {
  }

  LogOut(){
    localStorage.clear();
    this.router.navigate(['login'])
  }

  IsUserValid(){
    if(localStorage.getItem("username") == null){
      return true;
    }
    else{
      return false;
    }
  }
  IsAdminValid(){
    if(localStorage.getItem('adminname')==null){
      return true;
    }
    else{
      return false;
    }
  }

  GotoViewCart(){
    this.router.navigate(['viewcart']);
  }

}
