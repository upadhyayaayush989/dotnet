﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POC3_Persistant.CQRS.Commands
{
    public class DeleteProductCommand:IRequest<int>
    {

        public int Id { get; set; }
        public class DeleteProductCommandHandler : IRequestHandler<DeleteProductCommand, int>
        {
            private readonly ProductDbContext _context;
            public DeleteProductCommandHandler(ProductDbContext context)
            {
                _context = context;
            }

            public async Task<int> Handle(DeleteProductCommand command, CancellationToken cancellationToken)
            {
                var product = await _context.Products.Where(p => p.Id == command.Id).FirstOrDefaultAsync();
                _context.Products.Remove(product);
                return await _context.SaveChangesAsync();
            }
        }


    }
}
