﻿using UserApi.Exceptions;
using UserApi.Model;
using UserApi.Repository;

namespace UserApi.Service
{
    public class UserService : IUserService
    {
        readonly IUserRepository _userRepository;
        public UserService(IUserRepository userRepository)
        {
            _userRepository=userRepository;
        }

        public bool BlockUnBlockUser(int id, bool blockUnBlockUser)
        {
            User userExists = _userRepository.GetUserById(id);
            if(userExists == null)
            {
                return false;
            }
            else
            {
                int blockStatus = _userRepository.BlockUnBlockUser(blockUnBlockUser, userExists);
                if(blockStatus == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public bool DeleteUser(int id)
        {
            User userExists = _userRepository.GetUserById(id);
            if(userExists != null)
            {
                int userDeleteStatus = _userRepository.DeleteUser(userExists);
                if (userDeleteStatus == 1)
                {
                    return true;
                }
                else return false;
            }
            else
            {
                //return false;
                throw new UserDoesnotExistsException($"User with Id{id} not exists");
            }
        }       

        public bool EditUser(int id, User user)
        {
            //User userExists = _userRepository.GetUserById(id);
            //if(userExists == null)
            //{
            //    return false;
            //}
            //else
            //{
            user.Id = id;
                int userEditStatus = _userRepository.EditUser(user);
                if (userEditStatus == 1)
                {
                    return true;
                }
                else return false;
                
            //}
        }

        public async Task< List<User>> GetAllUsers()
        {
           return await _userRepository.GetAllUsers();
        }

        public User LogIn(LoginUser loginUser)
        {
            User user = _userRepository.LogIn(loginUser.Name, loginUser.Password);
            if (user != null)
            {
                return user;
            }
            else
            {
                throw new UserCredentialInValidException($"{loginUser.Name} and details are invalid");
            }
        }

        public bool RegisterUser(User user)
        {
            var userExists = _userRepository.GetUserByName(user.Name);
            if (userExists == null)
            {
              int userRegisterStatus = _userRepository.RegisterUser(user);
                if (userRegisterStatus == 1)
                {
                    return true;
                }
                else return false;
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
