﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations.Schema;

namespace RegistrationForm.Models.ViewModel
{
    [Keyless]
    public class ApplicantViewModel
    {

        #region LogInDetails
        [Remote(action: "EmailAlreadyExists", controller: "Home")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Field is required")]
        [RegularExpression(".+@.+", ErrorMessage = "Field must include exactly 1 '@'")]
        [MaxLength(254, ErrorMessage = "Field can be at most 254 characters long")]
        [DataType(DataType.EmailAddress)]
        [DisplayName("Email ")]
        public string Email { get; set; }

        [Remote(action: "AlternateEmailCheck", controller: "Home")]
        [RegularExpression(".+@.+", ErrorMessage = "Field must include exactly 1 '@'")]
        [MaxLength(254, ErrorMessage = "Field can be at most 254 characters long")]
        [DataType(DataType.EmailAddress)]
        [DisplayName("Alternate Email")]
        public string? AlternateEmail { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Field is required")]
        [RegularExpression("^[A-Za-z]+$", ErrorMessage = "Field can only contain alphabets")]
        [MinLength(2,ErrorMessage ="FirstName should be ateast be 2 characters long")]
        [MaxLength(50, ErrorMessage = "Field can be at most 50 characters long")]
        [DisplayName("First Name ")]
        public string FirstName { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Field is required")]
        [RegularExpression("^[A-Za-z]+$", ErrorMessage = "Field can only contain alphabets")]
        [MaxLength(50, ErrorMessage = "Field can be at most 50 characters long")]
        [DisplayName("Last Name ")]
        public string LastName { get; set; }

        //[Remote(action: "PasswordAlreadyInUse",controller:"Home")]
        [Required, RegularExpression("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,128}$", ErrorMessage = $"Please enter the Password in Proper format\nPassword should contain aleast one Uppercase ,lowercase and atleast one spacial character !@#$%^&*() ")]
        [MinLength(8, ErrorMessage = "Field must be atleast 8 characters long")]
        [MaxLength(128, ErrorMessage = "Field can be  128 characters long")]
        [DisplayName("Password ")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Field is required")]
        [Compare("Password", ErrorMessage = "Passwords don't match")]
        [DataType(DataType.Password)]
        [DisplayName("Confirm Password ")]
        public string ConfirmPassword { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Field is required")]
        [DisplayName("Looking For ")]
        //dropdown
        public string WorkTypeId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Field is required")]
        [DisplayName("Your Status ")]
        //dropdown
        public int YourStatusId { get; set; }

        #endregion

        #region PersonalDetails

        [DisplayName("Phone Number")]

        [MinLength(11, ErrorMessage = "Field can be at most 11 digits")]
        [MaxLength(11, ErrorMessage = "Field can be at most 11 digits")]
        [RegularExpression(@"^[0-9]{11}$")]
        public string? PhoneNumber { get; set; }



        [DisplayName("Mobile")]
        [Remote(action: "MobileNumberAlreadyExists", controller: "Home")]
        [RegularExpression(@"^[6,7,8,9][0-9]{0,9}$",ErrorMessage ="Mobile Number can only start from 6,7,8,9 and it should be 10 digit long")]
        [MinLength(10, ErrorMessage = "Field can be at most 10 digits")]
        [MaxLength(10, ErrorMessage = "Field can be at most 10 digits")]
        public string? MobileNumber { get; set; }

        #region DateOfBirth
        [DisplayName("Date of Birth ")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Field is required")]
        [DataType(DataType.Date)]
        [DateOfBirth(MinAge = 18, MaxAge = 59)]
        public DateTime DateofBirth { get; set; }

        public class DateOfBirthAttribute : ValidationAttribute
        {
            public int MinAge { get; set; }
            public int MaxAge { get; set; }

            public override bool IsValid(object value)
            {
                if (value == null)
                    return true;

                var val = (DateTime)value;

                if (val.AddYears(MinAge) > DateTime.Now)
                    return false;

                return (val.AddYears(MaxAge) > DateTime.Now);
            }
        }
        #endregion

        [DisplayName("Gender ")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Field is required")]
        //dropdown
        public int GenderId { get; set; }

        [DisplayName("Marital ")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Field is required")]
        //dropdown
        public int MaritalStatusId { get; set; }

        [DisplayName("Country ")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Field is required")]
        //dropdown
        public int CountryId { get; set; }

        [DisplayName("State ")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Field is required")]
        //dropdown
        public int StateId { get; set; }

        [DisplayName("District")]
        [MaxLength(50, ErrorMessage = "Field can be at most 50 characters long")]
        public string? District { get; set; }

        [DisplayName("City ")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Field is required")]
        //dropdown
        public int CityId { get; set; }

        [DisplayName("Locality ")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Field is required")]
        //dropdown
        public int LocalityId { get; set; }

        [DisplayName("ZipCode")]
        [RegularExpression(@"^[1-9][0-9]{5}$",ErrorMessage ="ZipCode can't start with 0,should not contain all zeros and must be 6 digits")]
        public int? ZipCode { get; set; }

        #endregion

        #region HighestQualificationDetails

        [Required(AllowEmptyStrings = false, ErrorMessage = "Field is required")]
        [DisplayName("Highest Qualification ")]
        //dropdown
        public int HighestQualificationId { get; set; }

        [DisplayName("Specialization ")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Field is required")]
        //dropdown
        public int SpecializationId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Field is required")]
        [DisplayName("Institute ")]
        //dropdown
        public int InstituteId { get; set; }

        [DisplayName("Please Specify(If any other) ")]
       
        [RegularExpression("^[a-zA-z]*(\\d*\\.\\d*)[a-zA-z]*$", ErrorMessage = "Field can only contain alphabets")]
        [MaxLength(30, ErrorMessage = "Field can be at most 30 characters long")]
        public string? OtherInstitute { get; set; }

        #endregion

        #region ProfessionalDetails

        [DisplayName("Preferred Location")]
        public int? PreferedLocationId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Field is required")]
        [DisplayName("Are you ready to Relocate ? ")]
        public bool RelocationStatus { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Field is required")]
        [Range(0, 30, ErrorMessage = "Experience must be between 0 and 30")]
        [DisplayName("Experience in Years")]
        //dropdown
        public int TotalExperienceInYears { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Field is required")]
        [Range(0, 11, ErrorMessage = "Experience must be between 0 and 11")]
        [DisplayName("Experience in Months")]
        //dropdown
        public int TotalExperienceInMonths { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Field is required")]
        [DisplayName("Job Category ")]
        public int JobCategoryId { get; set; }
        public int? EmployeeId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Field is required")]
        [DisplayName("Key Skills ")]
        
        public int[] KeySkillId { get; set; }

        [DisplayName("Current Industry")]
        //dropdown
        public int? CurrentIndustryId { get; set; }

        [DisplayName("Current Employer")]
        public int? CurrentEmployerId { get; set; }

        [DisplayName("Current Designation")]
        public string? CurrentDesignation { get; set; }

        [DisplayName("Previous Employer ")]
        public int? PreviousEmployerId { get; set; }

        [DisplayName("Previous Designation ")]
        public string? PreviousDesignation { get; set; }

        [DisplayName("Notice Period")]
        //dropdown
        public int? NoticePeriodId { get; set; }

        [DisplayName("CTC in Lakhs ")]
        public int CtcinLakhPerAnumId { get; set; }

        [DisplayName("CTC in Thousands ")]
        public int CtcinThousandPerAnumId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Field is required")]
        [DisplayName("Resume Title ")]
        public string? ResumeTitle { get; set; }

        [DisplayName("Attach Your Resume")]
        [DataType(DataType.Upload)]
        [MaxFileSize(10 * 1024 * 1024, ErrorMessage = "File Size is greater than 10 Mb. Please Upload a file which is less than 10 Mb.")]
        [AllowedExtensions(new string[] { ".pdf", ".docx" }, ErrorMessage = "File Extension Not Allowed. Please Upload a file with .pdf or .docx extension")]
        public IFormFile? ResumePath { get; set; }

        [DisplayName("Type In Your Resume")]
       
        public string? CopyResume { get; set; }
        #endregion
    }
}
