import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() {
    console.log("AuthService - constructor()");
   }

   GetToken()
  {
    return localStorage.getItem("userToken");
  }
  GetCurrentlyLoggedInUser()
  {
    return localStorage.getItem("username");
  }
}
