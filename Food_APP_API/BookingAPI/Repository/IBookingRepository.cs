﻿using BookingAPI.Models;

namespace BookingAPI.Repository
{
    public interface IBookingRepository
    {
        Task <bool> AddItems(FoodCart addItems);
        FoodCart CheckIfItemPresentForParticularUser(string productname,string username);
       
        Task<List<FoodCart>> GetCart(string userName);
        Task<bool> GetItemByCartId(int cartId);
        //List<Menu> GetMenu();
        Task<bool> PayBill(string userName);
        Task<bool> RemoveItem(int cartId);
        Task<bool> UpdateQuantity(FoodCart foodItem);
        Task<int> GetTotalAmount(string userName);
    }
}
