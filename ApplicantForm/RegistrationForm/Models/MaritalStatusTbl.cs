﻿using System;
using System.Collections.Generic;

namespace RegistrationForm.Models
{
    public partial class MaritalStatusTbl
    {
        public MaritalStatusTbl()
        {
            PersonalDetailTbls = new HashSet<PersonalDetailTbl>();
        }

        public int MaritalStatusId { get; set; }
        public string MaritalStatus { get; set; } = null!;

        public virtual ICollection<PersonalDetailTbl> PersonalDetailTbls { get; set; }
    }
}
