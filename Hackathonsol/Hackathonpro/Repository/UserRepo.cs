﻿using Database_MVC.Context;
using Hackathonpro.Models;

namespace Hackathonpro.Repository
{
    public class UserRepo : IUserRepo
    {
        static int GrandTotal;
        
        FoodDbContext _foodDbContext;

        public UserRepo(FoodDbContext foodDbContext)
        {
            _foodDbContext= foodDbContext;
        }
       

        public void AddUser(User user)
        {
            _foodDbContext.users.Add(user);
            _foodDbContext.SaveChanges();
        }

        public void AddValuesToCart(int id)
        {
            if (_foodDbContext.addToCarts.Count() == 0)
            {
                GrandTotal = 0;
            }

            Food newItem = _foodDbContext.foods.Where(w => w.Id == id).FirstOrDefault();
            string ItemName = newItem.FoodName;
            string img = newItem.ImgSource;
            int price = newItem.Price;
            int Quan = 1;
            int total = price * Quan;
            GrandTotal += total;
            AddToCart newFood = new AddToCart() { FoodOrdered = ItemName, Quantity = Quan, Total = total, ImageSource = img };
            AddToCart itemExists = _foodDbContext.addToCarts.Where(u => u.FoodOrdered == ItemName).FirstOrDefault();
            if (itemExists != null)
            {
                itemExists.Quantity += 1;
                itemExists.Total += price;
                
                _foodDbContext.addToCarts.Update(itemExists);
                _foodDbContext.SaveChanges();
            }
            else
            { 
            _foodDbContext.addToCarts.Add(newFood);
            _foodDbContext.SaveChanges();
        }
        }

        public void DeleteElement(int id)
        {
            AddToCart deletedElement = _foodDbContext.addToCarts.Where(u => u.Id == id).FirstOrDefault();
            int price = (int)deletedElement.Total / deletedElement.Quantity;
            if(GrandTotal > 60)
            {
                GrandTotal -= price;
            }

            if (deletedElement.Quantity > 1)
            {
                deletedElement.Quantity -= 1;
                deletedElement.Total -= price;
                _foodDbContext.addToCarts.Update(deletedElement);
                _foodDbContext.SaveChanges();   
            }
            else
            {

                _foodDbContext.addToCarts.Remove(deletedElement);
                _foodDbContext.SaveChanges();
            }
            
        }

        public int GenerateBill()
        {

            return GrandTotal;

        }

        public List<Food> GetAllProducts()
        {
            return _foodDbContext.foods.ToList();
        }
        public List<AddToCart> GetCart()
        {
            return _foodDbContext.addToCarts.ToList();
        }

        public bool GetUserByName(string Name)
        {
          
            User checker = _foodDbContext.users.Where(u => u.CustName == Name).FirstOrDefault();
            if(checker != null);
            {
                return true;
            }
            return false;
        }

        public bool GetUserByUserName(string username, string password)
        {
            return _foodDbContext.users.Any(u => u.CustName == username && u.Pasword == password);
        }

        public bool GetUserByUserPassword(string password)
        {
            return _foodDbContext.users.Any(u => u.Pasword == password);
        }

        public void RemoveElements()
        {
            foreach(AddToCart items in _foodDbContext.addToCarts)
            {
                _foodDbContext.addToCarts.Remove(items);
                
            }
            GrandTotal = 0;         
            _foodDbContext.SaveChanges();

        }
    }
}
