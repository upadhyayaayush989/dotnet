import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Food } from 'src/app/models/food';

@Injectable({
  providedIn: 'root'
})
export class AddfoodService {
  

  constructor(private httpClient:HttpClient) { 
    
  }
  AddFood(food:Food):Observable<boolean>{
    return this.httpClient.post<boolean>('https://localhost:7039/api/Food/api/AddFood',food);
  }


}
