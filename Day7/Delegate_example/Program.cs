﻿namespace Delegate_example
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("Delegate Demo");
            CoffeeMaker cofeeMaker = new CoffeeMaker();
            cofeeMaker.MakeCofee(PrepareCofee);
        }


        private static void PrepareCofee()
        {
            string[] instructions = new string[]
            {
                "Add HotWater","Add Cofee Powder","Add Milk"
            };
            foreach(var instruction in instructions)
            {
                Console.WriteLine(instruction);
            }
        }
    }
}