﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Collections.Exceptions;
using Collections.Model;

namespace Collections.Repository
{
    
    internal class ProductRepository
    {
        //declare a list
        List<Product> products;

        //constructor ctor+tab tab
        public ProductRepository()
        {
            //initializing list values inside the constructor
            //sot the values are initialized in the list 
            products = new List<Product>()
            {
                new Product(){Id=1,Name="LG TV",Price=50000},
                new Product(){Id=2,Name="Asus Mobile",Price=30000},

            };
        }
        //get all Products
        public List<Product> GetAllProducts()
        {
            return products;
        }

        //Add Product
        public string AddProduct(Product product)
        {
            //checking wether product is already there in the list or Not 
            var productExists =  GetProductByName(product.Name);
            if(productExists== null)
            {
                products.Add(product);
                return $"Product added Successfully";
            }
            else
            {
                throw new ProductAlreadyExistsException($"{product.Name} already exists");
            }

        }

        private Product GetProductByName(string name)
        {
            return products.Find(p => p.Name == name);
        }

        //delete Product

        public bool DeleteProductByName(string name) 
        {
            var product = GetProductByName(name);
            return product != null ? products.Remove(product) : false;
        
        }
        
        //create a function to update the product
        


    }
}
