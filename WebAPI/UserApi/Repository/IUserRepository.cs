﻿using UserApi.Model;

namespace UserApi.Repository
{
    public interface IUserRepository
    {
        int BlockUnBlockUser(bool blockUnBlockUser, User userExists);
        int DeleteUser(User userExists);
        int EditUser(User user);
        Task<List<User>> GetAllUsers();
        User GetUserById(int id);
        User GetUserByName(string name);
        User LogIn(string name, string password);
        int RegisterUser(User user);
    }
}
