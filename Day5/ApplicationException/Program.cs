﻿using ApplicationException.Exceptions;
using ApplicationException.Repository;


MobileNumberValidation mobileNumberValidation = new MobileNumberValidation();
Console.WriteLine("Enter Your Mobile number");

try
{
    string mobileNumber = Console.ReadLine();
    mobileNumberValidation.ValidateMobileNumber(mobileNumber);
}
catch (InvalidMobileNumber ex)
{
    Console.WriteLine(ex.Message);
}



