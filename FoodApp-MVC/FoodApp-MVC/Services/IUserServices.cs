﻿using FoodApp_MVC.Models;

namespace FoodApp_MVC.Services
{
    public interface IUserServices
    {
        bool RegisterUser(User user);
        User LogIn(User user);
        public List<FoodMenu> GetAllFood();
        public void AddValues(int id);

    }
}
