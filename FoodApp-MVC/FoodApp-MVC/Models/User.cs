﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;

namespace FoodApp_MVC.Models
{
    
    public class User
    {
        //[Key]
        //int UserId { get; set; }
        public int UserId{ get; set; }

        [Required(ErrorMessage = "Enter UserName")]
        [Display(Name = "UserName")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Enter the Password")]
        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        
    }
}
