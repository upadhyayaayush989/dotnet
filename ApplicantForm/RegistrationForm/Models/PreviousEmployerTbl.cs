﻿using System;
using System.Collections.Generic;

namespace RegistrationForm.Models
{
    public partial class PreviousEmployerTbl
    {
        public PreviousEmployerTbl()
        {
            ProfessionalDetailsTbls = new HashSet<ProfessionalDetailsTbl>();
        }

        public int PreviousEmployerId { get; set; }
        public string? PreviousEmployerName { get; set; }
        public int? CurrentEmployerId { get; set; }

        public virtual CurrentEmployerTbl? CurrentEmployer { get; set; }
        public virtual ICollection<ProfessionalDetailsTbl> ProfessionalDetailsTbls { get; set; }
    }
}
