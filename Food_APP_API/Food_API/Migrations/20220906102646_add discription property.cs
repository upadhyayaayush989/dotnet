﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Food_API.Migrations
{
    public partial class adddiscriptionproperty : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "FoodDiscription",
                table: "FoodItems",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FoodDiscription",
                table: "FoodItems");
        }
    }
}
