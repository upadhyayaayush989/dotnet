﻿using System.Data;
using System.Data.SqlClient;
Connection();
static void Connection()
{
    string cs = "Data source=DESKTOP-IKB2EQL;Database=ProductDb;integrated Security=true";
    SqlConnection con = new SqlConnection(cs);
    #region Using adapter
    SqlDataAdapter adapter = new SqlDataAdapter("select * from ProductTbl", con);
    //using Dataset
    DataSet dataset = new DataSet();
    adapter.Fill(dataset);
    foreach(DataRow row in dataset.Tables[0].Rows)
    {
        Console.WriteLine($"ID::{row["Id"]}\tName::{row["ProductName"]}\tPrice::{row["Price"]}\tCatId::{row["CatId"]}\tRating::{row["Rating"]}");
    }
    #endregion

    #region StoredProcedure
    SqlDataAdapter adapter1 = new SqlDataAdapter("SpGetAllProducts", con);
    #endregion

    #region DataTable
    DataTable dataTable = new DataTable();
    adapter.Fill(dataTable);
    foreach (DataRow row in dataTable.Rows)
    {
        Console.WriteLine($"ID::{row["Id"]}\tName::{row["ProductName"]}\tPrice::{row["Price"]}\tCatId::{row["CatId"]}\tRating::{row["Rating"]}");
    }

    #endregion

}