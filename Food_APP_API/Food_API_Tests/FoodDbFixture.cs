﻿using Food_API.Context;
using Food_API.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Food_API_Tests
{
    
    public class FoodDbFixture
    {
        internal FoodDbContext _foodDbContext;
        public FoodDbFixture()
        {
            var foodDbContextOptions = new DbContextOptionsBuilder<FoodDbContext>().UseInMemoryDatabase("FoodDb").Options;
            _foodDbContext = new FoodDbContext(foodDbContextOptions);
            _foodDbContext.Add(new FoodItems() { FoodId = 1, FoodName = "Rice", FoodType = "Veg",Price=300, ImagePath = "string", FoodDiscription = "Rice" });
            _foodDbContext.Add(new FoodItems() { FoodId = 2, FoodName = "Fried Rice", FoodType="Veg", Price = 400, ImagePath = "string",  FoodDiscription = "Fried" });
            _foodDbContext.Add(new FoodItems() { FoodId = 3, FoodName = "Manchurian", FoodType = "Veg", Price = 300, ImagePath = "string", FoodDiscription = "Rice" });
            _foodDbContext.SaveChanges();

        }
    }
}
