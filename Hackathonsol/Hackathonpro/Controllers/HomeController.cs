﻿using Database_MVC.Context;
using Hackathonpro.Models;
using Hackathonpro.Services;
using Microsoft.AspNetCore.Mvc;

namespace Hackathonpro.Controllers
{
    public class HomeController : Controller
    {
        IUserServices _userServices;
        public HomeController(IUserServices userServices)
        {
            _userServices = userServices;
        }

        public IActionResult DashBoard()
        {
            List<Food> sample = _userServices.GetProducts();
            return View(sample);
        }
        public IActionResult AddToCart(int id)
        {
            _userServices.AddValues(id);


            return RedirectToAction("Dashboard");

        }
        public IActionResult ViewCart()
        {
            List<AddToCart> viewCart =  _userServices.GetCart();
           
            return View(viewCart);
        }
        public IActionResult Delete(int id)
        {
            _userServices.DeleteItemFromCart(id);
           return RedirectToAction("ViewCart");
        }

        public IActionResult GenerateBill()
        {
            int gen = _userServices.GenerateBill();
            List <AddToCart> viewCart = _userServices.GetCart();
            ViewBag.bill = gen;
            return View(viewCart);
        }
        public IActionResult RemoveElements()
        {
            _userServices.RemoveItems();
            return RedirectToAction("DashBoard");
        }
    }
}
