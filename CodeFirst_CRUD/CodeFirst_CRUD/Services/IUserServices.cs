﻿using CodeFirst_CRUD.Models;

namespace CodeFirst_CRUD.Services
{
    public interface IUserServices
    {
        List<Food> GetProducts();
        void AddValues(int id);

        List<FoodCart> GetCart();
        void DeleteItemFromCart(int id);

        int GenerateBill();
        void RemoveItems();
    }
}
