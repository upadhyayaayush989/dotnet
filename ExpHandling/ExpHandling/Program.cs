﻿using System;
using System.IO;

string path = @"C:\Users\user\source\repos\ExpHandling\Data.txt";
StreamReader streamReader = null;
try
{
    streamReader = new StreamReader(path);
    Console.WriteLine(streamReader.ReadToEnd());
    
}
catch(FileNotFoundException ex )
{
    //you can also log the details in Database for later debugging process
    Console.WriteLine($"Please check if the File exists {ex.FileName}");
    
}
catch(Exception ex)
{
    Console.WriteLine(ex.Message);
}
finally
{
    if(streamReader != null) { streamReader.Close(); }
    Console.WriteLine("Finally Block Executed");
}