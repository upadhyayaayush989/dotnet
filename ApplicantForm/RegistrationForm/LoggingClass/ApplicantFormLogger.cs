﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using RegistrationForm.Controllers;

namespace RegistrationForm.LoggingClass
{
    public class ApplicantFormLogger:ActionFilterAttribute,IExceptionFilter
    {
        readonly string logFileName;
        DateTime startTime;
        readonly ILogger<HomeController> _logger;
        public ApplicantFormLogger(IWebHostEnvironment environment, ILogger<HomeController> logger)
        {
            _logger = logger;
            logFileName = environment.ContentRootPath + @"/LogFile/logEntry.txt";
        }

        public void OnException(ExceptionContext context)
        {
            if (!context.ExceptionHandled)
            {
                var exceptionMessage = context.Exception.Message;
                //var stackTrace = context.Exception.StackTrace;
                var controllerName = context.RouteData.Values["controller"].ToString();
                var actionName = context.RouteData.Values["action"].ToString();

                string Message = "Date :" + DateTime.Now.ToString() + ", Controller: " + controllerName + ", Action:" + actionName +
                                 "Error Message : " + exceptionMessage + "\n---------------------------------------------------\n";
                //+ Environment.NewLine + "Stack Trace : " + stackTrace;

                using (StreamWriter sw = File.AppendText(logFileName))
                {
                    sw.Write(Message);
                    sw.Close();
                }
                _logger.LogError(Message);
                context.ExceptionHandled = true;
                context.Result = new ViewResult()
                {
                    ViewName = "ErrorDisplayView"
                };

            }
        }






    }
}
