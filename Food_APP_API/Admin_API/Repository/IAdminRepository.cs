﻿using Admin_API.Model;

namespace Admin_API.Repository
{
    public interface IAdminRepository
    {
        Admin? LogIn(string username, string password);
    }
}
