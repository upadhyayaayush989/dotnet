﻿using Microsoft.EntityFrameworkCore;
using POC3_Persistant.Model;
using System.Collections.Generic;

namespace POC3_Persistant
{
    public class ProductDbContext:DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder dbContextOptionsBuilder)
        {
            dbContextOptionsBuilder.UseSqlServer(@"Data Source=DESKTOP-IKB2EQL;Database=POC3_ProductDb;Integrated Security=True");
        }

        public DbSet<Product> Products { get; set; }
    }
}