﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POC3_Persistant.CQRS.Commands
{
    public class UpdateProductCommand:IRequest<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Price { get; set; }

        public class UpdateProductCommandHandler : IRequestHandler<UpdateProductCommand, int>
        {
            private readonly ProductDbContext _context;
            public UpdateProductCommandHandler(ProductDbContext context)
            {
                _context = context;
            }

            public async Task<int> Handle(UpdateProductCommand command, CancellationToken cancellationToken)
            {
                var product = _context.Products.Where(p=>p.Id == command.Id).FirstOrDefault();
                if(product == null)
                {
                    return default;
                }
                else
                {
                    product.Name = command.Name;
                    product.Price = command.Price;
                    await _context.SaveChangesAsync();
                    return product.Id;
                }
            }
        }
    }
}
