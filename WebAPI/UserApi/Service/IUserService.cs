﻿using UserApi.Model;

namespace UserApi.Service
{
    public interface IUserService
    {
        bool BlockUnBlockUser(int id, bool blockUnBlockUser);
        bool DeleteUser(int id);
        bool EditUser(int id, User user);
        Task<List<User>> GetAllUsers();
        User LogIn(LoginUser loginUser);
        bool RegisterUser(User user);
    }
}
