﻿using System.ComponentModel.DataAnnotations;

namespace BookingAPI.Models
{
    public class FoodCart
    {
        [Key]
        public int CartId { get; set; }
        public string FoodName { get; set; }
        public string? FoodDiscription { get; set; }
        public string FoodType { get; set; }
        public int Price { get; set; }

        public string? ImagePath { get; set; }
        public string? Name { get; set; }
        public int? Quantity { get; set; }
        public int? Total { get; set; }
    }
}
