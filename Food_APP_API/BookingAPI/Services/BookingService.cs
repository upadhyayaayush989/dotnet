﻿using BookingAPI.Exceptions;
using BookingAPI.Models;
using BookingAPI.Repository;
using Microsoft.Extensions.Configuration;
using System.Net.Mail;
using MailKit.Security;
using MimeKit.Text;
using MimeKit;
using MailKit.Net.Smtp;
using SmtpClient = MailKit.Net.Smtp.SmtpClient;
using System.Net.Mime;

namespace BookingAPI.Services
{
    public class BookingService : IBookingService
    {
        readonly IBookingRepository _bookingRepository;
        readonly IConfiguration _configuration;
        public BookingService(IBookingRepository menuRepo, IConfiguration configuration)
        {
            _bookingRepository = menuRepo;
            _configuration = configuration;
        }

        public async Task<bool> AddItemsToCart(FoodCart addItems)
        {
            FoodCart existingProduct = _bookingRepository.CheckIfItemPresentForParticularUser(addItems.FoodName, addItems.Name);
            if (existingProduct == null)
            {
                return await _bookingRepository.AddItems(addItems);          
            }
           else
            {
                return await _bookingRepository.UpdateQuantity(addItems);
            }
           
               
        }

        public async Task <int> GenerateBill(string userName)
        {
            return await _bookingRepository.GetTotalAmount(userName);
        }

        public async Task <bool> PayBill(string userName)
        {
            return await _bookingRepository.PayBill(userName);
        }

        public async Task <bool> RemoveItem(int cartId)
        {
            bool checker = await _bookingRepository.GetItemByCartId(cartId);
            if (checker == true)
            {
                return await _bookingRepository.RemoveItem(cartId);  
            }
            throw new ProductNotInCartException("Product does not exist in cart");
        }

        public async Task< List<FoodCart>> ViewCartItems(string userName)
        {
            return await _bookingRepository.GetCart(userName);
        }

        public string InVoiceNo()
        {
            string year = DateTime.Now.Year.ToString();
            string month = DateTime.Now.Month.ToString();
            string day = DateTime.Now.Day.ToString();
            string hour = DateTime.Now.Hour.ToString();
            string minute = DateTime.Now.Minute.ToString();
            string second = DateTime.Now.Second.ToString();
            string inVoiceNo = "#" + year + month + day + "-" + hour + minute + second;
            return inVoiceNo;
        }

        public void SendEmail(string userEmail, EmailDetails request)
        {

            var email = new MimeMessage();
            email.From.Add(MailboxAddress.Parse(_configuration.GetSection("EmailUserName").Value));
            email.To.Add(MailboxAddress.Parse(userEmail));
            email.Subject = "Invoice Details";
            
            //var builder = new BodyBuilder();
            //builder.TextBody = request.Body;
            //builder.Attachments.Add("Invoice.pdf");
            //email.Body = builder.ToMessageBody();
            email.Body = new TextPart(TextFormat.Html) { Text = request.Body };
            var smtp = new SmtpClient();
            smtp.Connect(_configuration.GetSection("EmailHost").Value, 587, SecureSocketOptions.StartTls);//host and port
            smtp.Authenticate(_configuration.GetSection("EmailUserName").Value, _configuration.GetSection("EmailPassword").Value);
            smtp.Send(email);
            smtp.Disconnect(true);
        }

    }
}
