﻿namespace CodeFirst_CRUD.Models
{
    public class FoodCart
    {
        public int Id { get; set; }
        public string FoodName { get; set; }

        public int Quantity { get; set; }
        public int Total { get; set; }
        public string ImgPath { get; set; }
    }
}
