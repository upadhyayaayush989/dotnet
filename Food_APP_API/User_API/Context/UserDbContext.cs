﻿using Microsoft.EntityFrameworkCore;
using User_API.Model;

namespace User_API.Context
{
    public class UserDbContext:DbContext
    {
        public UserDbContext(DbContextOptions<UserDbContext> options):base(options)
        {

        }
        public DbSet<User> Users { get; set; }
    }
}
