import { Component, OnInit } from '@angular/core';
import { Route, Router } from '@angular/router';
import { Food } from 'src/app/models/food';
import { DeletefoodService } from 'src/app/services/admin/deletefood.service';
import { EditfoodService } from 'src/app/services/admin/editfood.service';
import { FoodserviceService } from 'src/app/services/foodservice.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-admindashboard',
  templateUrl: './admindashboard.component.html',
  styleUrls: ['./admindashboard.component.css']
})
export class AdmindashboardComponent implements OnInit {

  foods?:Food[];
  // foodItem?:Food;

  constructor(private foodService:FoodserviceService,private router:Router,private deleteFoodService:DeletefoodService) {
    console.log("admin dashboard constructor");
    
   }

  ngOnInit(): void {
    this.GetFoodList();
    
    
  }

 GetFoodList(){
  console.log("admin ngOnIt is working in admin dashboard");
    this.foodService.getAllFood().subscribe(res=>{
      console.log(res);
      this.foods=res;
    })
 }



  GotoAddFood(){
    this.router.navigate(['addfood'])
  }
  GotoEditFood(id:any){
    this.router.navigate(['editFood',id])
  }

  DeleteFood(food:Food){
    let id =food.foodId;
    Swal.fire({
      title: 'Are you sure?',
      text: "",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result)=>{
      if(result.isConfirmed){
        this.deleteFoodService.DeleteFood(id).subscribe(res => {
        console.log(res);
        this.GetFoodList();
      });
      Swal.fire(
        'Deleted!',
        'Food removed from cart.',
        'success'
      );
    } 
      
    })
    // this.router.navigate(['admindashboard']);
  }

  ReloadCurrentPage(){
    window.location.reload();
  }

  
}


