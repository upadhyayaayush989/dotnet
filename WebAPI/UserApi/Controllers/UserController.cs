﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using UserApi.Model;
using UserApi.Service;

namespace UserApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        readonly IUserService _userService;
        readonly ITokenGenerator _tokenGenerator;
        public UserController(IUserService userService, ITokenGenerator tokenGenerator)
        {
            _userService=userService;
            _tokenGenerator = tokenGenerator;
        }

        [Route("api/GetAllUsers")]
        [HttpGet]
        public async Task<ActionResult> GetAllUsers()
        {
            List<User> users = await _userService.GetAllUsers();
            return Ok(users);
        }
        [Route("RegisterUser")]
        [HttpPost]
        public ActionResult RegisterUser(User user)
        {
            bool regsiterUserStatus = _userService.RegisterUser(user);
            return Ok(regsiterUserStatus);
        }
        [Route("DeleteUser")]
        [HttpDelete]
        public ActionResult DeleteUser(int id)
        {
            bool userDeleteStatus = _userService.DeleteUser(id);
            return Ok(userDeleteStatus);
        }
        [Route("EditUser/{id:int}")]
        [HttpPut]
        public ActionResult EditUser(int id,User user)
        {
            bool userEditStatus = _userService.EditUser(id, user);
            return Ok(userEditStatus);
        }

        [Route("BlockUnBlockUser")]
        [HttpPut]
        public ActionResult BlockUnBlockUser(int id,bool blockUnBlockUser)
        {
            bool blockUnblockStatus = _userService.BlockUnBlockUser(id, blockUnBlockUser);
            return Ok(blockUnblockStatus);
        }

        [Route("LogIn")]
        [HttpPost]
        public ActionResult LogIn(LoginUser loginUser)
        {
            User user = _userService.LogIn(loginUser);
            string userToken = _tokenGenerator.GenerateToken(user.Id,user.Name);
            
            return Ok(userToken);
        }
    }
}
