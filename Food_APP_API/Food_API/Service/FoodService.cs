﻿using Food_API.Exceptions;
using Food_API.Model;
using Food_API.Repository;

namespace Food_API.Service
{
    public class FoodService:IFoodService
    {
        readonly IFoodRepository _foodRepository;
        public FoodService(IFoodRepository foodRepository)
        {
            _foodRepository=foodRepository;
        }

        public bool AddFood(FoodItems food)
        {
            FoodItems foodExists = _foodRepository.GetFoodById(food.FoodId);
            if (foodExists == null)
            {
                int addFoodStatus = _foodRepository.AddFood(food);
                if (addFoodStatus == 1)
                {
                    return true;
                }
                else return false;
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool DeleteFood(int id)
        {
            FoodItems foodExists = _foodRepository.GetFoodById(id);
            if (foodExists != null)
            {
                int foodDeleteStatus = _foodRepository.DeleteFood(foodExists);
                if (foodDeleteStatus == 1)
                {
                    return true;
                }
                else return false;
            }
            else
            {
                //return false;
                throw new FoodNotAvailableException($"User with Id{id} not exists");
            }
        }

        public bool EditFood(int id,FoodItems food)
        {
            FoodItems foodToUpdate = _foodRepository.GetFoodById(id);
            foodToUpdate.FoodId = id;
            
            int foodEditStatus = _foodRepository.EditFood(food,foodToUpdate);
            if (foodEditStatus == 1)
            {
                return true;
            }
            else return false;
        }
        public async Task<List<FoodItems>> GetAllFood()
        {
            return await _foodRepository.GetAllFood();
        }
    }
}
