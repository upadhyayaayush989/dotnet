﻿
namespace Demo_delegate
{
    class Program
    {
        //declare Delegate
        public delegate string DelegateToDetails(string details);
        public static void Main()
        {
            //Instatiate A Delegate
            DelegateToDetails ds = new DelegateToDetails(StudentDetails);
            //Invoke Delegate
            Console.WriteLine(ds("Student Name is:  Student1"));
        }
        public static string StudentDetails(string name)
        {
            return name;
        }
    }

}