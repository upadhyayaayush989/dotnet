﻿namespace UserApi.Exceptions
{
    public class UserCredentialInValidException:ApplicationException
    {
        public UserCredentialInValidException()
        {

        }
        public UserCredentialInValidException(string msg):base(msg)
        {

        }
    }
}
