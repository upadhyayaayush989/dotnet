﻿using Cache.Context;
using Cache.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using System.Diagnostics;

namespace Cache.Controllers
{
    public class HomeController : Controller
    {
        private readonly IMemoryCache _memoryCache;
        private readonly UserDbContext _context;
        public HomeController(IMemoryCache memoryCache, UserDbContext context)
        {
            _memoryCache = memoryCache;
            _context = context;
        }

        [HttpGet]
        public IActionResult Index()
        {
            List<User> users;
            //checking if there is already an entry present
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            if(_memoryCache.TryGetValue("Users", out users))
            {
                //calling the server
                _memoryCache.Set("Users", _context.UserList.ToList());
            }

            users = _memoryCache.Get("Users") as List<User>;
            stopWatch.Stop();
            ViewBag.TotalRows = users.Count;
            ViewBag.TotalTime = stopWatch.Elapsed;
            return View(users);

        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}