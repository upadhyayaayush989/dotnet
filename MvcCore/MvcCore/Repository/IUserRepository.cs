﻿using MvcCore.Models;

namespace MvcCore.Repository
{
    public interface IUserRepository
    {
        List<User> GetAllUsers();
        bool AddUsers(User user);
        bool DeleteUser(int id);
    }
}
