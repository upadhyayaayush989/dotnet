using Database_MVC.Context;
using Hackathonpro.Models;
using Hackathonpro.Repository;
using Hackathonpro.Services;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();
builder.Services.AddSession();
var localConnection = builder.Configuration.GetConnectionString("LocalDbConnection");
builder.Services.AddDbContext<FoodDbContext>(u => u.UseSqlServer(localConnection));
builder.Services.AddScoped<IUserServices, UserSevices>();
builder.Services.AddScoped<IUserRepo, UserRepo>();
var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();
app.UseSession();
app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Login}/{action=LoginOrSignUp}/{id?}");

app.Run();
