﻿namespace User_API.Exceptions
{
    public class UserCredentialInValidException:ApplicationException
    {
        public UserCredentialInValidException()
        {

        }
        public UserCredentialInValidException(string msg) : base(msg)
        {

        }
    }
}
