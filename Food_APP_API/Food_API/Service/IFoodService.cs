﻿    using Food_API.Model;

namespace Food_API.Service
{
    public interface IFoodService
    {
        public bool AddFood(FoodItems food);
        public bool DeleteFood(int id);
        public bool EditFood(int id, FoodItems food);
        Task<List<FoodItems>> GetAllFood();
    }
}
