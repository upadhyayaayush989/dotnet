﻿using ApplicantForm.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Reflection;

namespace ApplicantForm.Controllers
{
    public class ApplicantFormController : Controller
    {
        readonly ApplicantDatabaseContext _applicantDatabaseContext;
        public ApplicantFormController(ApplicantDatabaseContext applicantDatabaseContext)
        {
            _applicantDatabaseContext=applicantDatabaseContext;
        }
        public IActionResult Index()
        {
          
            return View();
        }

        public IActionResult ApplicantForm()
        {
            ViewBag.CountryList = new SelectList(GetAllCountries(), "CountryId", "CountryName");
            

            return View();
        }

        public IActionResult GetAllStates(int CountryId)
        {
            List<StateTbl> StateList = _applicantDatabaseContext.StateTbls.Where(x => x.CountryId == CountryId).ToList();
            ViewBag.StateList = new SelectList(StateList, "StateId", "StateName");
            return PartialView("DisplayState");
        }
        public IActionResult GetAllCities(int StateId)
        {
            List<CityTbl> CityList = _applicantDatabaseContext.CityTbls.Where(x => x.StateId == StateId).ToList();
            ViewBag.CityList = new SelectList(CityList, "CityId", "CityName");
            return PartialView("DisplayCity");
        }


        public List<CountryTbl> GetAllCountries()
        {

            List<CountryTbl> countries = _applicantDatabaseContext.CountryTbls.ToList();
            return countries;
        }

       

   
        


    }
}
