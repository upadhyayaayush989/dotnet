﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using POC5.Context;
using POC5.Model;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace POC5.Repository
{
    public class AccountRepository : IAccountRepository
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IConfiguration _configuration;
        private readonly ITokenGenerator _tokenGenerator;
        private readonly ApplicationDbContext _context;
        public AccountRepository(UserManager<ApplicationUser> userManager,SignInManager<ApplicationUser> signInManager, IConfiguration configuration, ITokenGenerator tokenGenerator,ApplicationDbContext context)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _configuration = configuration;
            _tokenGenerator = tokenGenerator;
            _context = context;
        }


        public async Task<IdentityResult> RegisterUser(RegisterUser registerUser)
        {
            var newUser = new ApplicationUser()
            {
                UserName = registerUser.UserName,
                Email = registerUser.Email,
                

            };
            return await _userManager.CreateAsync(newUser, registerUser.Password);
        }


        public async Task<string> GetRefreshToken(string userName)
        {
            ApplicationUser user =  await _context.Users.Where(u => u.UserName == userName).FirstOrDefaultAsync();
            var refeshToken = _tokenGenerator.GenerateRefreshToken();
            user.RefreshToken = refeshToken;
            user.RefreshTokenExpiryTime = DateTime.Now.AddDays(3);
            _context.SaveChangesAsync();
            return  refeshToken;
        }

        public async Task <ApplicationUser> GetUserByName(string username)
        {
            return await _userManager.FindByNameAsync(username);
        }

        public async Task<List<ApplicationUser>> GetAllUsers()
        {
            return await _context.Users.ToListAsync();
        }

        public async Task<string> LoginUser(LoginUser loginUser)
        {
            var result = await _signInManager.PasswordSignInAsync(loginUser.UserName, loginUser.Password, false, false);
            if (!result.Succeeded)
            {
                return null;
            }
            else
            {
                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, loginUser.UserName),
                    new Claim(JwtRegisteredClaimNames.Jti,Guid.NewGuid().ToString()),
                };

                var userToken = _tokenGenerator.GenerateToken(claims);
                
                return userToken;
            }
        }


       
    }
}
