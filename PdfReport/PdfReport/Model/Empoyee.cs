﻿namespace PdfReport.Model
{
    public class Empoyee
    {
        public int EmpId { get; set; }
        public string EmpName { get; set; }
        public string Department { get; set; }
        public string Designation { get; set; }
    }
}
