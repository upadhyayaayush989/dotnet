import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Bill } from 'src/app/models/bill';
import { FoodCart } from 'src/app/models/food-cart';

@Injectable({
  providedIn: 'root'
})
export class BookingService {

  constructor(private httpClient:HttpClient) { 

  }


  AddItemsToCart(newItem : FoodCart):Observable<boolean>{
    return this.httpClient.post<boolean>('https://localhost:7229/api/Booking/AddToCart', newItem);
}

ViewCart(userName:string):Observable<FoodCart[]>{
  return this.httpClient.get<FoodCart[]>('https://localhost:7229/api/Booking/ViewCart?userName='+userName)
}

RemoveFromCart(remove?:number):Observable<boolean>{
  return this.httpClient.delete<boolean>('https://localhost:7229/api/Booking/RemoveItem?cartId='+remove)
}

GenerateBill(userName:string):Observable<Bill[]>{
  return this.httpClient.get<Bill[]>('https://localhost:7229/api/Booking/GenerateBill?userName='+userName)
}

PayBill(userName:string):Observable<boolean>{
  return this.httpClient.delete<boolean>('https://localhost:7229/api/Booking/PayBill?userName='+userName)
}

}
