﻿using System;
using System.Collections.Generic;

namespace ApplicantForm.Models
{
    public partial class CityTbl
    {
        public CityTbl()
        {
            LocalityTbls = new HashSet<LocalityTbl>();
            PersonalDetailTbls = new HashSet<PersonalDetailTbl>();
        }

        public int StateId { get; set; }
        public int CityId { get; set; }
        public string CityName { get; set; } = null!;

        public virtual StateTbl State { get; set; } = null!;
        public virtual ICollection<LocalityTbl> LocalityTbls { get; set; }
        public virtual ICollection<PersonalDetailTbl> PersonalDetailTbls { get; set; }
    }
}
