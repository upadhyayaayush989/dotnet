﻿using Day6_task.Exception;
using Day6_task.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day6_task.Repository
{
    internal class ContactRepository
    {
        List<Contact> contacts;
        public ContactRepository()
        {
            contacts = new List<Contact>()
            {
                new Contact{Name="user1",Address="Address1",City="City1",PhoneNumber="phone1"},
                 new Contact{Name="user2",Address="Address2",City="City2",PhoneNumber="phone2"},
                 new Contact{Name="user3",Address="Address3",City="City3",PhoneNumber="phone3"}
            };
        }

        //add Contact 
        public string AddContact(Contact contact)
        {
            //checking if the contact already exists
            var contactExists = GetContactByName(contact.Name);
            if (contactExists == null)
            {
                contacts.Add(contact);
                return $"Contact added Successfully";
            }
            else
            {
                throw new UserExistsException($"Contact with name: {contact.Name} already exists!!");
            }
        }
        //get all contacts
        public List<Contact> GetAllContact()
        {
            return contacts;
        }
        public Contact GetContactByName(string name)
        {
            return contacts.Find(c => c.Name == name);
        }
        //delete Contact
        public void DeleteContactByName(string name)
        {
            var elementExists = GetContactByName(name);
            if (elementExists != null)
            {
                contacts.Remove(GetContactByName(name));
                Console.WriteLine("Contact Deleted Successully");
            }
            else
            {
                throw new UserExistsException($"Contact with name::{name} does not exists");
            }
            
        }
        public void UpdateContactByName(string name,string NewName,string address,string city,string phone)
        {
            var elementsExisits = GetContactByName(name);
            if(elementsExisits != null)
            {
                elementsExisits.Name = NewName;
                elementsExisits.Address = address;
                elementsExisits.City = city;
                elementsExisits.PhoneNumber = phone;
            }
            else
            {
                throw new UserExistsException($"Contact Name:: {name} does not exists");
            }
            public void DisplayAllContacts()
            {
                foreach(Contact contact1 in contacts)
                {
                    Console.WriteLine(contact1);
                }
            }
           
        }
    }
}
