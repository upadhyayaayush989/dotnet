import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Food } from 'src/app/models/food';
import { FoodCart } from 'src/app/models/food-cart';
import { FoodserviceService } from 'src/app/services/foodservice.service';
import { BookingService } from 'src/app/services/user/booking.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  foods?:Food[]
  foodItem:Food
  foodCart:FoodCart
  constructor(private foodService:FoodserviceService,private bookingService:BookingService,private router:Router) { 
    console.log("Dashboard constructor");
    this.foodItem = new Food();
    this.foodCart=new FoodCart();
    
  }

  ngOnInit(): void {
    console.log('ngonit is working in dashboard');
    this.foodService.getAllFood().subscribe(res=>{
      console.log(res);
      this.foods=res;
     
    })
    
  }

  AddToCart(newItem1:Food) {
      this.foodCart.foodName=newItem1.foodName;
      this.foodCart.foodDiscription=newItem1.foodDiscription;
      this.foodCart.foodType=newItem1.foodType;
      this.foodCart.imagePath=newItem1.imagePath;
      this.foodCart.price=newItem1.price;
      this.foodCart.quantity=1;
      this.foodCart.name=localStorage.getItem("username")!;
      
    this.bookingService.AddItemsToCart(this.foodCart).subscribe(res=>{
      if(res){
        this.router.navigate(['dashboard'])
        console.log("Added successfully");
        Swal.fire(
          `${this.foodCart.foodName}`,
          'Added Successfully',
          'success'
        )
      }
      else{
        //this.router.navigate(['dashboard'])
        console.log("Unsuccessful");
        Swal.fire(
          ' ',
          'UnSuccessfully',
          'error'
        )
        
      }
    });
  }

  GotoViewCart(){
    this.router.navigate(['viewCart'])
  }
  reloadCurrentPage(){
    window.location.reload();
  }

  

}
