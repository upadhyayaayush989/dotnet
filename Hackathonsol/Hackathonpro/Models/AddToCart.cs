﻿namespace Hackathonpro.Models
{
    public class AddToCart
    {
        public int Id { get; set; }

        public string FoodOrdered { get; set; }
        public int Quantity { get; set; }
        public int Total { get; set; }
        public string ImageSource { get; set; }

    }
}
