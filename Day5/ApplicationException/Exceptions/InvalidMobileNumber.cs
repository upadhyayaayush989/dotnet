﻿using System.Runtime.Serialization;
using ApplicationException.Repository;

namespace ApplicationException.Exceptions
{
    internal class InvalidMobileNumber : Exception
    {
        public InvalidMobileNumber(string? message) : base(message)
        {
        }
    }
}