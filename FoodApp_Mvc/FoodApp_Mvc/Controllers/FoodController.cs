﻿using FoodApp_Mvc.Data;
using FoodApp_Mvc.Models;
using Microsoft.AspNetCore.Mvc;

namespace FoodApp_Mvc.Controllers
{
    public class FoodController : Controller
    {
        readonly ApplicationContext _appdbcontext;
        public FoodController(ApplicationContext appdbcontext)
        {
            _appdbcontext = appdbcontext;
        }
        public IActionResult Index()
        {
            var showData = _appdbcontext.FoodMenu.ToList();
            return View(showData);
        }
        //Get method of Create Action
        public IActionResult Create()
        {
            return View();
        }
        //Post Method of Create Action
        [HttpPost]
        public IActionResult Create(Food model)
        {
            if (ModelState.IsValid)
            {
                var food = new Food()
                {
                    FoodName = model.FoodName,
                    FoodType = model.FoodType,
                    Price = model.Price,
                    ImgPath = model.ImgPath

                };
                _appdbcontext.FoodMenu.Add(food);
                _appdbcontext.SaveChanges();
                TempData["show"] = "Food Created";
                return RedirectToAction("Index");
            }
            else
            {
                TempData["message"] = "Empty Fields Can't be Submitted";
                return View(model);
            }
        }
        //Delete Method for Action Delete
        public IActionResult Delete(int id)
        {
            var findFood = _appdbcontext.FoodMenu.SingleOrDefault(f => f.Foodid == id);//SingleOrDefault is a LinQ extension function 
            _appdbcontext.FoodMenu.Remove(findFood);
            _appdbcontext.SaveChanges();
            TempData["show"] = "Food Is Deleted";
            return RedirectToAction("Index");
        }
        [HttpGet]
        public IActionResult Edit(int id)
        {
            var findFood = _appdbcontext.FoodMenu.SingleOrDefault(f => f.Foodid == id);
            var food = new Food()
            {
                FoodName = findFood.FoodName,
                FoodType = findFood.FoodType,
                Price = findFood.Price,
                ImgPath = findFood.ImgPath
            };
            return View(food);
        }

        [HttpPost]
        public IActionResult Edit(Food model)
        {
            var food = new Food()
            {
                Foodid = model.Foodid,
                FoodName = model.FoodName,
                FoodType = model.FoodType,
                Price = model.Price,
                ImgPath = model.ImgPath
            };
            _appdbcontext.FoodMenu.Update(food);
            _appdbcontext.SaveChanges();
            TempData["show"] = "Food has been Updated";
            return RedirectToAction("Index");
        }
    }
    
}
