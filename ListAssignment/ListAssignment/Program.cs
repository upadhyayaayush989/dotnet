﻿// See https://aka.ms/new-console-template for more information
using ListAssignment.Model;
using ListAssignment.Repository;

ContactRepository contactrepository = new ContactRepository();
List<Contact> contactList = contactrepository.GetAllDetails();
foreach (Contact item in contactList)
    Console.WriteLine(item);


Console.WriteLine(" ContactDetails List After addition");
Contact contact = new Contact() { Name = "Aakash", Address = "Mumbai", City = "Mira-Bhayandar", Phone = "9922556644" };
contactrepository.AddContact(contact);
foreach (Contact item in contactList)
{
    Console.WriteLine(item);
}

Console.WriteLine("Delete Operation");
Console.WriteLine("Enter contact to be deleted");
var itemDeleted = Console.ReadLine();
Console.WriteLine(contactrepository.DeleteContactWithName(itemDeleted));
foreach (Contact item in contactList)
{
    Console.WriteLine(item);
}

Console.WriteLine("After Updating.....");
Console.WriteLine("Enter contact to Update...");
var contactUpdate = Console.ReadLine();
contactrepository.UpdateContactByName(contactUpdate);
Console.WriteLine("After Updating.....");
foreach (Contact item in contactList)
{
    Console.WriteLine(item);
}