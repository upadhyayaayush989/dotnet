﻿using Microsoft.AspNetCore.Mvc;
using System.Text;

namespace mvc2.Controllers
{
    public class ProductController : Controller
    {
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Index(IFormCollection form)
        {
            int userId = int.Parse(form["Id"]);
            string name = form["Name"];
            return Content($"ID::{userId}\tName::{name}");
            //return View();
        }
    }
}
