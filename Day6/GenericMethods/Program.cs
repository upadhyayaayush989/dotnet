﻿namespace GenericMethods
{
    //declare the three Delegates
    //public delegate int DelegateToSquare(int num1,int num2);
    //public delegate void DelegateToSquare1(int num1, int num2);
    //public delegate bool DelegateToCount(string message);
    class Program
    {

        static void Main()
        {
            //DelegateToSquare ds = new DelegateToSquare(Square);
            //int result = ds(12, 12);
            //Console.WriteLine(result);
            //Func<int,int,int> ds = Square;
            Func<int, int, int> ds = (a, b) => a * b;
            int result = ds(12, 12);
            Console.WriteLine(result);


            //DelegateToSquare1 ds1 = new DelegateToSquare1(Square1);
            //ds(12, 12);

            Action<int, int> ds1 = Square1;
            ds1(12, 12);

            //DelegateToCount dc = new DelegateToCount(CountLetters);
            //bool result2 = dc("hello team");
            //Console.WriteLine(result2);

            Predicate<string> dc = CountLetters;
            bool resutofcount = dc("hiiii....");
            Console.WriteLine(resutofcount);
        }

        public static int Square(int num1,int num2)
        {
            return num1 * num2;
        }

        public static void Square1(int num1,int num2)
        {
            Console.WriteLine(num1*num2);
        }

        public static bool CountLetters(string message)
        {
            if (message.Count()>4)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }


}

//action delegate ---use when funtion does not returns a value
//func delegate -- use when function returns a value 
// predecate delegate -- use when the function return boolean value